SHADOW_BUILD_DIR = release

SHADOW_DEBUG_DIR = debug

LIBNAME = Photon

NAME =  lib$(LIBNAME).so

DEBUG =  lib$(LIBNAME)D.so

all: $(NAME)

$(NAME) :
	mkdir -p $(SHADOW_BUILD_DIR)
	cd $(SHADOW_BUILD_DIR) && cmake -DCMAKE_BUILD_TYPE=Release ..
	make -j8 -C $(SHADOW_BUILD_DIR)

$(DEBUG) :
	mkdir -p $(SHADOW_DEBUG_DIR)
	cd $(SHADOW_DEBUG_DIR) && cmake -DCMAKE_BUILD_TYPE=Debug ..
	make -j8 -C $(SHADOW_DEBUG_DIR)

release : $(NAME)
	./deploy.sh

debug : $(DEBUG)

clean :
	rm -Rf $(SHADOW_BUILD_DIR)
	rm -Rf $(SHADOW_DEBUG_DIR)

fclean : clean
	rm -Rf lib/*.so

re: fclean all

install: uninstall fclean release debug
	sudo cp lib/$(NAME) /usr/lib
	sudo cp lib/$(DEBUG) /usr/lib
	sudo cp -R include/$(LIBNAME) /usr/include

uninstall:
	sudo rm -rf /usr/lib/$(NAME)
	sudo rm -rf /usr/lib/$(DEBUG)
	sudo rm -rf /usr/include/$(LIBNAME)
