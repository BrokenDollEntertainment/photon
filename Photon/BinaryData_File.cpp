#include "BinaryData_File.hpp"

Photon::BinaryData::File::File() : valid(true), header({PFB_VERSION, 0}) {}

Photon::BinaryData::File::File(bool valid) : valid(valid) {}

Photon::BinaryData::File::File(Header header, const std::unordered_map<std::string, std::shared_ptr<Section>> &sections) :
    valid(true),
    header(header),
    sections(sections) {}

bool Photon::BinaryData::File::find(const std::string &name) const
{
    return (sections.find(name) != sections.end());
}

void Photon::BinaryData::File::addSection(const std::shared_ptr<Photon::BinaryData::Section> &section)
{
    std::string name = section->getSectionName();
    if (sections.find(name) == sections.end())
        ++header.nbSection;
    sections[name] = section;
}

bool Photon::BinaryData::File::operator!() const
{
    return !valid;
}