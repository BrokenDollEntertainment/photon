#ifndef PHOTON_BINARYDATA_FILE_HPP
#define PHOTON_BINARYDATA_FILE_HPP

#include <memory>
#include <unordered_map>
#include "BinaryData_Section.hpp"

#define PFB_VERSION 1

/**
 * Namespace containing everything needed to load and write pbdf (Photon binary data file)
 *
 * @see Photon::BinaryData::Reader
 */
namespace Photon::BinaryData
{
    class Reader;
    /**
     * Class representing a pbdf file
     */
    class File
    {
        friend class Reader;
    private:
        struct Header
        {
            //Always have to be on top of header structure
            unsigned int version;
            unsigned int nbSection;
        };

    private:
        bool valid;
        Header header;
        std::unordered_map<std::string, std::shared_ptr<Section>> sections;

    private:
        File(bool valid);
        File(Header, const std::unordered_map<std::string, std::shared_ptr<Section>> &);

    public:
        File();
        /**
         * Check if a section exist in the file
         *
         * @param name Name of the section to search
         * @return true if the section exist
         */
        bool find(const std::string &name) const;
        /**
         * Add the given section to the file
         *
         * @param section Section to add
         */
        void addSection(const std::shared_ptr<Section> &section);
        /**
         * Operator use to check the file validity
         *
         * @return false if the file is valid
         */
        bool operator!() const;

        /**
         * Return the section with the given name
         *
         * @tparam T Type of the Section to return
         * @param name Name of the Section to return
         * @return The Section with the given type
         */
        template <typename T>
        const std::shared_ptr<T> getSection(const std::string &name) const
        {
            return std::dynamic_pointer_cast<T>(sections.at(name));
        }
    };
}

#endif
