#ifndef PHOTON_BINARYDATA_OBJECT_HPP
#define PHOTON_BINARYDATA_OBJECT_HPP

#include "BinaryData_File.hpp"

namespace Photon::BinaryData
{
    /**
     * Class that represent a pbdf Object
     */
    class Object
    {
    public:
        /**
         * Read the Object from the file
         *
         * @param file Photon Binary Data File to read from
         */
        virtual void read(const File &file) = 0;
        /**
         * Write the Object to the file
         *
         * @param file Photon Binary Data File to write to
         */
        virtual void write(File &file) = 0;
    };
}

#endif
