#include "Debugger.hpp"
#include "BinaryData_Reader.hpp"
#include "BinaryData_SkeletonSectionReader.hpp"
#include "BinaryData_SkeletonKeyFramesSectionReader.hpp"

std::unordered_map<std::string, std::shared_ptr<Photon::BinaryData::SectionReader>> Photon::BinaryData::Reader::readers = {
        {"s", std::make_shared<Photon::BinaryData::SkeletonSectionReader>()},
        {"kF", std::make_shared<Photon::BinaryData::SkeletonKeyFramesSectionReader>()}
};

Photon::BinaryData::File Photon::BinaryData::Reader::read(const std::string &path)
{
    FILE *fp = fopen(path.c_str(), "r");
    if (fp == nullptr)
    {
        Debugger::log("Photon::BinaryData::Reader", "read", "Cannot open " + path);
        return File(false);
    }
    File::Header header = {PFB_VERSION, 0};
    auto readSize = static_cast<unsigned int>(fread(&header, sizeof(File::Header), 1, fp));
    if (readSize != 1 || header.version > PFB_VERSION)
    {
        fclose(fp);
        Debugger::log("Photon::BinaryData::Reader", "read", "Invalid header");
        return File(false);
    }
    std::unordered_map<std::string, std::shared_ptr<Section>> sections;
    for (unsigned int n = 0; n != header.nbSection; ++n)
    {
        std::string sectionKeyWord;
        unsigned int strSize = 0;
        readSize = static_cast<unsigned int>(fread(&strSize, sizeof(unsigned int), 1, fp));
        if (readSize != 1)
        {
            fclose(fp);
            Debugger::log("Photon::BinaryData::Reader", "read", "Cannot read section name size");
            return File(false);
        }
        if (strSize != 0)
        {
            char *nameBytes = new char[strSize + 1];
            readSize = static_cast<unsigned int>(fread(nameBytes, 1, strSize, fp));
            if (readSize != strSize)
            {
                delete[](nameBytes);
                fclose(fp);
                Debugger::log("Photon::BinaryData::Reader", "read", "Cannot read section name");
                return File(false);
            }
            nameBytes[strSize] = '\0';
            sectionKeyWord = nameBytes;
            delete[](nameBytes);
        }
        std::shared_ptr<Section> section = nullptr;
        if (readers.find(sectionKeyWord) != readers.end())
            section = readers[sectionKeyWord]->create();
        if (section != nullptr)
        {
            section->setFd(fp);
            if (!section->readSection(header.version))
            {
                fclose(fp);
                Debugger::log("Photon::BinaryData::Reader", "read", "Invalid section " + section->sectionName);
                return File(false);
            }
            section->setFd(nullptr);
            sections[section->getSectionName()] = section;
        }
    }
    fclose(fp);
    return File(header, sections);
}

void Photon::BinaryData::Reader::write(const std::string &path, const File &file)
{
    std::string pfbPath = path + ".pbdf";
    FILE *fp = fopen(pfbPath.c_str(), "w+");
    if (fp == nullptr)
    {
        Debugger::log("Photon::BinaryData::Reader", "write", "Cannot create or open " + path);
        return;
    }
    auto header = file.header;
    fwrite(&header, sizeof(File::Header), 1, fp);
    for (const auto &pair : file.sections)
    {
        auto section = pair.second;
        std::string str = section->getKeyWord();
        auto nameSize = static_cast<unsigned int>(str.size());
        fwrite(&nameSize, sizeof(unsigned int), 1, fp);
        if (nameSize != 0)
            fwrite(str.c_str(), 1, nameSize, fp);
        section->setFd(fp);
        section->writeSection();
        section->setFd(nullptr);
    }
    fclose(fp);
}