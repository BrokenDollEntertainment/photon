#ifndef PHOTON_BINARYDATA_READER_HPP
#define PHOTON_BINARYDATA_READER_HPP

#include <functional>
#include "BinaryData_File.hpp"
#include "BinaryData_SectionReader.hpp"

namespace Photon::BinaryData
{
    /**
     * Class that represent an pbdf parser
     */
    class Reader
    {
    private:
        static std::unordered_map<std::string, std::shared_ptr<SectionReader>> readers;
    public:
        /**
         * Create a SectionReader and add it to the Reader
         *
         * @tparam T Type of the SectionReader to add
         * @param args Argument to instantiate the SectionReader
         */
        template <typename T, typename... Args>
        static void registerReader(Args&&... args)
        {
            static_assert(std::is_base_of<T, SectionReader>::value, "Can only register SectionReader");
            auto reader = std::make_shared<T>(std::forward<Args>(args)...);
            auto section = reader->create();
            std::string name = section->getKeyWord();
            readers[name] = std::make_shared<T>(std::forward<Args>(args)...);
        }
        /**
         * Read the pbdf file at the given path
         *
         * @param path Path to the pbdf to read
         * @return A File containing everything read from the path if possible
         */
        static File read(const std::string &path);
        /**
         * Write the given pbdf file to the given path
         *
         * @param path Path where to write the pbdf
         * @param file File to write
         */
        static void write(const std::string &path, const File &file);
    };
}

#endif
