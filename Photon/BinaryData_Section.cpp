#include "BinaryData_Section.hpp"

Photon::BinaryData::Section::Section(const std::string &keyWord, const std::string &sectionName) : keyWord(keyWord), sectionName(sectionName) {}

void Photon::BinaryData::Section::setFd(FILE *fd)
{
    this->fd = fd;
}

const std::string &Photon::BinaryData::Section::getKeyWord() const
{
    return keyWord;
}

const std::string &Photon::BinaryData::Section::getSectionName() const
{
    return sectionName;
}
