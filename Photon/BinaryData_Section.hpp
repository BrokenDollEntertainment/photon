#ifndef PHOTON_BINARYDATA_SECTION_HPP
#define PHOTON_BINARYDATA_SECTION_HPP

#include <cstdio>
#include <string>

namespace Photon::BinaryData
{
    class Reader;
    /**
     * Class that represent a section of a pbdf
     */
    class Section
    {
        friend class Reader;
    private:
        std::string keyWord;
        std::string sectionName;
        FILE *fd;

    private:
        void setFd(FILE *);

    protected:
        /**
         * Create a Section
         *
         * @param keyWord Name of the section in the pbdf file
         * @param sectionName Name of the section in the File
         */
        Section(const std::string &keyWord, const std::string &sectionName);

        /**
         * When writing the section, write the given value to the pbdf
         *
         * @param val Value to write
         */
        template <typename T>
        void write(T val)
        {
            if (fd == nullptr)
                return;
            fwrite(&val, sizeof(T), 1, fd);
        }

        /**
         * When reading the section, read from the pbdf
         *
         * @param buf Buffer where to write the read value
         * @return true if the read succeed
         */
        template <typename T>
        bool read(T *buf)
        {
            if (fd == nullptr)
                return false;
            return (fread(buf, sizeof(T), 1, fd) == 1);
        }

        /**
         * When writing the section, write the given string to the pbdf
         *
         * @param val String to write
         */
        void write(const std::string &val)
        {
            if (fd == nullptr)
                return;
            auto nameSize = static_cast<unsigned int>(val.size());
            fwrite(&nameSize, sizeof(unsigned int), 1, fd);
            if (nameSize != 0)
                fwrite(val.c_str(), 1, nameSize, fd);
        }

        /**
         * When reading the section, read a string from the pbdf
         *
         * @param buf String where to write the read value
         * @return true if the read succeed
         */
        bool read(std::string *buf)
        {
            if (fd == nullptr)
                return false;
            unsigned int strSize = 0;
            auto readSize = static_cast<unsigned int>(fread(&strSize, sizeof(unsigned int), 1, fd));
            if (readSize != 1)
                return false;
            if (strSize != 0)
            {
                char *nameBytes = new char[strSize + 1];
                readSize = static_cast<unsigned int>(fread(nameBytes, 1, strSize, fd));
                if (readSize != strSize)
                {
                    delete[](nameBytes);
                    return false;
                }
                nameBytes[strSize] = '\0';
                *buf = nameBytes;
                delete[](nameBytes);
            }
            return true;
        }

    private:
        const std::string &getKeyWord() const;
        virtual void writeSection() = 0;
        virtual bool readSection(unsigned int) = 0;

    public:
        /**
         * @return The name of the Section in the File
         */
        const std::string &getSectionName() const;
    };
}

#endif
