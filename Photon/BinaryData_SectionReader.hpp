#ifndef PHOTON_BINARYDATA_SECTIONREADER_HPP
#define PHOTON_BINARYDATA_SECTIONREADER_HPP

#include <memory>
#include "BinaryData_Section.hpp"

namespace Photon::BinaryData
{
    /**
     * Class use by the reader to instantiate Section when reading pbdf file
     */
    class SectionReader
    {
        friend class Reader;
    protected:
        /**
         * Instantiate a Section to read
         *
         * @return The new Section to read
         */
        virtual std::shared_ptr<Section> create() = 0;
    };
}

#endif
