#include "Debugger.hpp"
#include "BinaryData_SkeletonKeyFramesSection.hpp"

Photon::BinaryData::SkeletonKeyFramesSection::SkeletonKeyFramesSection() : Section("kF", "keyFrames") {}

void Photon::BinaryData::SkeletonKeyFramesSection::writeSection()
{

}

bool Photon::BinaryData::SkeletonKeyFramesSection::readSection(unsigned int version)
{
    if (version > 1)
    {
        Debugger::log("Photon::BinaryData::SkeletonKeyFramesSection", "readSection", "Unsupported version " + std::to_string(version));
        return false;
    }
    return false;
}
