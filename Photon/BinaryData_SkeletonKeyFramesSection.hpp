#ifndef PHOTON_BINARYDATA_SKELETONKEYFRAMESSECTION_HPP
#define PHOTON_BINARYDATA_SKELETONKEYFRAMESSECTION_HPP

#include "BinaryData_Section.hpp"

namespace Photon::BinaryData
{
    /**
     * Class that represent the Section storing all key frames information for skeleton animation
     */
    class SkeletonKeyFramesSection : public Section
    {
    private:
        void writeSection() override;
        bool readSection(unsigned int i) override;

    public:
        SkeletonKeyFramesSection();
    };
}

#endif
