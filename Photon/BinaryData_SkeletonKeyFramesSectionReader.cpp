#include "BinaryData_SkeletonKeyFramesSectionReader.hpp"
#include "BinaryData_SkeletonKeyFramesSection.hpp"

std::shared_ptr<Photon::BinaryData::Section> Photon::BinaryData::SkeletonKeyFramesSectionReader::create()
{
    return std::make_shared<SkeletonKeyFramesSection>();
}
