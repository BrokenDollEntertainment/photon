#ifndef PHOTON_BINARYDATA_SKELETONKEYFRAMESSECTIONREADER_HPP
#define PHOTON_BINARYDATA_SKELETONKEYFRAMESSECTIONREADER_HPP

#include "BinaryData_SectionReader.hpp"

namespace Photon::BinaryData
{
    /**
     * Class use by the reader to instantiate SkeletonKeyFramesSection when reading pbdf file
     */
    class SkeletonKeyFramesSectionReader : public SectionReader
    {
    private:
        std::shared_ptr<Section> create() override;
    };
}

#endif
