#include "Debugger.hpp"
#include "BinaryData_SkeletonSection.hpp"

Photon::BinaryData::SkeletonSection::SkeletonSection() : Section("s", "skeleton") {}

void Photon::BinaryData::SkeletonSection::writeSection()
{
    auto nbJoint = static_cast<unsigned int>(joints.size());
    write(nbJoint);
    for (const auto &joint : joints)
    {
        write(joint.name);
        write(joint.parent);
        write(joint.posX);
        write(joint.posY);
        write(joint.posZ);
        write(joint.rotX);
        write(joint.rotY);
        write(joint.rotZ);
    }
}

bool Photon::BinaryData::SkeletonSection::readJoint()
{
    Joint joint;
    if (!read(&joint.name))
    {
        Debugger::log("Photon::BinaryData::SkeletonSection", "readJoint", "Cannot read joint name");
        return false;
    }
    if (!read(&joint.parent))
    {
        Debugger::log("Photon::BinaryData::SkeletonSection", "readJoint", "Cannot read joint parent");
        return false;
    }
    if (!read(&joint.posX))
    {
        Debugger::log("Photon::BinaryData::SkeletonSection", "readJoint", "Cannot read joint x position");
        return false;
    }
    if (!read(&joint.posY))
    {
        Debugger::log("Photon::BinaryData::SkeletonSection", "readJoint", "Cannot read joint y position");
        return false;
    }
    if (!read(&joint.posZ))
    {
        Debugger::log("Photon::BinaryData::SkeletonSection", "readJoint", "Cannot read joint z position");
        return false;
    }
    if (!read(&joint.rotX))
    {
        Debugger::log("Photon::BinaryData::SkeletonSection", "readJoint", "Cannot read joint x rotation");
        return false;
    }
    if (!read(&joint.rotY))
    {
        Debugger::log("Photon::BinaryData::SkeletonSection", "readJoint", "Cannot read joint y rotation");
        return false;
    }
    if (!read(&joint.rotZ))
    {
        Debugger::log("Photon::BinaryData::SkeletonSection", "readJoint", "Cannot read joint z rotation");
        return false;
    }
    joints.emplace_back(joint);
    return true;
}

bool Photon::BinaryData::SkeletonSection::readSection(unsigned int version)
{
    if (version > 1)
    {
        Debugger::log("Photon::BinaryData::SkeletonSection", "readSection", "Unsupported version " + std::to_string(version));
        return false;
    }
    unsigned int nbJoint;
    if (!read(&nbJoint))
    {
        Debugger::log("Photon::BinaryData::SkeletonSection", "readSection", "Cannot read number of joint");
        return false;
    }
    for (unsigned int n = 0; n != nbJoint; ++n)
    {
        if (!readJoint())
        {
            Debugger::log("Photon::BinaryData::SkeletonSection", "readSection", "Cannot read joint " + std::to_string(n));
            return false;
        }
    }
    return true;
}