#ifndef PHOTON_BINARYDATA_SKELETONSECTION_HPP
#define PHOTON_BINARYDATA_SKELETONSECTION_HPP

#include <vector>
#include "BinaryData_Section.hpp"

namespace Photon
{
    namespace Object
    {
        class Skeleton;
    }
    namespace BinaryData
    {
        /**
         * Class that represent the Section storing all skeleton information for skeleton animation
         */
        class SkeletonSection : public Section
        {
            friend class Photon::Object::Skeleton;
        private:
            struct Joint
            {
                std::string name;
                std::string parent;
                float posX;
                float posY;
                float posZ;
                float rotX;
                float rotY;
                float rotZ;
            };

        private:
            std::vector<Joint> joints;

        private:
            bool readJoint();
            void writeSection() override;
            bool readSection(unsigned int) override;

        public:
            SkeletonSection();
        };
    }
}

#endif
