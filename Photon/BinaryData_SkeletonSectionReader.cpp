#include "BinaryData_SkeletonSectionReader.hpp"
#include "BinaryData_SkeletonSection.hpp"

std::shared_ptr<Photon::BinaryData::Section> Photon::BinaryData::SkeletonSectionReader::create()
{
    return std::make_shared<SkeletonSection>();
}
