#ifndef PHOTON_BINARYDATA_SKELETONSECTIONREADER_HPP
#define PHOTON_BINARYDATA_SKELETONSECTIONREADER_HPP

#include "BinaryData_SectionReader.hpp"

namespace Photon::BinaryData
{
    /**
     * Class use by the reader to instantiate SkeletonSection when reading pbdf file
     */
    class SkeletonSectionReader : public SectionReader
    {
    private:
        std::shared_ptr<Section> create() override;
    };
}

#endif
