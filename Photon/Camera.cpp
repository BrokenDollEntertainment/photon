#include "Debugger.hpp"
#include "GLUtil.hpp"
#include "Camera.hpp"
#include "Mouse.hpp"
#include "Renderer.hpp"
#include "LightManager.hpp"
#include "View.hpp"

#define VIEW_DISTANCE_LIMIT 2

double Photon::Camera::viewDistanceFactor = sqrt(3);

Photon::Camera::Camera(const std::string &name, const std::weak_ptr<Scene> &scene) :
    name(name),
    enable(true),
    depthClamp(false),
    skyBox(nullptr),
    upY(1),
    viewDistance(100),
    fov(70),
    screenWidth(0),
    screenHeight(0),
    position(3, 3, 3),
    lookAt(0, 0, 0),
    scene(scene)
{
    viewDirection();
}

void Photon::Camera::enableDepthClamp(bool value)
{
    depthClamp = value;
}

void Photon::Camera::initView(bool setView)
{
    if (!depthClamp)
		glEnable(GL_DEPTH_CLAMP);
    else
		glDisable(GL_DEPTH_CLAMP);
    glPushMatrix();
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glMatrixMode(GL_PROJECTION);
        glPushMatrix();
            glLoadIdentity();
            unsigned int camWidth = (screenWidth == 0) ? winWidth() : screenWidth;
            unsigned int camHeight = (screenHeight == 0) ? winHeight() : screenHeight;
            glScissor(screenPosition.getX(), screenPosition.getY(), camWidth, camHeight);
            glViewport(screenPosition.getX(), screenPosition.getY(), camWidth, camHeight);
            if (setView)
                View::setView(screenPosition, camWidth, camHeight);
            initCamera(setView);
}

void Photon::Camera::clearView()
{
            clearCamera();
        glPopMatrix();
    glPopMatrix();
}

const Photon::Vector3f Photon::Camera::mouseToWorldCoord(float zPercent)
{
    return toWorldCoord(Mouse::getRawPosition(), zPercent);
}

const Photon::Vector3f Photon::Camera::toWorldCoord(const Photon::Vector2f &pos, float zPercent)
{
    initView(false);
    GLint viewport[4];
    GLdouble modelview[16];
    GLdouble projection[16];
    GLdouble posX, posY, posZ;
    glGetDoublev(GL_MODELVIEW_MATRIX, modelview);
    glGetDoublev(GL_PROJECTION_MATRIX, projection);
    glGetIntegerv(GL_VIEWPORT, viewport);
    gluUnProject(pos.getX(), viewport[3] - pos.getY(), 0, modelview, projection, viewport, &posX, &posY, &posZ);
    auto ray0 = Vector3f(static_cast<float>(posX), static_cast<float>(posY), static_cast<float>(posZ));
    gluUnProject(pos.getX(), viewport[3] - pos.getY(), 1, modelview, projection, viewport, &posX, &posY, &posZ);
    auto ray1 = Vector3f(static_cast<float>(posX), static_cast<float>(posY), static_cast<float>(posZ));
    clearView();
    return ray0 + ((ray1 - ray0) * (zPercent / 100));
}

const Photon::Vector2f Photon::Camera::toScreenCoord(const Photon::Vector3f &pos)
{
    initView(false);
    GLint viewport[4];
    GLdouble modelview[16];
    GLdouble projection[16];
    GLdouble posX, posY, posZ;
    glGetDoublev(GL_MODELVIEW_MATRIX, modelview);
    glGetDoublev(GL_PROJECTION_MATRIX, projection);
    glGetIntegerv(GL_VIEWPORT, viewport);
    gluProject(pos.getX(), pos.getY(), pos.getZ(), modelview, projection, viewport, &posX, &posY, &posZ);
    auto screenCoord = Vector2f(static_cast<float>(posX), static_cast<float>(posY));
    clearView();
    return screenCoord;
}

void Photon::Camera::update()
{
    if (enable)
    {
        initView(true);
        renderLayers();
        clearView();
    }
}

void Photon::Camera::renderLayers()
{
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
        glLoadIdentity();
        for (const auto &layer : layers)
            layer.second->render();
    glPopMatrix();
}

int Photon::Camera::winWidth()
{
    return Renderer::getWindowWidth();
}

int Photon::Camera::winHeight()
{
    return Renderer::getWindowHeight();
}

void Photon::Camera::addLayer(const std::shared_ptr<Photon::Layer> &layer)
{
    if (layerToWatch.find(layer->getName()) != layerToWatch.end())
        layers[layer->getName()] = layer;
}

void Photon::Camera::addLayer(const std::string &layer)
{
    if (layerToWatch.find(layer) != layerToWatch.end())
    {
        Debugger::log("Photon::Camera", "addLayer", "Camera is already watching layer " + layer);
        return;
    }
    layerToWatch.insert(layer);
    auto layerToAdd = scene.lock()->getLayer(layer);
    if (layerToAdd == nullptr)
    {
        Debugger::log("Photon::Camera", "addLayer", "Layer " + layer + " does not exist");
        return;
    }
    addLayer(layerToAdd);
}

void Photon::Camera::removeLayer(const std::string &layer)
{
    if (layerToWatch.find(layer) == layerToWatch.end())
    {
        Debugger::log("Photon::Camera", "removeLayer", "Layer " + layer + " does not exist");
        return;
    }
    layerToWatch.erase(layer);
    if (layers.find(layer) == layers.end())
    {
        Debugger::log("Photon::Camera", "removeLayer", "Layer " + layer + " does not exist");
        return;
    }
    layers.erase(layer);
}

void Photon::Camera::viewDirection()
{
    direction = (position - lookAt).normalize();
    right = direction.crossProduct(Vector3f(0, upY, 0)).normalize();
    up = (right.crossProduct(direction) * Vector3f(1, -1, 1)).normalize();
}

void Photon::Camera::setPosition(float x, float y, float z)
{
    position.setX(x);
    position.setY(y);
    position.setZ(z);
    viewDirection();
}

void Photon::Camera::setLookAt(float x, float y, float z)
{
    lookAt.setX(x);
    lookAt.setY(y);
    lookAt.setZ(z);
    viewDirection();
}

void Photon::Camera::translate(float rightDir, float upDir, float forward)
{
    translatePos(rightDir, upDir, forward);
    translateAt(rightDir, upDir, forward);
}

void Photon::Camera::translatePos(float rightDir, float upDir, float forward)
{
    if (rightDir != 0)
        position += right * rightDir;
    if (upDir != 0)
        position += up * upDir;
    if (forward != 0)
        position += direction * forward;
    viewDirection();
}

void Photon::Camera::translateAt(float rightDir, float upDir, float forward)
{
    if (rightDir != 0)
        lookAt += right * rightDir;
    if (upDir != 0)
        lookAt += up * upDir;
    if (forward != 0)
        lookAt += direction * forward;
    viewDirection();
}

const Photon::Vector3f &Photon::Camera::getPosition() const
{
    return position;
}

const Photon::Vector3f &Photon::Camera::getLookAt() const
{
    return lookAt;
}

void Photon::Camera::setViewDistance(double viewDistance)
{
    if (viewDistance < VIEW_DISTANCE_LIMIT)
        viewDistance = VIEW_DISTANCE_LIMIT;
    this->viewDistance = viewDistance * viewDistanceFactor;
    if (skyBox != nullptr)
        skyBox->setSize(static_cast<float>(viewDistance * 2));
}

void Photon::Camera::setFov(double fov)
{
    if (fov > 180)
    {
        while (fov > 180)
            fov -= 180;
    }
    else if (fov < 0)
    {
        while (fov < 0)
            fov += 180;
    }
    this->fov = fov;
}

void Photon::Camera::setEnable(bool enable)
{
    this->enable = enable;
}

const std::string &Photon::Camera::getName() const
{
    return name;
}

void Photon::Camera::initSkyBox()
{
    skyBox->linkPosition(position);
    skyBox->setSize(static_cast<float>((viewDistance / viewDistanceFactor) * 2));
    skyBox->getMaterial()->setLightSensitivity(false);
}

void Photon::Camera::setSkyBox(Texture::Box &texture)
{
    this->skyBox = std::make_shared<Object::SkyBox>(texture);
    initSkyBox();
}

void Photon::Camera::setSkyBox(const Color &color)
{
    this->skyBox = std::make_shared<Object::SkyBox>(color);
    initSkyBox();
}

void Photon::Camera::setSkyBox()
{
    this->skyBox = std::make_shared<Object::SkyBox>();
    initSkyBox();
}

const std::shared_ptr<Photon::Object::SkyBox> &Photon::Camera::getSkyBox() const
{
    return skyBox;
}

void Photon::Camera::setScreenPosition(int x, int y)
{
    screenPosition.setX(x);
    screenPosition.setY(y);
}

void Photon::Camera::setScreenPosition(const Photon::Vector2i &screenPosition)
{
    this->screenPosition = screenPosition;
}

void Photon::Camera::setScreenDimension(unsigned int screenWidth, unsigned int screenHeight)
{
    this->screenWidth = screenWidth;
    this->screenHeight = screenHeight;
}

double Photon::Camera::getViewDistance() const
{
    return viewDistance;
}

double Photon::Camera::getFov() const
{
    return fov;
}
