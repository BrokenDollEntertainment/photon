#ifndef PHOTON_CAMERA_HPP
#define PHOTON_CAMERA_HPP

#include <set>
#include <unordered_map>
#include "Layer.hpp"
#include "Object_SkyBox.hpp"
#include "Vector2.hpp"

namespace Photon
{
    class Scene;
    class Camera
    {
        friend class Scene;
    protected:
        static double viewDistanceFactor;
        std::string name;
        bool enable;
        bool depthClamp;
        std::set<std::string> layerToWatch;
        std::unordered_map<std::string, std::shared_ptr<Layer>> layers;
        std::shared_ptr<Object::SkyBox> skyBox;
        short upY;
        double viewDistance;
        double fov;
        Vector2i screenPosition;
        unsigned int screenWidth;
        unsigned int screenHeight;
        Vector3f position;
        Vector3f lookAt;
        Vector3f direction;
        Vector3f right;
        Vector3f up;
        std::weak_ptr<Scene> scene;

    private:
        void initView(bool);
        void clearView();
        void renderLayers();
        void initSkyBox();
        void update();
        void viewDirection();
        void addLayer(const std::shared_ptr<Layer> &);
        virtual void initCamera(bool) = 0;
        virtual void clearCamera() = 0;

    protected:
        int winWidth();
        int winHeight();

    public:
        Camera(const std::string &, const std::weak_ptr<Scene> &);
        void enableDepthClamp(bool);
        void setViewDistance(double);
        void setFov(double);
        void setPosition(float, float, float);
        void setLookAt(float, float, float);
        void translate(float, float, float);
        void translatePos(float, float, float);
        void translateAt(float, float, float);
        const Vector3f &getPosition() const;
        const Vector3f &getLookAt() const;
        void addLayer(const std::string &);
        void removeLayer(const std::string &);
        void setEnable(bool enable);
        const std::string &getName() const;
        void setSkyBox(Texture::Box &);
        void setSkyBox(const Color &);
        void setSkyBox();
        const std::shared_ptr<Object::SkyBox> &getSkyBox() const;
        void setScreenPosition(int, int);
        void setScreenPosition(const Vector2i &);
        void setScreenDimension(unsigned int, unsigned int);
        double getViewDistance() const;
        double getFov() const;
        const Vector3f mouseToWorldCoord(float = 50.0f);
        const Vector3f toWorldCoord(const Vector2f &, float = 50.0f);
        const Vector2f toScreenCoord(const Vector3f &);
    };
}

#endif