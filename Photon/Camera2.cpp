#include "OpenGL.hpp"
#include "Camera2.hpp"
#include "View.hpp"

Photon::Camera2::Camera2(const std::string &name, const std::weak_ptr<Photon::Scene> &scene) : Camera(name, scene)
{
    viewDistance = 10;
}

void Photon::Camera2::initCamera(bool setView)
{
    glOrtho(0, winWidth(), 0, winHeight(), -1, (viewDistance - 1));
    if (setView)
        View::ortho(0, winWidth(), 0, winHeight());
}

void Photon::Camera2::clearCamera() {}
