#ifndef PHOTON_CAMERA2_HPP
#define PHOTON_CAMERA2_HPP

#include "Camera.hpp"

namespace Photon
{
    class Camera2 : public Camera
    {
    private:
        void initCamera(bool) override;
        void clearCamera() override;

    public:
        Camera2(const std::string &, const std::weak_ptr<Scene> &);
    };
}

#endif
