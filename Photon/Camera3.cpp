#include "GLUtil.hpp"
#include "Camera3.hpp"
#include "LightManager.hpp"
#include "View.hpp"

Photon::Camera3::Camera3(const std::string &name, const std::weak_ptr<Photon::Scene> &scene) : Camera(name, scene) {}

void Photon::Camera3::initCamera(bool setView)
{
    unsigned int camWidth = (screenWidth == 0) ? winWidth() : screenWidth;
    unsigned int camHeight = (screenHeight == 0) ? winHeight() : screenHeight;
    gluPerspective(fov, camWidth / camHeight, 1, viewDistance);
    gluLookAt(position.getX(), position.getY(), position.getZ(), lookAt.getX(), lookAt.getY(), lookAt.getZ(), 0.0f, upY, 0.0f);
    LightManager::enableLight();
    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_TRUE);
    //if (setView)
    //View::perspective(fov, camWidth / camHeight, 1, viewDistance);
}

void Photon::Camera3::clearCamera()
{
    glDepthMask(GL_FALSE);
    if (skyBox != nullptr)
        skyBox->render();
    glDepthMask(GL_TRUE);
    LightManager::disableLight();
    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);
}
