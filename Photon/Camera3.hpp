#ifndef PHOTON_CAMERA3_HPP
#define PHOTON_CAMERA3_HPP

#include <variant>
#include "Camera.hpp"

namespace Photon
{
    class Camera3 : public Camera
    {
    private:
        void initCamera(bool) override;
        void clearCamera() override;

    public:
        Camera3(const std::string &, const std::weak_ptr<Scene> &);
    };
}

#endif
