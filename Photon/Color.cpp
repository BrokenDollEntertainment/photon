#include "Color.hpp"

const Photon::Color Photon::Color::WHITE(255, 255, 255);
const Photon::Color Photon::Color::BLACK(0, 0, 0);
const Photon::Color Photon::Color::RED(255, 0, 0);
const Photon::Color Photon::Color::GREEN(0, 255, 0);
const Photon::Color Photon::Color::BLUE(0, 0, 255);
const Photon::Color Photon::Color::YELLOW(255, 255, 0);
const Photon::Color Photon::Color::CYAN(0, 255, 255);
const Photon::Color Photon::Color::PINK(255, 0, 255);

Photon::Color::Color(float r, float g, float b, float a) : r(r), g(g), b(b), a(a) {}

float Photon::Color::getR() const
{
    return r;
}

float Photon::Color::getG() const
{
    return g;
}

float Photon::Color::getB() const
{
    return b;
}

float Photon::Color::getA() const
{
    return a;
}

void Photon::Color::setRGB(float r, float g, float b)
{
    this->r = r;
    this->g = g;
    this->b = b;
}

void Photon::Color::setRGB(float r, float g, float b, float a)
{
    this->r = r;
    this->g = g;
    this->b = b;
    this->a = a;
}

bool Photon::Color::operator==(const Photon::Color &other) const
{
    return r == other.r && g == other.g && b == other.b && a == other.a;
}

bool Photon::Color::operator!=(const Photon::Color &other) const
{
    return !(other == *this);
}
