#ifndef PHOTON_COLOR_HPP
#define PHOTON_COLOR_HPP

namespace Photon
{
    class Color
    {
    public:
        static const Color WHITE;
        static const Color BLACK;
        static const Color RED;
        static const Color GREEN;
        static const Color BLUE;
        static const Color YELLOW;
        static const Color CYAN;
        static const Color PINK;
    private:
        float r;
        float g;
        float b;
        float a;

    public:
        Color(const Color &) = default;
        Color(float r, float g, float b, float a = 255);
        Color &operator=(const Color &) = default;
        float getR() const;
        float getG() const;
        float getB() const;
        float getA() const;
        void setRGB(float, float, float);
        void setRGB(float, float, float, float);
        bool operator==(const Color &rhs) const;
        bool operator!=(const Color &rhs) const;
    };
}

#endif
