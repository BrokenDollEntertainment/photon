#include <iostream>
#include "Debugger.hpp"

#ifdef DEBUG
    bool Photon::Debugger::enabled = true;
#else
    bool Photon::Debugger::enabled = false;
#endif

std::unordered_map<std::string, std::vector<std::pair<std::string, std::string>>> Photon::Debugger::logs;

void Photon::Debugger::log(const std::string &object, const std::string &method, const std::string &message)
{
    std::string msg = message;
    while (msg[msg.size() - 1] == '\n')
        msg.pop_back();
    if (logs.find(object) == logs.end())
        logs[object] = std::vector<std::pair<std::string, std::string>>();
    logs[object].emplace_back(std::make_pair(method, msg));
    if (enabled)
        std::cout << object.c_str() << "::" << method.c_str() << ": " << msg.c_str() << std::endl;
}

void Photon::Debugger::setEnabled(bool enabled)
{
    Debugger::enabled = enabled;
}

const std::pair<std::string, std::string> Photon::Debugger::getLast(const std::string &object)
{
    if (logs.find(object) == logs.end())
        return std::make_pair("", "");
    return logs[object][logs[object].size() - 1];
}
