#ifndef PHOTON_DEBUGGER_HPP
#define PHOTON_DEBUGGER_HPP

#include <unordered_map>
#include <vector>

namespace Photon
{
    class Debugger
    {
    private:
        static bool enabled;
        static std::string lastObject;
        static std::unordered_map<std::string, std::vector<std::pair<std::string, std::string>>> logs;

    public:
        static const std::pair<std::string, std::string> getLast(const std::string &);
        static void log(const std::string &, const std::string &, const std::string &);
        static void setEnabled(bool enabled);
    };
}

#endif //PHOTON_DEBUGGER_HPP
