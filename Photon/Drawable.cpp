#include "Renderer.hpp"

Photon::Drawable::Drawable() : updatable(false), visible(true), layer(DEFAULT_LAYER), material(std::make_shared<Material>()) {}

const std::shared_ptr<Photon::Material> &Photon::Drawable::getMaterial() const
{
    return material;
}

void Photon::Drawable::setMaterial(const std::shared_ptr<Photon::Material> &material)
{
    this->material = material;
}

void Photon::Drawable::setLayer(const std::string &layer)
{
    this->layer = layer;
}

const std::string &Photon::Drawable::getLayer() const
{
    return layer;
}

void Photon::Drawable::update() {}

void Photon::Drawable::load() {}

void Photon::Drawable::unload() {}

void Photon::Drawable::setVisibility(bool visible)
{
    this->visible = visible;
}

bool Photon::Drawable::isUpdatable() const
{
    return updatable;
}

void Photon::Drawable::setUpdatable(bool updatable)
{
    this->updatable = updatable;
}

bool Photon::Drawable::isPaused() const
{
    return paused;
}

void Photon::Drawable::setPaused(bool paused)
{
    this->paused = paused;
}
