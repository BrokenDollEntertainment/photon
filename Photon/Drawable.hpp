#ifndef PHOTON_DRAWABLE_HPP
#define PHOTON_DRAWABLE_HPP

#include <stack>
#include "Material.hpp"

namespace Photon
{
    class Layer;
    class Scene;
    class Drawable2;
    class Drawable3;
    class Drawable
    {
        friend class Layer;
        friend class Scene;
        friend class Drawable2;
        friend class Drawable3;
    private:
        bool updatable;
        bool paused;
        bool visible;
        std::string layer;

    protected:
        std::shared_ptr<Material> material;

    protected:
        Drawable();
        void setUpdatable(bool);

    private:
        virtual void draw() = 0;

    private:
        bool isPaused() const;
        bool isUpdatable() const;
        virtual void update();
        virtual void load();
        virtual void unload();

    public:
        const std::shared_ptr<Material> &getMaterial() const;
        void setMaterial(const std::shared_ptr<Material> &material);
        const std::string &getLayer() const;
        void setLayer(const std::string &);
        void setVisibility(bool);
        void setPaused(bool);
        virtual void render() = 0;
    };
}

#endif