#include "OpenGL.hpp"
#include "Renderer.hpp"
#include "Drawable2.hpp"

Photon::Drawable2::Drawable2() : Drawable(), rotation(0, 0), scale(1, 1), position(0, 0) {}

void Photon::Drawable2::linkPosition(const Photon::Vector2f &other)
{
    position.link(other);
}

void Photon::Drawable2::translate(float x, float y)
{
    position.setX(position.getX() + x);
    position.setY(position.getY() + y);
}

void Photon::Drawable2::setPosition(float x, float y)
{
    position.setX(x);
    position.setY(y);
}

void Photon::Drawable2::setRotation(float x, float y)
{
    rotations.set({0, 0}, {x, y});
}

void Photon::Drawable2::setScale(float x, float y)
{
    scale.setX(x);
    scale.setY(y);
}

void Photon::Drawable2::translate(const Vector2f &other)
{
    position.setX(position.getX() + other.getX());
    position.setY(position.getY() + other.getY());
}

void Photon::Drawable2::setPosition(const Vector2f &other)
{
    position = other;
}

void Photon::Drawable2::setRotation(const Vector2f &other)
{
    rotations.set({0, 0}, other);
}

void Photon::Drawable2::setScale(const Vector2f &other)
{
    scale = other;
}

void Photon::Drawable2::render()
{
    if (visible)
    {
        glPushMatrix();
        glTranslatef(position.getX(), position.getY(), 0);
        rotations.rotate(position);
        glScalef(scale.getX(), scale.getY(), 1);
        material->set();
        draw();
        material->unset();
        glPopMatrix();
    }
}

const Photon::Vector2f &Photon::Drawable2::getRotation() const
{
    return rotation;
}

const Photon::Vector2f &Photon::Drawable2::getScale() const
{
    return scale;
}

const Photon::Vector2f &Photon::Drawable2::getPosition() const
{
    return position;
}

Photon::Rotation2 &Photon::Drawable2::getRotations()
{
    return rotations;
}
