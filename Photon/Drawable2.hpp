#ifndef PHOTON_DRAWABLE2_HPP
#define PHOTON_DRAWABLE2_HPP

#include <memory>
#include <stack>
#include "Drawable.hpp"
#include "Rotation2.hpp"
#include "Vector2.hpp"

namespace Photon
{
    class Drawable2 : public Drawable
    {
    private:
        Rotation2 rotations;
        Vector2f rotation;
        Vector2f scale;
        Vector2f position;

    protected:
        Drawable2();

    public:
        Rotation2 &getRotations();
        const Vector2f &getRotation() const;
        const Vector2f &getScale() const;
        const Vector2f &getPosition() const;
        void linkPosition(const Vector2f &);
        void translate(float, float);
        void setPosition(float, float);
        void setRotation(float, float);
        void setScale(float, float);
        void translate(const Vector2f &);
        void setPosition(const Vector2f &);
        void setRotation(const Vector2f &);
        void setScale(const Vector2f &);
        void render() final;
    };
}

#endif