#include "OpenGL.hpp"
#include "Renderer.hpp"
#include "Drawable3.hpp"

Photon::Drawable3::Drawable3() : Drawable(), rotation(0, 0, 0), scale(1, 1, 1), position(0, 0, 0) {}

void Photon::Drawable3::linkPosition(const Photon::Vector3f &other)
{
    position.link(other);
}

void Photon::Drawable3::translate(float x, float y, float z)
{
    position.setX(position.getX() + x);
    position.setY(position.getY() + y);
    position.setZ(position.getZ() + z);
}

void Photon::Drawable3::setPosition(float x, float y, float z)
{
    position.setX(x);
    position.setY(y);
    position.setZ(z);
}

void Photon::Drawable3::setRotation(float x, float y, float z)
{
    rotations.set({0, 0, 0}, {x, y, z});
}

void Photon::Drawable3::setScale(float x, float y, float z)
{
    scale.setX(x);
    scale.setY(y);
    scale.setZ(z);
}

void Photon::Drawable3::translate(const Vector3f &other)
{
    position.setX(position.getX() + other.getX());
    position.setY(position.getY() + other.getY());
    position.setZ(position.getZ() + other.getZ());
}

void Photon::Drawable3::setPosition(const Vector3f &other)
{
    position = other;
}

void Photon::Drawable3::setRotation(const Vector3f &other)
{
    rotations.set({0, 0, 0}, other);
}

void Photon::Drawable3::setScale(const Vector3f &other)
{
    scale = other;
}

void Photon::Drawable3::render()
{
    if (visible)
    {
        glPushMatrix();
        glTranslatef(position.getX(), position.getY(), position.getZ());
        rotations.rotate(position);
        glScalef(scale.getX(), scale.getY(), scale.getZ());
        material->set();
        draw();
        material->unset();
        glPopMatrix();
    }
}

const Photon::Vector3f &Photon::Drawable3::getRotation() const
{
    return rotation;
}

const Photon::Vector3f &Photon::Drawable3::getScale() const
{
    return scale;
}

const Photon::Vector3f &Photon::Drawable3::getPosition() const
{
    return position;
}

Photon::Rotation3 &Photon::Drawable3::getRotations()
{
    return rotations;
}
