#ifndef PHOTON_DRAWABLE3_HPP
#define PHOTON_DRAWABLE3_HPP

#include <memory>
#include <stack>
#include "Drawable.hpp"
#include "Rotation3.hpp"
#include "Vector3.hpp"

namespace Photon
{
    class Drawable3 : public Drawable
    {
    private:
        Rotation3 rotations;
        Vector3f rotation;
        Vector3f scale;
        Vector3f position;

    protected:
        Drawable3();

    public:
        Rotation3 &getRotations();
        const Vector3f &getRotation() const;
        const Vector3f &getScale() const;
        const Vector3f &getPosition() const;
        void linkPosition(const Vector3f &);
        void translate(float, float, float);
        void setPosition(float, float, float);
        void setRotation(float, float, float);
        void setScale(float, float, float);
        void translate(const Vector3f &);
        void setPosition(const Vector3f &);
        void setRotation(const Vector3f &);
        void setScale(const Vector3f &);
        void render() final;
    };
}

#endif