#ifndef PHOTON_ENUM_HPP
#define PHOTON_ENUM_HPP

/**
 * General namespace containing all the graphical engine
 */
namespace Photon
{
    #define GUI_LAYER "Gui"
    #define DEFAULT_LAYER "Default"
    #define GUI_CAMERA "Gui"
    #define DEFAULT_CAMERA "Default"
    #define DEFAULT_SCENE "Default"
    enum struct MouseKey : int
    {
        NOCLICK,
        LEFTCLICK,
        MIDDLECLICK,
        RIGHTCLICK
    };

    enum struct Axis : int
    {
        XAXIS,
        YAXIS,
        ZAXIS,
        WHEELAXIS
    };

    enum struct Key : int
    {
        NONE = -2,
        UNKNOWN = -1,
        A = 0,
        B,
        C,
        D,
        E,
        F,
        G,
        H,
        I,
        J,
        K,
        L,
        M,
        N,
        O,
        P,
        Q,
        R,
        S,
        T,
        U,
        V,
        W,
        X,
        Y,
        Z,
        NUMKEY0,
        NUMKEY1,
        NUMKEY2,
        NUMKEY3,
        NUMKEY4,
        NUMKEY5,
        NUMKEY6,
        NUMKEY7,
        NUMKEY8,
        NUMKEY9,
        ESC,
        LCTRL,
        LSHIFT,
        LALT,
        LSYS,
        RCTRL,
        RSHIFT,
        RALT,
        RSYS,
        MENU,
        PERCENT,
        LBRACKET,
        RBRACKET,
        SEMICOLON,
        COLON,
        CAPSLOCK,
        EXCLAMATION,
        LESSTHAN,
        CARET,
        DOLLAR,
        COMMA,
        PERIOD,
        QUOTE,
        SLASH,
        BACKSLASH,
        TILDE,
        EQUAL,
        DASH,
        SPACE,
        BREAK,
        BACKSPACE,
        TAB,
        PGUP,
        PGDOWN,
        END,
        HOME,
        INSERT,
        DEL,
        ADD,
        SUB,
        MUL,
        EXPONENT,
        DIV,
        LEFT,
        RIGHT,
        UP,
        DOWN,
        NUMPAD0,
        NUMPAD1,
        NUMPAD2,
        NUMPAD3,
        NUMPAD4,
        NUMPAD5,
        NUMPAD6,
        NUMPAD7,
        NUMPAD8,
        NUMPAD9,
        F1,
        F2,
        F3,
        F4,
        F5,
        F6,
        F7,
        F8,
        F9,
        F10,
        F11,
        F12,
        F13,
        F14,
        F15,
        STOP,
        PAUSE,
        EXIT,
        KeyCount
    };

    enum struct ButtonState : int
    {
        RELEASED,
        PRESSED,
        NONE
    };

    enum struct ImageType : int
    {
        BMP,
        TGA,
        JPG,
        PNG
    };
}

#endif
