#include "Event_Event.hpp"

Photon::Event::Event::Event(Type eventType) : eventType(eventType), consumed(false) {}

Photon::Event::Event::Type Photon::Event::Event::getType()
{
    return eventType;
}

void Photon::Event::Event::consume()
{
    consumed = true;
}

bool Photon::Event::Event::isConsumed()
{
    return consumed;
}