#ifndef PHOTON_EVENT_EVENT_HPP
#define PHOTON_EVENT_EVENT_HPP

namespace Photon
{
    /**
     * Namespace containing all class for handling event
     */
    namespace Event
    {
        class Queue;
        /**
         * Class reprensenting an event
         *
         * @see Photon::Event::KeyboardEvent
         * @see Photon::Event::MouseEvent
         * @see Photon::Event::WindowEvent
         */
        class Event
        {
            friend class Queue;
        public:
            enum Type
            {
                KEYBOARD,
                MOUSE,
                WINDOW
            };

        private:
            Type eventType;
            bool consumed;

        private:
            bool isConsumed();

        protected:
            /**
             * Create an event
             *
             * @param eventType Type of the event
             */
            Event(Type eventType);

        public:
            /**
             * @return The type of the event
             */
            Type getType();
            /**
             * Specify that this event is consumed preventing it from being treated by other Handler
             */
            void consume();
        };
    }
}

#endif