#ifndef PHOTON_EVENT_HANDLER_HPP
#define PHOTON_EVENT_HANDLER_HPP

#include <memory>

namespace Photon
{
    namespace Event
    {
        /**
         * Class representing an Event handler
         *
         * @tparam T Type of the Event to handle
         */
        template <typename T>
        class Handler
        {
        private:
            bool paused;
            int priority;

        public:
            /**
             * Create an Handler
             *
             * @param priority Priority of the Handler
             */
            Handler(int priority) : paused(false), priority(priority) {}

            /**
             * @return true if the handler is paused
             */
            bool isPaused() const
            {
                return paused;
            }

            /**
             * Specify if the handler is paused
             *
             * @param paused New paused value
             */
            void setPaused(bool paused)
            {
                this->paused = paused;
            }

            /**
             * @return The priority value of the Handler
             */
            int getPriority() const
            {
                return priority;
            }

            /**
             * Handle the event
             *
             * @param event Event to handle
             */
            virtual void handle(const std::shared_ptr<T> &event) = 0;
        };
    }
}
#endif
