#include "Event_HandlerContainer.hpp"
#include "Event_Queue.hpp"

Photon::Event::HandlerContainer::HandlerContainer() : loaded(false), paused(false) {}

void Photon::Event::HandlerContainer::loadHandler()
{
    loaded = true;
    for (const auto &handler : keyboardHandlers)
        Queue::add(handler);
    for (const auto &handler : mouseHandlers)
        Queue::add(handler);
    for (const auto &handler : windowHandlers)
        Queue::add(handler);
}

void Photon::Event::HandlerContainer::unloadHandler()
{
    loaded = false;
    for (const auto &handler : keyboardHandlers)
        Queue::del(handler);
    for (const auto &handler : mouseHandlers)
        Queue::del(handler);
    for (const auto &handler : windowHandlers)
        Queue::del(handler);
}

void Photon::Event::HandlerContainer::addEventHandler(const std::shared_ptr<Handler<KeyboardEvent>> &handler)
{
    handler->setPaused(paused);
    keyboardHandlers.insert(handler);
    if (loaded)
        Queue::add(handler);
}

void Photon::Event::HandlerContainer::addEventHandler(const std::shared_ptr<Handler<MouseEvent>> &handler)
{
    handler->setPaused(paused);
    mouseHandlers.insert(handler);
    if (loaded)
        Queue::add(handler);
}

void Photon::Event::HandlerContainer::addEventHandler(const std::shared_ptr<Handler<WindowEvent>> &handler)
{
    handler->setPaused(paused);
    windowHandlers.insert(handler);
    if (loaded)
        Queue::add(handler);
}

std::shared_ptr<Photon::Event::Handler<Photon::Event::KeyboardEvent>> Photon::Event::HandlerContainer::addEventHandler(const std::function<void(const std::shared_ptr<KeyboardEvent> &)> &fct, int priority)
{
    auto handler = std::make_shared<HandlerWrapper<KeyboardEvent>>(fct, priority);
    addEventHandler(handler);
    return handler;
}

std::shared_ptr<Photon::Event::Handler<Photon::Event::MouseEvent>> Photon::Event::HandlerContainer::addEventHandler(const std::function<void(const std::shared_ptr<MouseEvent> &)> &fct, int priority)
{
    auto handler = std::make_shared<HandlerWrapper<MouseEvent>>(fct, priority);
    addEventHandler(handler);
    return handler;
}

std::shared_ptr<Photon::Event::Handler<Photon::Event::WindowEvent>> Photon::Event::HandlerContainer::addEventHandler(const std::function<void(const std::shared_ptr<WindowEvent> &)> &fct, int priority)
{
    auto handler = std::make_shared<HandlerWrapper<WindowEvent>>(fct, priority);
    addEventHandler(handler);
    return handler;
}

void Photon::Event::HandlerContainer::delEventHandler(const std::shared_ptr<Handler<KeyboardEvent>> &handler)
{
    keyboardHandlers.erase(handler);
    Queue::del(handler);
}

void Photon::Event::HandlerContainer::delEventHandler(const std::shared_ptr<Handler<MouseEvent>> &handler)
{
    mouseHandlers.erase(handler);
    Queue::del(handler);
}

void Photon::Event::HandlerContainer::delEventHandler(const std::shared_ptr<Handler<WindowEvent>> &handler)
{
    windowHandlers.erase(handler);
    Queue::del(handler);
}

bool Photon::Event::HandlerContainer::isLoaded() const
{
    return loaded;
}

bool Photon::Event::HandlerContainer::isPaused() const
{
    return paused;
}

void Photon::Event::HandlerContainer::setPaused(bool paused)
{
    this->paused = paused;
    for (const auto &handler : keyboardHandlers)
        handler->setPaused(paused);
    for (const auto &handler : mouseHandlers)
        handler->setPaused(paused);
    for (const auto &handler : windowHandlers)
        handler->setPaused(paused);

}
