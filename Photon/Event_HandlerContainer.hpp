#ifndef PHOTON_EVENT_HANDLERCONTAINER_HPP
#define PHOTON_EVENT_HANDLERCONTAINER_HPP

#include <set>
#include "Event_HandlerWrapper.hpp"
#include "Event_KeyboardEvent.hpp"
#include "Event_MouseEvent.hpp"
#include "Event_WindowEvent.hpp"

namespace Photon::Event
{
    /**
     * Class representing a container for Handler
     */
    class HandlerContainer
    {
    private:
        std::set<std::shared_ptr<Handler<KeyboardEvent>>> keyboardHandlers;
        std::set<std::shared_ptr<Handler<MouseEvent>>> mouseHandlers;
        std::set<std::shared_ptr<Handler<WindowEvent>>> windowHandlers;
        bool loaded;
        bool paused;

    protected:
        /**
         * Load all Handler in the Queue
         */
        void loadHandler();
        /**
         * Unload all Handler from the Queue
         */
        void unloadHandler();
        /**
         * @return True if all Handler have been loaded
         */
        bool isLoaded() const;

    public:
        /**
         * Create an HandlerContainer
         */
        HandlerContainer();
        /**
         * Add an Handler for KeyboardEvent to the container
         *
         * @param handler Handler to add
         */
        void addEventHandler(const std::shared_ptr<Handler<KeyboardEvent>> &handler);
        /**
         * Add an Handler for MouseEvent to the container
         *
         * @param handler Handler to add
         */
        void addEventHandler(const std::shared_ptr<Handler<MouseEvent>> &handler);
        /**
         * Add an Handler for WindowEvent to the container
         *
         * @param handler Handler to add
         */
        void addEventHandler(const std::shared_ptr<Handler<WindowEvent>> &handler);
        /**
         * Add an Handler for KeyboardEvent to the container
         *
         * @param fct Function to handle the event
         * @param priority Priority of the Handler
         * @return A pointer to the newly created Handler
         */
        std::shared_ptr<Handler<KeyboardEvent>> addEventHandler(const std::function<void(const std::shared_ptr<KeyboardEvent> &)> &fct, int priority = 0);
        /**
         * Add an Handler for MouseEvent to the container
         *
         * @param fct Function to handle the event
         * @param priority Priority of the Handler
         * @return A pointer to the newly created Handler
         */
        std::shared_ptr<Handler<MouseEvent>> addEventHandler(const std::function<void(const std::shared_ptr<MouseEvent> &)> &fct, int priority = 0);
        /**
         * Add an Handler for WindowEvent to the container
         *
         * @param fct Function to handle the event
         * @param priority Priority of the Handler
         * @return A pointer to the newly created Handler
         */
        std::shared_ptr<Handler<WindowEvent>> addEventHandler(const std::function<void(const std::shared_ptr<WindowEvent> &)> &fct, int priority = 0);
        /**
         * Remove an Handler for KeyboardEvent from the container
         *
         * @param handler Handler to remove
         */
        void delEventHandler(const std::shared_ptr<Handler<KeyboardEvent>> &handler);
        /**
         * Remove an Handler for MouseEvent from the container
         *
         * @param handler Handler to remove
         */
        void delEventHandler(const std::shared_ptr<Handler<MouseEvent>> &handler);
        /**
         * Remove an Handler for WindowEvent from the container
         *
         * @param handler Handler to remove
         */
        void delEventHandler(const std::shared_ptr<Handler<WindowEvent>> &handler);
        /**
         * @return True if the container is paused
         */
        bool isPaused() const;
        /**
         * Specify if the container and all its Handler are paused
         *
         * @param paused New paused value
         */
        void setPaused(bool paused);
    };
}

#endif
