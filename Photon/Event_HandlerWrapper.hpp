#ifndef PHOTON_EVENT_HANDLERWRAPPER_HPP
#define PHOTON_EVENT_HANDLERWRAPPER_HPP

#include <functional>
#include "Event_Handler.hpp"

namespace Photon
{
    namespace Event
    {
        /**
         * Class that wrap function into Handler
         *
         * @tparam T Type of the Event to handle
         */
        template <typename T>
        class HandlerWrapper : public Handler<T>
        {
        private:
            std::function<void(const std::shared_ptr<T> &)> handler;
        public:
            /**
             * Create a HandlerWrapper
             *
             * @param handler Function to wrap into an Handler
             * @param priority Priority of the Handler
             */
            HandlerWrapper(const std::function<void(const std::shared_ptr<T> &)> &handler, int priority) : Handler<T>(priority), handler(handler) {}

            /**
             * Handle the event by calling the wrapped function
             *
             * @param event Event to handle
             */
            void handle(const std::shared_ptr<T> &event)
            {
                handler(event);
            }
        };
    }
}
#endif
