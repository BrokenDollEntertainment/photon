#include "Event_KeyboardEvent.hpp"

Photon::Event::KeyboardEvent::KeyboardEvent(Key key, ButtonState state) :
    Event(Event::Type::KEYBOARD), redundant(false), key(key), state(state) {}

Photon::Key Photon::Event::KeyboardEvent::getKey() const
{
    return key;
}

Photon::ButtonState Photon::Event::KeyboardEvent::getState() const
{
    return state;
}

bool Photon::Event::KeyboardEvent::isRedundant() const
{
    return redundant;
}

void Photon::Event::KeyboardEvent::setRedundant(bool redundant)
{
    this->redundant = redundant;
}