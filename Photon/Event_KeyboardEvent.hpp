#ifndef PHOTON_EVENT_KEYBOARDEVENT_HPP
#define PHOTON_EVENT_KEYBOARDEVENT_HPP

#include "Enum.hpp"
#include "Event_Event.hpp"

namespace Photon
{
    namespace Event
    {
        class Queue;
        /**
         * Class representing a keyboard event
         */
        class KeyboardEvent : public Event
        {
            friend class Queue;
        private:
            bool redundant;
            Key key;
            ButtonState state;

        private:
            void setRedundant(bool);

        public:
            /**
             * Create a KeyboardEvent
             *
             * @param key Key generating the event
             * @param state State of the key generating the event
             */
            KeyboardEvent(Key key, ButtonState state);
            /**
             * @return True if the event is redundant
             */
            bool isRedundant() const;
            /**
             * @return The key generating this event
             */
            Key getKey() const;
            /**
             * @return The state of the key
             */
            ButtonState getState() const;
        };
    }
}

#endif
