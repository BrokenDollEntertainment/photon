#include "Event_MouseEvent.hpp"

Photon::Event::MouseEvent::MouseEvent(float x, float y, Photon::MouseKey key, Photon::ButtonState state, short delta) :
    Event(Event::Type::MOUSE),
    mousePosition(x, y),
    key(key),
    state(state),
    wheelDelta(delta) {}

const Photon::Vector2f &Photon::Event::MouseEvent::getPosition() const
{
    return mousePosition;
}

Photon::MouseKey Photon::Event::MouseEvent::getKey() const
{
    return key;
}

Photon::ButtonState Photon::Event::MouseEvent::getState() const
{
    return state;
}

short Photon::Event::MouseEvent::getWheelDelta() const
{
    return wheelDelta;
}