#ifndef PHOTON_EVENT_MOUSEEVENT_HPP
#define PHOTON_EVENT_MOUSEEVENT_HPP

#include "Enum.hpp"
#include "Event_Event.hpp"
#include "Vector2.hpp"

namespace Photon
{
    namespace Event
    {
        /**
         * Class representing a mouse event
         */
        class MouseEvent : public Event
        {
        protected:
            Vector2f mousePosition;
            MouseKey key;
            ButtonState state;
            short wheelDelta;

        public:
            /**
             * Create a MouseEvent
             *
             * @param x X position of the mouse on the screen
             * @param y Y position of the mouse on the screen
             * @param key Mouse button that possibly generate a click
             * @param state State of this mouse button (ButtonState::NONE if no click event)
             * @param delta Delta of the mouse wheel
             */
            MouseEvent(float x, float y, MouseKey key, ButtonState state, short delta);
            /**
             * @return The position of the mouse
             */
            const Vector2f &getPosition() const;
            /**
             * @return The mouse key that possibly generte an event
             */
            MouseKey getKey() const;
            /**
             * @return The state of the mouse key
             */
            ButtonState getState() const;
            /**
             * @return The delta of the mouse wheel
             */
            short getWheelDelta() const;
        };
    }
}

#endif
