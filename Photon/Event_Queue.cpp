#include "Event_Queue.hpp"
#include "Keyboard.hpp"
#include "Mouse.hpp"

Photon::SortedList<std::shared_ptr<Photon::Event::Handler<Photon::Event::KeyboardEvent>>> Photon::Event::Queue::keyboardHandlers;
Photon::SortedList<std::shared_ptr<Photon::Event::Handler<Photon::Event::MouseEvent>>> Photon::Event::Queue::mouseHandlers;
Photon::SortedList<std::shared_ptr<Photon::Event::Handler<Photon::Event::WindowEvent>>> Photon::Event::Queue::windowHandlers;
std::unordered_map<Photon::Key, int> Photon::Event::Queue::keyboardReleaser;

void Photon::Event::Queue::submit(const std::shared_ptr<KeyboardEvent> &event)
{
    if (event->getState() == ButtonState::PRESSED)
        keyboardReleaser[event->getKey()] = 3;
    event->setRedundant(event->getState() == Keyboard::getKeyState(event->getKey()));
    Keyboard::submitEvent(event);
    for (const auto &handler : keyboardHandlers)
    {
        if (!handler->isPaused())
        {
            handler->handle(event);
            if (event->isConsumed())
                return;
        }
    }
}

void Photon::Event::Queue::update()
{
    auto mousePos = Mouse::getRawPosition();
    auto event = std::make_shared<MouseEvent>(mousePos.getX(), mousePos.getY(), MouseKey::NOCLICK, ButtonState::NONE, 0);
    submit(event);
    for (auto &pair : keyboardReleaser)
    {
        if (pair.second > 0)
        {
            --pair.second;
            if (pair.second == 0)
                submit(std::make_shared<KeyboardEvent>(pair.first, ButtonState::RELEASED));
        }
    }
}

void Photon::Event::Queue::setWinDimension(int width, int height)
{
    Mouse::setWinDimension(width, height);
}

void Photon::Event::Queue::submit(const std::shared_ptr<MouseEvent> &event)
{
    Mouse::submitEvent(event);
    for (const auto &handler : mouseHandlers)
    {
        if (!handler->isPaused())
        {
            handler->handle(event);
            if (event->isConsumed())
                return;
        }
    }
}

void Photon::Event::Queue::submit(const std::shared_ptr<WindowEvent> &event)
{
    for (const auto &handler : windowHandlers)
    {
        if (!handler->isPaused())
        {
            handler->handle(event);
            if (event->isConsumed())
                return;
        }
    }
}

void Photon::Event::Queue::add(const std::shared_ptr<Handler<KeyboardEvent>> &handler)
{
    keyboardHandlers.add(handler, -handler->getPriority());
}

void Photon::Event::Queue::add(const std::shared_ptr<Handler<MouseEvent>> &handler)
{
    mouseHandlers.add(handler, -handler->getPriority());
}

void Photon::Event::Queue::add(const std::shared_ptr<Handler<WindowEvent>> &handler)
{
    windowHandlers.add(handler, -handler->getPriority());
}

void Photon::Event::Queue::del(const std::shared_ptr<Handler<KeyboardEvent>> &handler)
{
    keyboardHandlers.remove(handler);
}

void Photon::Event::Queue::del(const std::shared_ptr<Handler<MouseEvent>> &handler)
{
    mouseHandlers.remove(handler);
}

void Photon::Event::Queue::del(const std::shared_ptr<Handler<WindowEvent>> &handler)
{
    windowHandlers.remove(handler);
}