#ifndef PHOTON_EVENT_QUEUE_HPP
#define PHOTON_EVENT_QUEUE_HPP

#include <set>
#include <unordered_map>
#include "SortedList.hpp"
#include "Event_Handler.hpp"
#include "Event_KeyboardEvent.hpp"
#include "Event_MouseEvent.hpp"
#include "Event_WindowEvent.hpp"

namespace Photon
{
    class Renderer;
    namespace Event
    {
        class HandlerContainer;
        /**
         * Class that enqueue all the submitted event and handle them with their Handler
         */
        class Queue
        {
            friend class Photon::Renderer;
            friend class HandlerContainer;
        private:
            static SortedList<std::shared_ptr<Handler<KeyboardEvent>>> keyboardHandlers;
            static SortedList<std::shared_ptr<Handler<MouseEvent>>> mouseHandlers;
            static SortedList<std::shared_ptr<Handler<WindowEvent>>> windowHandlers;
            static std::unordered_map<Photon::Key, int> keyboardReleaser;

        private:
            static void update();
            static void add(const std::shared_ptr<Handler<KeyboardEvent>> &);
            static void add(const std::shared_ptr<Handler<MouseEvent>> &);
            static void add(const std::shared_ptr<Handler<WindowEvent>> &);
            static void del(const std::shared_ptr<Handler<KeyboardEvent>> &);
            static void del(const std::shared_ptr<Handler<MouseEvent>> &);
            static void del(const std::shared_ptr<Handler<WindowEvent>> &);
            static void submit(const std::shared_ptr<KeyboardEvent> &event);
            static void submit(const std::shared_ptr<MouseEvent> &event);
            static void submit(const std::shared_ptr<WindowEvent> &event);
            static void setWinDimension(int, int);
        };
    }
}

#endif
