#include "Event_WindowEvent.hpp"

Photon::Event::WindowEvent::WindowEvent() : Event(Event::Type::WINDOW), type(UNKNOWN) {}

Photon::Event::WindowEvent::Type Photon::Event::WindowEvent::getType() const
{
    return type;
}