#ifndef PHOTON_EVENT_WINDOWEVENT_HPP
#define PHOTON_EVENT_WINDOWEVENT_HPP

#include "Event_Event.hpp"

namespace Photon
{
    namespace Event
    {
        /**
         * Class representing a window event
         *
         * @warning Class is not used for the moment
         */
        class WindowEvent : public Event
        {
        public:
            enum Type
            {
                FOCUSIN,
                FOCUSOUT,
                SHOW,
                HIDE,
                CLOSE,
                UNKNOWN
            };

        protected:
            WindowEvent::Type type;

        public:
            /**
             * Create an UNKNOWN WindowEvent
             */
            WindowEvent();
            /**
             * @return The type of WindowEvent
             */
            WindowEvent::Type getType() const;
        };
    }
}

#endif
