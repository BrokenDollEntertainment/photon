#ifndef PHOTON_GLUTIL_HPP
#define PHOTON_GLUTIL_HPP

#include "OpenGL.hpp"

#ifdef _WIN32
    #include <GL/glu.h>
#elif __APPLE__
    #include <OpenGL/glu.h>
#elif __linux__
    #include <GL/glu.h>
#endif

#endif
