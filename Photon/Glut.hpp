#ifndef PHOTON_GLUT_HPP
#define PHOTON_GLUT_HPP

#include "GLUtil.hpp"

#ifdef _WIN32
#define FREEGLUT_STATIC
#define _LIB
#define FREEGLUT_LIB_PRAGMAS 0
    #include <GL/glut.h>
#elif __APPLE__
    #include <OpenGL/glut.h>
#elif __linux__
    #include <GL/glut.h>
#endif

#endif
