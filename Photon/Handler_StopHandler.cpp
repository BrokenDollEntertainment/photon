#include "Handler_StopHandler.hpp"
#include "Renderer.hpp"

Photon::Handler::StopHandler::StopHandler(const std::set<Photon::Key> &keys) : Handler(2), keys(keys) {}

void Photon::Handler::StopHandler::handle(const std::shared_ptr<Photon::Event::KeyboardEvent> &event)
{
    if (!event->isRedundant() && event->getState() == ButtonState::PRESSED && keys.find(event->getKey()) != keys.end())
    {
        event->consume();
        Photon::Renderer::stop();
    }
}

