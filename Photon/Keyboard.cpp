#include "Keyboard.hpp"

std::unordered_map<Photon::Key, Photon::ButtonState> Photon::Keyboard::keyboardStates;

void Photon::Keyboard::submitEvent(const std::shared_ptr<Event::KeyboardEvent> &event)
{
    keyboardStates[event->getKey()] = event->getState();
}

bool Photon::Keyboard::isKeyPressed(Key key)
{
    if (keyboardStates.find(key) != keyboardStates.end())
        return keyboardStates.at(key) == ButtonState::PRESSED;
    return false;
}

Photon::ButtonState Photon::Keyboard::getKeyState(Photon::Key key)
{
    if (keyboardStates.find(key) != keyboardStates.end())
        return keyboardStates.at(key);
    return ButtonState::NONE;
}