#ifndef PHOTON_KEYBOARD_HPP
#define PHOTON_KEYBOARD_HPP

#include <memory>
#include <unordered_map>
#include "Enum.hpp"
#include "Event_KeyboardEvent.hpp"

namespace Photon
{
    namespace Event
    {
        class Queue;
    }
    class Keyboard
    {
        friend class Event::Queue;
    private:
        static std::unordered_map<Key, ButtonState> keyboardStates;

    private:
        static void submitEvent(const std::shared_ptr<Event::KeyboardEvent> &);

    public:
        static bool isKeyPressed(Key);
        static ButtonState getKeyState(Key);
    };
}

#endif
