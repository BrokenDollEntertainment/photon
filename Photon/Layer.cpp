#include "Layer.hpp"

Photon::Layer::Layer(const std::string &name) : loaded(false), name(name) {}

const std::string &Photon::Layer::getName() const
{
    return name;
}

void Photon::Layer::load()
{
    if (!loaded)
    {
        loaded = true;
        for (const auto &drawable : drawables)
            drawable->load();
    }
}

void Photon::Layer::unload()
{
    loaded = false;
    for (const auto &drawable : drawables)
        drawable->unload();
    drawables.clear();
}

void Photon::Layer::addDrawable(const std::shared_ptr<Drawable> &drawable)
{
    if (loaded)
        drawable->load();
    drawables.emplace_back(drawable);
}

void Photon::Layer::render()
{
    for (auto &drawable : drawables)
        drawable->render();
}
