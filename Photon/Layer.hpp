#ifndef PHOTON_LAYER_HPP
#define PHOTON_LAYER_HPP

#include <string>
#include <vector>
#include "Drawable.hpp"

namespace Photon
{
    class Camera;
    class Scene;
    class Layer
    {
        friend class Camera;
        friend class Scene;
    private:
        bool loaded;
        std::string name;
        std::vector<std::shared_ptr<Drawable>> drawables;

    private:
        void load();
        void unload();
        void render();
        void update();

    public:
        Layer(const std::string &name);
        void addDrawable(const std::shared_ptr<Drawable> &);
        const std::string &getName() const;
    };
}

#endif
