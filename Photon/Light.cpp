#include "OpenGL.hpp"
#include "Light.hpp"

Photon::Light::Light(unsigned int idx) :
    finite(false),
    spotDirection(0, 0, 1),
    spotAngle(180),
    spotIntensity(0),
    idx(idx),
    on(false),
    ambientColor(Color::BLACK),
    diffuseColor(Color::WHITE),
    specularColor(Color::WHITE),
    constantAttenuation(1),
    linearAttenuation(0),
    quadraticAttenuation(0) {}

void Photon::Light::update()
{
    if (on)
    {
        glEnable(idx);
        GLfloat ambient[] = {ambientColor.getR() / 255, ambientColor.getG() / 255, ambientColor.getB() / 255, ambientColor.getA() / 255};
        glLightfv(idx, GL_AMBIENT, ambient);
        GLfloat diffuse[] = {diffuseColor.getR() / 255, diffuseColor.getG() / 255, diffuseColor.getB() / 255, diffuseColor.getA() / 255};
        glLightfv(idx, GL_DIFFUSE, diffuse);
        GLfloat specular[] = {specularColor.getR() / 255, specularColor.getG() / 255, specularColor.getB() / 255, specularColor.getA() / 255};
        glLightfv(idx, GL_SPECULAR, specular);
        if (finite)
        {
            GLfloat lightPosition[] = {position.getX(), position.getY(), position.getZ(), 1.0};
            glLightfv(idx, GL_POSITION, lightPosition);
        }
        else
        {
            GLfloat lightPosition[] = {position.getX(), position.getY(), position.getZ(), 0.0};
            glLightfv(idx, GL_POSITION, lightPosition);
        }
        GLfloat lightDirection[] = {spotDirection.getX(), spotDirection.getY(), spotDirection.getZ()};
        glLightfv(idx, GL_SPOT_DIRECTION, lightDirection);
        glLightf(idx, GL_SPOT_CUTOFF, spotAngle);
        glLightf(idx, GL_SPOT_EXPONENT, spotIntensity);
        glLightf(idx, GL_CONSTANT_ATTENUATION, constantAttenuation);
        glLightf(idx, GL_LINEAR_ATTENUATION, linearAttenuation);
        glLightf(idx, GL_QUADRATIC_ATTENUATION, quadraticAttenuation);
    }
    else
        glDisable(idx);
}

bool Photon::Light::isOn() const
{
    return on;
}

void Photon::Light::setOn(bool on)
{
    this->on = on;
}

const Photon::Vector3f &Photon::Light::getPosition() const
{
    return position;
}

void Photon::Light::linkPosition(const Photon::Vector3f &position)
{
    this->position.link(position);
}

void Photon::Light::setPosition(const Photon::Vector3f &position)
{
    this->position = position;
}

const Photon::Vector3f &Photon::Light::getSpotDirection() const
{
    return spotDirection;
}

void Photon::Light::setSpotDirection(const Photon::Vector3f &spotDirection)
{
    this->spotDirection = spotDirection;
}

float Photon::Light::getSpotAngle() const
{
    return spotAngle;
}

void Photon::Light::setSpotAngle(float spotAngle)
{
    this->spotAngle = spotAngle;
}

float Photon::Light::getSpotIntensity() const
{
    return spotIntensity;
}

void Photon::Light::setSpotIntensity(float spotIntensity)
{
    this->spotIntensity = spotIntensity;
}

const Photon::Color &Photon::Light::getAmbientColor() const
{
    return ambientColor;
}

void Photon::Light::setAmbientColor(const Photon::Color &ambientColor)
{
    this->ambientColor = ambientColor;
}

const Photon::Color &Photon::Light::getDiffuseColor() const
{
    return diffuseColor;
}

void Photon::Light::setDiffuseColor(const Photon::Color &diffuseColor)
{
    this->diffuseColor = diffuseColor;
}

const Photon::Color &Photon::Light::getSpecularColor() const
{
    return specularColor;
}

void Photon::Light::setSpecularColor(const Photon::Color &specularColor)
{
    this->specularColor = specularColor;
}

void Photon::Light::setColor(const Photon::Color &color)
{
    ambientColor = color;
    diffuseColor = color;
    specularColor = color;
}

bool Photon::Light::isFinite() const
{
    return finite;
}

void Photon::Light::setFinite(bool finite)
{
    this->finite = finite;
}
