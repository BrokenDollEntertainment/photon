#ifndef PHOTON_LIGHT_HPP
#define PHOTON_LIGHT_HPP

#include "Vector3.hpp"
#include "Color.hpp"

namespace Photon
{
    class LightManager;
    class Light
    {
        friend class LightManager;
    private:
        bool finite;
        Vector3f spotDirection;
        float spotAngle;
        float spotIntensity;
        unsigned int idx;
        bool on;
        Vector3f position;
        Color ambientColor;
        Color diffuseColor;
        Color specularColor;
        float constantAttenuation;
        float linearAttenuation;
        float quadraticAttenuation;

    private:
        Light(unsigned int idx);

    public:
        Light(const Light &) = default;
        bool isOn() const;
        void setOn(bool on);
        const Vector3f &getPosition() const;
        void linkPosition(const Vector3f &);
        void setPosition(const Vector3f &);
        bool isFinite() const;
        void setFinite(bool);
        const Vector3f &getSpotDirection() const;
        void setSpotDirection(const Vector3f &);
        float getSpotAngle() const;
        void setSpotAngle(float);
        float getSpotIntensity() const;
        void setSpotIntensity(float);
        const Color &getAmbientColor() const;
        void setAmbientColor(const Color &);
        const Color &getDiffuseColor() const;
        void setDiffuseColor(const Color &);
        const Color &getSpecularColor() const;
        void setSpecularColor(const Color &);
        void setColor(const Color &);
        void update();
    };
}

#endif