#include "Debugger.hpp"
#include "OpenGL.hpp"
#include "LightManager.hpp"

bool Photon::LightManager::activate = false;
bool Photon::LightManager::enabled = false;
const std::array<std::shared_ptr<Photon::Light>, 8> Photon::LightManager::lights = {
        std::make_shared<Light>(Light(GL_LIGHT0)),
        std::make_shared<Light>(Light(GL_LIGHT1)),
        std::make_shared<Light>(Light(GL_LIGHT2)),
        std::make_shared<Light>(Light(GL_LIGHT3)),
        std::make_shared<Light>(Light(GL_LIGHT4)),
        std::make_shared<Light>(Light(GL_LIGHT5)),
        std::make_shared<Light>(Light(GL_LIGHT6)),
        std::make_shared<Light>(Light(GL_LIGHT7))
};

bool Photon::LightManager::isActive()
{
    for (const auto &light : lights)
    {
        if (light->isOn())
            return true;
    }
    return false;
}

void Photon::LightManager::enable()
{
    enabled = true;
}

void Photon::LightManager::disable()
{
    enabled = false;
}

void Photon::LightManager::enableLight()
{
    if (!activate && enabled)
    {
        glEnable(GL_LIGHTING);
        glEnable(GL_COLOR_MATERIAL);
        glEnable(GL_NORMALIZE);
        for (const auto &light : lights)
            light->update();
        activate = true;
    }
}

void Photon::LightManager::disableLight()
{
    glDisable(GL_NORMALIZE);
    glDisable(GL_LIGHTING);
    glDisable(GL_COLOR_MATERIAL);
    glDisable(GL_LIGHT0);
    glDisable(GL_LIGHT1);
    glDisable(GL_LIGHT2);
    glDisable(GL_LIGHT3);
    glDisable(GL_LIGHT4);
    glDisable(GL_LIGHT5);
    glDisable(GL_LIGHT6);
    glDisable(GL_LIGHT7);
    activate = false;
}

const std::shared_ptr<Photon::Light> Photon::LightManager::get(int idx)
{
    if (idx >= 8)
    {
        Debugger::log("Photon::LightManager", "get", "Index out of bound, please set an index between 0 and 7");
        return nullptr;
    }
    return lights[idx];
}

void Photon::LightManager::setSmoothLight(bool smoothLight)
{
    if (smoothLight)
        glShadeModel(GL_SMOOTH);
    else
        glShadeModel(GL_FLAT);
}
