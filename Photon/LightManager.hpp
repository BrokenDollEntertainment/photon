#ifndef PHOTON_LIGHTMANAGER_HPP
#define PHOTON_LIGHTMANAGER_HPP

#include <array>
#include <memory>
#include "Light.hpp"

namespace Photon
{
    class Camera3;
    class Material;
    class LightManager
    {
        friend class Camera3;
        friend class Material;
    private:
        static bool activate;
        static bool enabled;
        static const std::array<std::shared_ptr<Light>, 8> lights;

    private:
        static void enableLight();
        static void disableLight();


    public:
        static void setSmoothLight(bool);
        static bool isActive();
        static void enable();
        static void disable();
        static const std::shared_ptr<Light> get(int idx);
    };
}

#endif