#include "OpenGL.hpp"
#include "Material.hpp"
#include "LightManager.hpp"

Photon::Material::Material() :
    type(FILLED),
    lastBinded(0),
    defaultTexture(Color::WHITE),
    ka(Color::BLACK),
    kd(Color::BLACK),
    ks(Color::BLACK),
    emission(Color::BLACK),
    lineWidth(1),
    shininess(0),
    lightSensitivity(true) {}

void Photon::Material::set()
{
    defaultTexture.color(0);
    GLfloat ambientColor[] = {ka.getR() / 255, ka.getG() / 255, ka.getB() / 255, ka.getA() / 255};
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ambientColor);
    GLfloat diffuseColor[] = {kd.getR() / 255, kd.getG() / 255, kd.getB() / 255, kd.getA() / 255};
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diffuseColor);
    GLfloat specularColor[] = {ks.getR() / 255, ks.getG() / 255, ks.getB() / 255, ks.getA() / 255};
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specularColor);
    GLfloat shininessValue = 128 * shininess;
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shininessValue);
    GLfloat emissionColor[] = {emission.getR() / 255, emission.getG() / 255, emission.getB() / 255, emission.getA() / 255};
    glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, emissionColor);
    if (!lightSensitivity)
        LightManager::disableLight();
    glLineWidth(lineWidth);
    if (type == Material::WIRED)
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    else
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    if (!textures.empty())
        glPushAttrib(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Photon::Material::bindTexture(unsigned long idx)
{
    if (textures.empty())
        return;
    rawIndex = idx;
    lastBinded = idx % textures.size();
    textures[lastBinded].bind();
}

void Photon::Material::unbindTexture()
{
    if (textures.empty())
        return;
    textures[lastBinded].unbind();
}

void Photon::Material::begin(unsigned int mode)
{
    if (type == POINTS)
        glBegin(GL_POINTS);
    else
        glBegin(mode);
    textureBinded = false;
}

void Photon::Material::begin(unsigned int mode, unsigned long idx)
{
    bindTexture(idx);
    begin(mode);
    textureBinded = true;
}

void Photon::Material::end()
{
    glEnd();
    if (textureBinded)
        unbindTexture();
}

void Photon::Material::point(float x, float y, unsigned int color)
{
    if (textures.empty())
    {
        defaultTexture.coord(x, y);
        defaultTexture.color(color);
        return;
    }
    const auto &texture = textures[lastBinded];
    if (!texture.isDefault())
    {
        texture.coord(x, y);
        texture.color(color);
    }
    else
    {
        defaultTexture.coord(x, y);
        defaultTexture.color(color);
    }
}

void Photon::Material::point(const float arr[2], unsigned int color)
{
    if (textures.empty())
    {
        defaultTexture.coord(arr);
        defaultTexture.color(color);
        return;
    }
    const auto &texture = textures[lastBinded];
    if (!texture.isDefault())
    {
        texture.coord(arr);
        texture.color(color);
    }
    else
    {
        defaultTexture.coord(arr);
        defaultTexture.color(color);
    }
}

void Photon::Material::unset()
{
    if (!lightSensitivity)
        LightManager::enableLight();
}

Photon::Material::MaterialType Photon::Material::getType() const
{
    return type;
}

void Photon::Material::setType(Photon::Material::MaterialType type)
{
    this->type = type;
}

const Photon::Texture::Texture Photon::Material::getTexture(unsigned long idx) const
{
    if (textures.empty())
        return Texture::Texture();
    return textures[idx % textures.size()];
}

void Photon::Material::addTexture(const std::vector<Photon::Texture::Texture> &textures)
{
    for (const auto &texture : textures)
        addTexture(texture);
}

void Photon::Material::addTexture(const Photon::Texture::Texture &texture)
{
    textures.emplace_back(texture);
}

void Photon::Material::removeTexture(const Texture::Texture &toRemove)
{
    for (unsigned long n = 0; n != textures.size(); ++n)
    {
        if (textures[n] == toRemove)
        {
            removeTexture(n);
            return;
        }
    }
}

void Photon::Material::removeTexture(unsigned long idx)
{
    textures.erase(textures.begin() + (idx % textures.size()));
}

void Photon::Material::clearTextures()
{
    textures.clear();
}

const Photon::Color Photon::Material::getColor() const
{
    return defaultTexture.getColor(0);
}

void Photon::Material::setColor(const Photon::Color &color)
{
    defaultTexture.clearColor();
    defaultTexture.addColor(color);
}

float Photon::Material::getLineWidth() const
{
    return lineWidth;
}

void Photon::Material::setLineWidth(float lineWidth)
{
    this->lineWidth = lineWidth;
}

float Photon::Material::getShininess() const
{
    return shininess;
}

void Photon::Material::setShininess(float shininess)
{
    while (shininess > 100)
        shininess -= 100;
    while (shininess < 0)
        shininess += 100;
    this->shininess = (shininess / 100);
}

bool Photon::Material::isLightSensitive() const
{
    return lightSensitivity;
}

void Photon::Material::setLightSensitivity(bool lightSensitivity)
{
    Material::lightSensitivity = lightSensitivity;
}

const Photon::Color &Photon::Material::getKa() const
{
    return ka;
}

void Photon::Material::setKa(const Photon::Color &ka)
{
    this->ka = ka;
}

const Photon::Color &Photon::Material::getKd() const
{
    return kd;
}

void Photon::Material::setKd(const Photon::Color &kd)
{
    this->kd = kd;
}

const Photon::Color &Photon::Material::getKs() const
{
    return ks;
}

void Photon::Material::setKs(const Photon::Color &ks)
{
    this->ks = ks;
}

const Photon::Color &Photon::Material::getEmission() const
{
    return emission;
}

void Photon::Material::setEmission(const Photon::Color &emission)
{
    this->emission = emission;
}
