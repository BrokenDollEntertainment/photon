#ifndef PHOTON_MATERIAL_HPP
#define PHOTON_MATERIAL_HPP

#include "Texture_Texture.hpp"

namespace Photon
{
    class Material
    {
    public:
        enum MaterialType : int
        {
            WIRED,
            FILLED,
            POINTS
        };
    private:
        MaterialType type;
        unsigned long lastBinded;
        unsigned long rawIndex;
        std::vector<Texture::Texture> textures;
        Texture::Texture defaultTexture;
        Color ka;
        Color kd;
        Color ks;
        Color emission;
        float lineWidth;
        float shininess;
        bool lightSensitivity;
        bool textureBinded;

    public:
        Material();
        Material(const Material &) = default;
        MaterialType getType() const;
        void setType(MaterialType);
        const Texture::Texture getTexture(unsigned long) const;
        void addTexture(const std::vector<Texture::Texture> &);
        void addTexture(const Texture::Texture &);
        void removeTexture(const Texture::Texture &);
        void removeTexture(unsigned long);
        void clearTextures();
        void bindTexture(unsigned long);
        void unbindTexture();
        void begin(unsigned int);
        void begin(unsigned int, unsigned long);
        void end();
        void point(float, float, unsigned int);
        void point(const float [2], unsigned int);
        const Color getColor() const;
        void setColor(const Color &);
        const Color &getKa() const;
        void setKa(const Color &);
        const Color &getKd() const;
        void setKd(const Color &);
        const Color &getKs() const;
        void setKs(const Color &);
        const Color &getEmission() const;
        void setEmission(const Color &);
        float getLineWidth() const;
        void setLineWidth(float);
        float getShininess() const;
        void setShininess(float shininess);
        bool isLightSensitive() const;
        void setLightSensitivity(bool lightSensitivity);
        void set();
        void unset();
    };
}

#endif
