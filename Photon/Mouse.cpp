#include "Glut.hpp"
#include "Mouse.hpp"

int Photon::Mouse::winWidth;
int Photon::Mouse::winHeight;
Photon::Vector2f Photon::Mouse::position(0, 0);
Photon::Vector2f Photon::Mouse::rawPosition(0, 0);
std::unordered_map<Photon::MouseKey, bool> Photon::Mouse::mouseStates;
std::unordered_map<Photon::Axis, float> Photon::Mouse::mouseAxis;

void Photon::Mouse::submitEvent(const std::shared_ptr<Event::MouseEvent> &event)
{
    auto newPosition = event->getPosition();
    mouseAxis[Axis::WHEELAXIS] = event->getWheelDelta();
    mouseAxis[Axis::YAXIS] = newPosition.getY() - rawPosition.getY();
    mouseAxis[Axis::XAXIS] = newPosition.getX() - rawPosition.getX();
    position = newPosition;
    position.setY(winHeight - position.getY());
    rawPosition = newPosition;
    if (event->getState() != ButtonState::NONE)
        mouseStates[event->getKey()] = (event->getState() == ButtonState::PRESSED);
}

void Photon::Mouse::setCursorVisibility(bool visibility)
{
    if (visibility)
        glutSetCursor(GLUT_CURSOR_INHERIT);
    else
        glutSetCursor(GLUT_CURSOR_NONE);
}

void Photon::Mouse::moveCursor(const Photon::Vector2f &pos)
{
	glutWarpPointer(pos.getX(), winHeight - pos.getY());
    position = pos;
    position.setY(winHeight - position.getY());
	rawPosition = pos;
}

void Photon::Mouse::centerCursor()
{
    moveCursor({winWidth / 2.f, winHeight / 2.f});
}

void Photon::Mouse::setWinDimension(int width, int height)
{
    winWidth = width;
    winHeight = height;
}

const Photon::Vector2f &Photon::Mouse::getPosition()
{
    return position;
}

const Photon::Vector2f &Photon::Mouse::getRawPosition()
{
    return rawPosition;
}

bool Photon::Mouse::isClicked(MouseKey key)
{
    if (mouseStates.find(key) != mouseStates.end())
        return mouseStates.at(key);
    return false;
}

float Photon::Mouse::getDelta(Axis axis)
{
    if (mouseAxis.find(axis) != mouseAxis.end())
        return mouseAxis.at(axis);
    return 0;
}
