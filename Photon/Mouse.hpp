#ifndef PHOTON_MOUSE_HPP
#define PHOTON_MOUSE_HPP

#include <memory>
#include <unordered_map>
#include "Enum.hpp"
#include "Event_MouseEvent.hpp"

namespace Photon
{
    namespace Event
    {
        class Queue;
    }
    class Mouse
    {
        friend class Event::Queue;
    private:
        static int winWidth;
        static int winHeight;
        static Vector2f position;
        static Vector2f rawPosition;
        static std::unordered_map<MouseKey, bool> mouseStates;
        static std::unordered_map<Axis, float> mouseAxis;

    private:
        static void submitEvent(const std::shared_ptr<Event::MouseEvent> &);
        static void setWinDimension(int, int);

    public:
        static void setCursorVisibility(bool);
        static void moveCursor(const Vector2f &);
        static void centerCursor();
        static const Vector2f &getPosition();
        static const Vector2f &getRawPosition();
        static bool isClicked(MouseKey);
        static float getDelta(Axis);
    };
}

#endif
