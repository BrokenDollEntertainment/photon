#include "OpenGL.hpp"
#include "Object_Box.hpp"

float Photon::Object::Box::normal[6][3] = {
        {-1.0f, 0.0f, 0.0f}, //Left
        {0.0f, 1.0f, 0.0f}, //Top
        {1.0f, 0.0f, 0.0f}, //Right
        {0.0f, -1.0f, 0.0f}, //Bottom
        {0.0f, 0.0f, 1.0f}, //Front
        {0.0f, 0.0f, -1.0f} //Back
};

int Photon::Object::Box::faces[6][4] = {
        {0, 1, 2, 3}, //Left
        {3, 2, 6, 7}, //Top
        {7, 6, 5, 4}, //Right
        {4, 5, 1, 0}, //Bottom
        {5, 6, 2, 1}, //Front
        {7, 4, 0, 3} //Back
};

float Photon::Object::Box::texture[6][4][2] = {
        {{0, 1}, {1, 1}, {1, 0}, {0, 0}}, //Left
        {{0, 0}, {0, 1}, {1, 1}, {1, 0}}, //Top
        {{1, 0}, {0, 0}, {0, 1}, {1, 1}}, //Right
        {{1, 1}, {1, 0}, {0, 0}, {0, 1}}, //Bottom
        {{1, 1}, {1, 0}, {0, 0}, {0, 1}}, //Front
        {{0, 0}, {0, 1}, {1, 1}, {1, 0}} //Back
};

Photon::Object::Box::Box(float size) : width(size), height(size), depth(size) {}

Photon::Object::Box::Box(float width, float height, float depth) : width(width), height(height), depth(depth) {}

Photon::Object::Box::Box(float size, const Color &color) : width(size), height(size), depth(size)
{
    material->setColor(color);
}

Photon::Object::Box::Box(float width, float height, float depth, const Color &color) : width(width), height(height), depth(depth)
{
    material->setColor(color);
}

Photon::Object::Box::Box(float size, const Texture::Box &texture) : width(size), height(size), depth(size)
{
    setTexture(texture);
}

Photon::Object::Box::Box(float width, float height, float depth, const Texture::Box &texture) : width(width), height(height), depth(depth)
{
    setTexture(texture);
}

void Photon::Object::Box::draw()
{
    float vertex[8][3] = {
            {0.0f, 0.0f, 0.0f}, //Bottom front left corner
            {0.0f, 0.0f, depth}, //Bottom back left corner
            {0.0f, height, depth}, //Top back left corner
            {0.0f, height, 0.0f}, //Top front left corner
            {width, 0.0f, 0.0f}, //Bottom front right corner
            {width, 0.0f, depth}, //Bottom back right corner
            {width, height, depth}, //Top back right corner
            {width, height, 0.0f} //Top front right corner
    };
    for (unsigned int i = 6; i != 0; --i)
    {
        material->begin(GL_QUADS, i - 1);
        glNormal3fv(normal[i - 1]);
        material->point(texture[i - 1][0], 0);
        glVertex3fv(vertex[faces[i - 1][0]]);
        material->point(texture[i - 1][1], 1);
        glVertex3fv(vertex[faces[i - 1][1]]);
        material->point(texture[i - 1][2], 2);
        glVertex3fv(vertex[faces[i - 1][2]]);
        material->point(texture[i - 1][3], 3);
        glVertex3fv(vertex[faces[i - 1][3]]);
        material->end();
    }
}

void Photon::Object::Box::setTexture(const Texture::Box &texture)
{
    material->clearTextures();
    material->addTexture(texture[Texture::Box::LEFT].build());
    material->addTexture(texture[Texture::Box::TOP].build());
    material->addTexture(texture[Texture::Box::RIGHT].build());
    material->addTexture(texture[Texture::Box::BOTTOM].build());
    material->addTexture(texture[Texture::Box::FRONT].build());
    material->addTexture(texture[Texture::Box::BACK].build());
}

float Photon::Object::Box::getWidth() const
{
    return width;
}

void Photon::Object::Box::setWidth(float width)
{
    this->width = width;
}

float Photon::Object::Box::getHeight() const
{
    return height;
}

void Photon::Object::Box::setHeight(float height)
{
    this->height = height;
}

float Photon::Object::Box::getDepth() const
{
    return depth;
}

void Photon::Object::Box::setDepth(float depth)
{
    this->depth = depth;
}

void Photon::Object::Box::setSize(float size)
{
    width = size;
    height = size;
    depth = size;
}
