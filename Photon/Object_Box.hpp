#ifndef PHOTON_OBJECT_BOX_HPP
#define PHOTON_OBJECT_BOX_HPP

#include "Drawable3.hpp"
#include "Texture_Box.hpp"

namespace Photon::Object
{
    class Box : public Photon::Drawable3
    {
    private:
        float width;
        float height;
        float depth;
        static float normal[6][3];
        static int faces[6][4];
        static float texture[6][4][2];

    private:
        virtual void draw() override;

    public:
        Box(float);
        Box(float, float, float);
        Box(float, const Color &);
        Box(float, float, float, const Color &);
        Box(float, const Texture::Box &);
        Box(float, float, float, const Texture::Box &);
        void setTexture(const Texture::Box &);
        float getWidth() const;
        void setWidth(float);
        float getHeight() const;
        void setHeight(float);
        float getDepth() const;
        void setDepth(float);
        void setSize(float);
    };
}

#endif