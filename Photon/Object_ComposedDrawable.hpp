#ifndef PHOTON_OBJECT_COMPOSEDDRAWABLE_HPP
#define PHOTON_OBJECT_COMPOSEDDRAWABLE_HPP

#include "Drawable2.hpp"
#include "Drawable3.hpp"

namespace Photon::Object
{
    template <typename T>
    class ComposedDrawable : public T
    {
    private:
        std::vector<std::shared_ptr<Photon::Drawable>> drawables;

    private:
        void draw() override
        {
            for (const auto &drawable : drawables)
                drawable->render();
        }

    public:
        void addDrawable(const std::shared_ptr<T> &drawable)
        {
            drawables.emplace_back(drawable);
        }
    };

    class ComposedDrawable2 : public ComposedDrawable<Photon::Drawable2> {};
    class ComposedDrawable3 : public ComposedDrawable<Photon::Drawable3> {};
}

#endif
