#include "OpenGL.hpp"
#include "Object_Line.hpp"

Photon::Object::Line::Line(const Photon::Vector3f &pointA, const Photon::Vector3f &pointB) :
    pointA(pointA), pointB(pointB) {}

void Photon::Object::Line::draw()
{
    material->begin(GL_LINE);
        glVertex3f(pointA.getX(), pointA.getY(), pointA.getZ());
        glVertex3f(pointB.getX(), pointB.getY(), pointB.getZ());
    material->end();
}
