#ifndef PHOTON_OBJECT_LINE_HPP
#define PHOTON_OBJECT_LINE_HPP

#include "Drawable3.hpp"

namespace Photon::Object
{
    class Line : public Photon::Drawable3
    {
    private:
        Vector3f pointA;
        Vector3f pointB;

    private:
        void draw() override;

    public:
        Line(const Vector3f &pointA, const Vector3f &pointB);
    };
}

#endif
