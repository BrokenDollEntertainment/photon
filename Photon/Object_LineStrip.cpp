#include "OpenGL.hpp"
#include "Object_LineStrip.hpp"

void Photon::Object::LineStrip::draw()
{
    material->begin(GL_LINE_STRIP);
        for (const auto &point: points)
            glVertex3f(point.getX(), point.getY(), point.getZ());
    material->end();
}

void Photon::Object::LineStrip::addPoint(const Vector3f &point)
{
    points.emplace_back(point);
}

void Photon::Object::LineStrip::setPoints(const std::vector<Vector3f> &points)
{
    this->points = points;
}
