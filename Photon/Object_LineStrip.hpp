#ifndef PHOTON_OBJECT_LINESTRIP_HPP
#define PHOTON_OBJECT_LINESTRIP_HPP

#include "Drawable3.hpp"

namespace Photon::Object
{
    class LineStrip : public Photon::Drawable3
    {
    private:
        std::vector<Vector3f> points;

    private:
        void draw() override;

    public:
        void addPoint(const Vector3f &);
        void setPoints(const std::vector<Vector3f> &);
    };
}

#endif
