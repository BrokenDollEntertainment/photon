#include <algorithm>
#include <cstring>
#include <fstream>
#include <sstream>
#include "Debugger.hpp"
#include "Object_Mesh.hpp"
#include "Texture_Loader.hpp"

std::unordered_map<std::string, Photon::Object::Mesh> Photon::Object::Mesh::meshes;

Photon::Object::Mesh::Mesh(const Mesh &other) :
        valid(other.valid),
	    minVertex(other.minVertex),
        minNormal(other.minNormal),
        minTex(other.minTex),
        vertexesPoint(other.vertexesPoint),
        vertexesNormal(other.vertexesNormal),
        vertexesTexture(other.vertexesTexture),
		vertices(other.vertices),
		uvs(other.uvs),
		normals(other.normals)
{
    setMaterial(other.getMaterial());
}

Photon::Object::Mesh::~Mesh()
{
	clearFaces();
}

Photon::Object::Mesh::Mesh() : valid(false), minVertex(-1), minNormal(-1), minTex(-1) {}

std::shared_ptr<Photon::Object::Mesh> Photon::Object::Mesh::loadOBJFile(const std::string& filename)
{
	if (meshes.find(filename) != meshes.end())
		return std::make_shared<Mesh>(meshes.at(filename));
	Mesh mesh;
	std::ifstream fileStream(filename.c_str());
	if (!fileStream)
	{
		Debugger::log("Photon::Object::MeshLoader", "loadMesh", "Cannot open " + filename + ": " + ::strerror(errno));
		return nullptr;
	}
	std::string line;
	while (std::getline(fileStream, line))
	{
		std::stringstream stream(line);
		std::string word;
		stream >> word;
		if (word == "v")
		{
			float x, y, z;
			stream >> x >> y >> z;
			mesh.addVertex(Vector3f(x, y, z));
		}
		else if (word == "vn")
		{
			float x, y, z;
			stream >> x >> y >> z;
			mesh.addVertexNormal(Vector3f(x, y, z));
		}
		else if (word == "vt")
		{
			float x, y;
			stream >> x >> y;
			mesh.addVertexTexture(Vector2f(x, y));
		}
		else if (word == "f")
		{
			std::vector<std::string> facesInfo;
			std::string temp;
			while (stream >> temp)
				facesInfo.emplace_back(temp);
			mesh.addFace(facesInfo);
		}
	}
	mesh.validate();
	if (!mesh.valid)
	{
		Debugger::log("Photon::Object::MeshLoader", "loadMesh", filename + " is invalid");
		return nullptr;
	}
	meshes.emplace(filename, mesh);
	return std::make_shared<Mesh>(mesh);
}

std::shared_ptr<Photon::Object::Mesh> Photon::Object::Mesh::loadOBJFile(const std::string& filename, const Photon::Color& color)
{
	if (auto mesh = loadOBJFile(filename))
	{
		mesh->getMaterial()->setColor(color);
		return mesh;
	}
	return nullptr;
}

std::shared_ptr<Photon::Object::Mesh> Photon::Object::Mesh::loadOBJFile(const std::string& filename, const std::string& texture)
{
	if (auto mesh = loadOBJFile(filename))
	{
		mesh->getMaterial()->addTexture(Texture::Loader::load(texture));
		return mesh;
	}
	return nullptr;
}


void Photon::Object::Mesh::addVertex(const Vector3f &vertex)
{
    vertexesPoint.emplace_back(vertex);
    valid = false;
}

void Photon::Object::Mesh::addVertexNormal(const Vector3f &normal)
{
    vertexesNormal.emplace_back(normal.normalize());
    valid = false;
}

void Photon::Object::Mesh::addVertexTexture(const Vector2f &texture)
{
    vertexesTexture.emplace_back(texture);
    valid = false;
}

void Photon::Object::Mesh::minMax(const FaceVertexInfo &value)
{
    if (minVertex == -1 || (value.vertexPointIdx != -1 && minVertex > value.vertexPointIdx))
		minVertex = value.vertexPointIdx;
	if (minNormal == -1 || (value.vertexNormalIdx != -1 && minNormal > value.vertexNormalIdx))
        minNormal = value.vertexNormalIdx;
	if (minTex == -1 || (value.vertexTexIdx != -1 && minTex > value.vertexTexIdx))
        minTex = value.vertexTexIdx;
}

bool Photon::Object::Mesh::convert(const std::string &info, FaceVertexInfo &out)
{
    size_t nbSeparator = std::count(info.begin(), info.end(), '/');
    std::string parsableInfo = info;
    if (nbSeparator == 0)
    {
        out.vertexPointIdx = std::atoi(info.c_str());
        out.vertexTexIdx = -1;
		out.vertexNormalIdx = -1;
        return true;
    }
    else if (nbSeparator == 1)
    {
        int arr[] = { -1, -1};
        int idx = 0;
		for (size_t n = 0; n != parsableInfo.size(); n++)
		{
            char c = info[n];
            if (c == '/')
                ++idx;
            else if (c >= '0' && c <= '9')
            {
                int value = c - '0';
                if (arr[idx] == -1)
                    arr[idx] = value;
                else
                {
                    arr[idx] *= 10;
                    arr[idx] += value;
                }
            }
            else
			{
				Debugger::log("Photon::Object::Mesh", "convert", "Bad format face vertex: Face vertex should only contains digit and /");
				return false;
			}
		}
        out.vertexPointIdx = arr[0];
        out.vertexTexIdx = arr[1];
        minMax(out);
		return true;
	}
	else if (nbSeparator == 2)
	{
		int arr[] = { -1, -1, -1 };
		int idx = 0;
		for (size_t n = 0; n != parsableInfo.size(); n++)
		{
			char c = info[n];
			if (c == '/')
				++idx;
			else if (c >= '0' && c <= '9')
			{
				int value = c - '0';
				if (arr[idx] == -1)
					arr[idx] = value;
				else
				{
					arr[idx] *= 10;
					arr[idx] += value;
				}
			}
			else
			{
				Debugger::log("Photon::Object::Mesh", "convert", "Bad format face vertex: Face vertex should only contains digit and /");
				return false;
			}
		}
		out.vertexPointIdx = arr[0];
		out.vertexTexIdx = arr[1];
		out.vertexNormalIdx = arr[2];
		minMax(out);
		return true;
	}
    else
    {
        Debugger::log("Photon::Object::Mesh", "convert", "Bad format face vertex: Face vertex should be formatted as follow v1 or v1/vt1 or v1/vt1/vn1");
        return false;
    }
}

void Photon::Object::Mesh::addFace(const std::vector<std::string> &pointsInfo)
{
    if (pointsInfo.size() <= 2)
	{
		Debugger::log("Photon::Object::Mesh", "addFace", "Face vertex should be composed of at least three vertex");
		return;
	}
	std::vector<FaceVertexInfo> faceInfo;
    for (const std::string &pointInfo : pointsInfo)
    {
		FaceVertexInfo vertexInfo;
		if (!convert(pointInfo, vertexInfo))
			return;
        faceInfo.emplace_back(vertexInfo);
    }
    facesInfo.emplace_back(faceInfo);
    valid = false;
}

bool Photon::Object::Mesh::convert(const FaceVertexInfo &info, std::array<float, 3> &position, std::array<float, 2> &uv, std::array<float, 3> &normal)
{
	size_t vertexOffset = (minVertex < 0) ? 0 : minVertex;
    size_t textureOffset = (minTex < 0) ? 0 : minTex;
    size_t normalOffset = (minNormal < 0) ? 0 : minNormal;
    if (info.vertexPointIdx >= static_cast<int>(vertexesPoint.size() + vertexOffset) || info.vertexPointIdx <= 0)
    {
		Debugger::log("Photon::Object::Mesh", "convert", "Face vertex out of bound");
		return false;
	}
	else if (info.vertexTexIdx >= static_cast<int>(vertexesTexture.size() + textureOffset) || info.vertexTexIdx == 0)
	{
		Debugger::log("Photon::Object::Mesh", "convert", "Face vertex texture out of bound");
		return false;
	}
	else if (info.vertexNormalIdx >= static_cast<int>(vertexesNormal.size() + normalOffset) || info.vertexNormalIdx == 0)
	{
		Debugger::log("Photon::Object::Mesh", "convert", "Face vertex normal out of bound");
		return false;
	}
    Vector3f vertexPoint = vertexesPoint[static_cast<size_t>(info.vertexPointIdx) - vertexOffset];
    position[0] = vertexPoint.getX();
	position[1] = vertexPoint.getY();
	position[2] = vertexPoint.getZ();
    if (info.vertexNormalIdx > 0)
    {
		Vector3f vertexNormal = vertexesNormal[static_cast<size_t>(info.vertexNormalIdx) - normalOffset];
		normal[0] = vertexNormal.getX();
		normal[1] = vertexNormal.getY();
		normal[2] = vertexNormal.getZ();
    }
	if (info.vertexTexIdx > 0)
	{
		Vector2f vertexUV = vertexesTexture[static_cast<size_t>(info.vertexTexIdx) - textureOffset];
		uv[0] = vertexUV.getX();
		uv[1] = vertexUV.getY();
	}
    return true;
}

void Photon::Object::Mesh::validate()
{
    if (vertexesPoint.size() == 0)
    {
        Debugger::log("Photon::Object::Mesh", "validate", "No vertex in the mesh");
        return;
	}
	vertices.clear();
	uvs.clear();
	normals.clear();
    for (const auto &info : facesInfo)
    {
        for (const auto& pointInfo : info)
        {
            std::array<float, 3> position {0, 0, 0};
            std::array<float, 2> uv {0, 0};
            std::array<float, 3> normal {0, 0, 0};
			if (!convert(pointInfo, position, uv, normal))
				return;
			vertices.emplace_back(position);
			uvs.emplace_back(uv);
			normals.emplace_back(normal);
        }
	}
	if (vertices.size() == 0)
	{
		Debugger::log("Photon::Object::Mesh", "validate", "No faces in the mesh");
		return;
	}
    valid = true;
}

void Photon::Object::Mesh::draw()
{
	if (valid)
	{
		if (vertices.empty())
			return;
		material->begin(GL_TRIANGLES, 0);
		for (size_t n = 0; n != vertices.size(); ++n)
		{
			material->point(uvs[n][0], uvs[n][1], 0);
			glNormal3f(normals[n][0], normals[n][1], normals[n][2]);
			glVertex3f(vertices[n][0], vertices[n][1], vertices[n][2]);
		}

		// 	glBindVertexArray(face.VertexArrayID);
		// 
		// 	glBindBuffer(GL_ARRAY_BUFFER, face.vertexBuffer);
		// 	glEnableVertexAttribArray(0);
		// 	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
		// 
		// 	// 2nd attribute buffer : UVs
		// 	glBindBuffer(GL_ARRAY_BUFFER, face.uvBuffer);
		// 	glEnableVertexAttribArray(1);
		// 	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);
		// 
		// 	// 3rd attribute buffer : normals
		// 	glBindBuffer(GL_ARRAY_BUFFER, face.normalBuffer);
		// 	glEnableVertexAttribArray(2);
		// 	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
		// 
		// 	glDrawArrays(GL_TRIANGLES, 0, face.vertices.size());
		// 
		// 	glDisableVertexAttribArray(0);
		// 	glDisableVertexAttribArray(1);
		// 	glDisableVertexAttribArray(2);
		material->end();
	}
}

std::shared_ptr<Photon::Object::PointCloud> Photon::Object::Mesh::toPointCloud()
{
    std::shared_ptr<PointCloud> pointCloud = std::make_shared<PointCloud>();
    for (const auto &vertex : vertexesPoint)
        pointCloud->addPoint(vertex.getX(), vertex.getY(), vertex.getZ());
    return pointCloud;
}

void Photon::Object::Mesh::clearFaces()
{

}
