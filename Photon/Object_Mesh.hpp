#ifndef PHOTON_OBJECT_MESH_HPP
#define PHOTON_OBJECT_MESH_HPP

#include <array>
#include <vector>
#include "Drawable3.hpp"
#include "Object_PointCloud.hpp"
#include "OpenGL.hpp"
#include "Vector2.hpp"

namespace Photon::Object
{
    class MeshLoader;
    class Mesh : public Photon::Drawable3
    {
        friend class MeshLoader;
    private:
        struct FaceVertexInfo
        {
            int vertexPointIdx;
			int vertexNormalIdx;
			int vertexTexIdx;
		};

	private:
		static std::unordered_map<std::string, Mesh> meshes;

    private:
		static std::shared_ptr<Mesh> loadOBJFile(const std::string&);

    public:
		static std::shared_ptr<Mesh> loadOBJFile(const std::string&, const Photon::Color&);
		static std::shared_ptr<Mesh> loadOBJFile(const std::string&, const std::string&);

    private:
        bool valid;
		int minVertex;
		int minNormal;
		int minTex;
        GLuint vboVertices;
        GLuint vboNormals;
        GLuint vboTextures;
        std::vector<Vector3f> vertexesPoint;
        std::vector<Vector3f> vertexesNormal;
		std::vector<Vector2f> vertexesTexture;
		std::vector<std::vector<FaceVertexInfo>> facesInfo;
		std::vector<std::array<float, 3>> vertices;
		std::vector<std::array<float, 2>> uvs;
		std::vector<std::array<float, 3>> normals;

    private:
		void minMax(const FaceVertexInfo &);
		bool convert(const std::string &, FaceVertexInfo &);
        bool convert(const FaceVertexInfo &, std::array<float, 3> &, std::array<float, 2> &, std::array<float, 3> &);
        void draw() override;
        void clearFaces();

    public:
        Mesh();
        Mesh(const Mesh &);
        ~Mesh();
        void addVertex(const Vector3f &);
        void addVertexNormal(const Vector3f &);
        void addVertexTexture(const Vector2f &);
        void addFace(const std::vector<std::string> &);
        void validate();
        std::shared_ptr<PointCloud> toPointCloud();
    };
}

#endif
