#include "OpenGL.hpp"
#include "Object_Plane.hpp"

Photon::Object::Plane::Plane(float width, float height) : width(width), height(height), floor(true) {}

Photon::Object::Plane::Plane(float width, float height, const Photon::Color &color) :
        width(width), height(height), floor(true)
{
    getMaterial()->setColor(color);
}

void Photon::Object::Plane::setDimension(float width, float height)
{
    this->width = width;
    this->height = height;
}

void Photon::Object::Plane::draw()
{
    material->begin(GL_QUADS, 0);
    if (floor)
    {
        material->point(0, height, 0);
        glVertex3f(0, 0, 0);
        material->point(0, 0, 1);
        glVertex3f(0, 0, height);
        material->point(width, 0, 2);
        glVertex3f(width, 0, height);
        material->point(width, height, 3);
        glVertex3f(width, 0, 0);
        glNormal3i(0, 1, 0);
    }
    else
    {
        material->point(0, height, 0);
        glVertex3f(0, 0, height);
        material->point(0, 0, 1);
        glVertex3f(0, 0, 0);
        material->point(width, 0, 2);
        glVertex3f(width, 0, 0);
        material->point(width, height, 3);
        glVertex3f(width, 0, height);
        glNormal3i(0, -1, 0);
    }
    material->end();
}

void Photon::Object::Plane::isFloor(bool floor)
{
    this->floor = floor;
}
