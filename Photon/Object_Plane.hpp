#ifndef PHOTON_OBJECT_PLANE_HPP
#define PHOTON_OBJECT_PLANE_HPP

#include "Drawable3.hpp"
#include "Color.hpp"

namespace Photon::Object
{
    class Plane : public Photon::Drawable3
    {
    private:
        float width;
        float height;
        bool floor;

    private:
        void draw() override;

    public:
        Plane(float, float);
        Plane(float, float, const Color &);
        void isFloor(bool);
        void setDimension(float, float);
    };
}

#endif
