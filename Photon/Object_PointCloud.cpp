#include "OpenGL.hpp"
#include "Object_PointCloud.hpp"

void Photon::Object::PointCloud::draw()
{
    material->begin(GL_POINTS);
        for (const auto &point : points)
            point.render();
    material->end();
}

void Photon::Object::PointCloud::addPoint(float x, float y, float z)
{
    points.emplace_back(PointCloudVertex(x, y, z));
}

void Photon::Object::PointCloud::addPoint(float x, float y, float z, const Color &color)
{
    points.emplace_back(PointCloudVertex(x, y, z, color));
}