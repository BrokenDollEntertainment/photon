#ifndef PHOTON_OBJECT_POINTCLOUD_HPP
#define PHOTON_OBJECT_POINTCLOUD_HPP

#include <vector>
#include "Drawable3.hpp"
#include "Object_PointCloudVertex.hpp"

namespace Photon::Object
{
    class PointCloud : public Photon::Drawable3
    {
    private:
        std::vector<PointCloudVertex> points;

    private:
        void draw() override;

    public:
        void addPoint(float, float, float);
        void addPoint(float, float, float, const Color &);
    };
}

#endif
