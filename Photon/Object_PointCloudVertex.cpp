#include "OpenGL.hpp"
#include "Object_PointCloudVertex.hpp"

Photon::Object::PointCloudVertex::PointCloudVertex(float x, float y, float z) : position(x, y, z), color(255, 255, 255) {}

Photon::Object::PointCloudVertex::PointCloudVertex(float x, float y, float z, const Photon::Color &color) : position(x, y, z), color(color) {}

void Photon::Object::PointCloudVertex::render() const
{
    glColor4f(color.getR() / 255, color.getG() / 255, color.getB() / 255, color.getA() / 255);
    glVertex3f(position.getX(), position.getY(), position.getZ());
}