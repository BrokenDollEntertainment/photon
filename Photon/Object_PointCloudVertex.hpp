#ifndef PHOTON_OBJECT_POINTCLOUDVERTEX_HPP
#define PHOTON_OBJECT_POINTCLOUDVERTEX_HPP

#include "Color.hpp"
#include "Vector3.hpp"

namespace Photon::Object
{
    class PointCloud;
    class PointCloudVertex
    {
        friend class PointCloud;
    private:
        Vector3f position;
        Color color;

    private:
        PointCloudVertex(float, float, float);
        PointCloudVertex(float, float, float, const Color &);
        void render() const;

    public:
        PointCloudVertex(const PointCloudVertex &) = default;
    };
}

#endif
