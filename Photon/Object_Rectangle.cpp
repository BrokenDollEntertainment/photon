#include "OpenGL.hpp"
#include "Object_Rectangle.hpp"

Photon::Object::Rectangle::Rectangle(float width, float height) : width(width), height(height) {}

Photon::Object::Rectangle::Rectangle(float width, float height, const Photon::Color &color) :
    width(width), height(height)
{
    getMaterial()->setColor(color);
}

void Photon::Object::Rectangle::setDimension(float width, float height)
{
    this->width = width;
    this->height = height;
}

void Photon::Object::Rectangle::draw()
{
    material->begin(GL_QUADS, 0);
        material->point(0, 1, 0);
        glVertex2f(0, 0);
        material->point(0, 0, 1);
        glVertex2f(0, height);
        material->point(1, 0, 2);
        glVertex2f(width, height);
        material->point(1, 1, 3);
        glVertex2f(width, 0);
    material->end();
}

float Photon::Object::Rectangle::getWidth() const
{
    return width;
}

float Photon::Object::Rectangle::getHeight() const
{
    return height;
}
