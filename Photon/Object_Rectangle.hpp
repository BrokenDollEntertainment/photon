#ifndef PHOTON_OBJECT_RECTANGLE_HPP
#define PHOTON_OBJECT_RECTANGLE_HPP

#include "Drawable2.hpp"
#include "Color.hpp"

namespace Photon::Object
{
    class Rectangle : public Photon::Drawable2
    {
    private:
        float width;
        float height;

    private:
        void draw() override;

    public:
        Rectangle(float, float);
        Rectangle(float, float, const Color &);
        void setDimension(float, float);
        float getWidth() const;
        float getHeight() const;
    };
}

#endif
