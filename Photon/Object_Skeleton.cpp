#include "Debugger.hpp"
#include "Object_Skeleton.hpp"
#include "BinaryData_SkeletonSection.hpp"

Photon::Object::Skeleton::Bone::Bone(const std::shared_ptr<Joint> &joint) : joint(joint), drawable(nullptr) {}

void Photon::Object::Skeleton::Bone::setDrawable(const std::shared_ptr<Photon::Drawable3> &drawable)
{
    this->drawable = drawable;
}

void Photon::Object::Skeleton::Bone::draw()
{
    if (drawable != nullptr)
        drawable->render();
    rendered = joint->render(rendered);
}

std::set<std::string> Photon::Object::Skeleton::Bone::render(std::set<std::string> &rendered)
{
    this->rendered = rendered;
    Drawable3::render();
    return this->rendered;
}

Photon::Object::Skeleton::Joint::Joint(const std::string &name, const std::string &parent, const Photon::Vector3f &pos, const Vector3f &rotation) :
    name(name),
    parent(parent),
    bakedPosition(pos),
    jointPos(pos),
    jointRotation(rotation),
    drawable(nullptr) {}

void Photon::Object::Skeleton::Joint::reset()
{
    setJointRotation(jointRotation);
}

void Photon::Object::Skeleton::Joint::setDrawable(const std::shared_ptr<Drawable3> &drawable)
{
    this->drawable = drawable;
}

bool Photon::Object::Skeleton::Joint::haveChildren(const std::shared_ptr<Joint> &search) const
{
    for (const auto &child : childs)
    {
        if (child->joint == search)
            return true;
    }
    return false;
}

void Photon::Object::Skeleton::Joint::addChildren(const std::shared_ptr<Joint> &newChild)
{
    if (haveChildren(newChild))
        return;
    auto bone = std::make_shared<Bone>(newChild);
    newChild->setBone(bone);
    childs.emplace_back(bone);
}

void Photon::Object::Skeleton::Joint::draw()
{
    if (drawable != nullptr)
        drawable->render();
    for (const auto &child : childs)
        rendered = child->render(rendered);
}

std::set<std::string> Photon::Object::Skeleton::Joint::render(std::set<std::string> &rendered)
{
    if (rendered.find(name) != rendered.end())
        return rendered;
    rendered.insert(name);
    this->rendered = rendered;
    Drawable3::render();
    return this->rendered;
}

void Photon::Object::Skeleton::Joint::setBone(const std::shared_ptr<Photon::Object::Skeleton::Bone> &bone)
{
    this->bone = bone;
    bone->setRotation(jointRotation);
}

const std::shared_ptr<Photon::Object::Skeleton::Bone> Photon::Object::Skeleton::Joint::getBone() const
{
    return bone.lock();
}

void Photon::Object::Skeleton::Joint::setRootPosition(const Photon::Vector3f &rootPosition)
{
    bakedPosition = jointPos - rootPosition;
    setPosition(bakedPosition);
    for (const auto &child : childs)
        child->joint->setRootPosition(jointPos);
}

void Photon::Object::Skeleton::Joint::setJointRotation(const Photon::Vector3f &rotation)
{
    for (const auto &child : childs)
        child->setRotation(rotation);
}

Photon::Object::Skeleton::Skeleton() : valid(false), keyFrame("")
{
    setUpdatable(true);
}

void Photon::Object::Skeleton::setJoint(const std::string &joint, const std::shared_ptr<Photon::Drawable3> &drawable)
{
    if (joints.find(joint) != joints.end())
        return joints.at(joint)->setDrawable(drawable);
}

void Photon::Object::Skeleton::setBone(const std::string &joint, const std::shared_ptr<Photon::Drawable3> &drawable)
{
    if (joints.find(joint) != joints.end())
    {
        auto bone = joints.at(joint)->getBone();
        if (bone != nullptr)
            bone->setDrawable(drawable);
    }
}

bool Photon::Object::Skeleton::addJoint(const std::string &name, const std::string &parent, const Photon::Vector3f &position, const Photon::Vector3f &rotation)
{
    if (joints.find(name) != joints.end())
        return false;
    auto joint = std::make_shared<Joint>(name, parent, position, rotation);
    joints[name] = joint;
    jointsName.emplace_back(name);
    links.emplace_back(std::pair(name, parent));
    valid = false;
    return true;
}

void Photon::Object::Skeleton::draw()
{
    if (valid)
    {
        std::set<std::string> rendered;
        for (const auto &child : childs)
            rendered = child->render(rendered);
    }
}

void Photon::Object::Skeleton::validate()
{
    for (const auto &link : links)
    {
        if (link.second.empty())
            childs.emplace_back(joints[link.first]);
        else if (joints.find(link.second) != joints.end())
            joints[link.second]->addChildren(joints[link.first]);
        else
        {
            Debugger::log("Photon::Object::Skeleton", "validate", "Missing parent " + link.second);
            return;
        }
    }
    Photon::Vector3f pos = {0, 0, 0};
    for (const auto &child : childs)
    {
        child->reset();
        child->setRootPosition(pos);
    }
    links.clear();
    valid = true;
}

void Photon::Object::Skeleton::update()
{
    if (valid && !keyFrame.empty() && keyFrames.find(keyFrame) != keyFrames.end() && !keyFrames[keyFrame]->empty())
    {
        auto frame = keyFrames[keyFrame]->next();
        for (const auto &pair : frame)
        {
            if (!pair.first.empty() && joints.find(pair.first) != joints.end())
                joints[pair.first]->setJointRotation(pair.second);
        }
    }
}

void Photon::Object::Skeleton::setKeyFrame(const std::string &keyFrame)
{
    if (!this->keyFrame.empty() && keyFrames.find(this->keyFrame) != keyFrames.end())
        keyFrames[this->keyFrame]->reset();
    this->keyFrame = keyFrame;
}

void Photon::Object::Skeleton::addKeyFrame(const std::string &name, const std::shared_ptr<Photon::Object::SkeletonKeyFrame> &keyFrame)
{
    keyFrames[name] = keyFrame;
}

const std::vector<std::string> &Photon::Object::Skeleton::getJoints() const
{
    return jointsName;
}

void Photon::Object::Skeleton::read(const Photon::BinaryData::File &file)
{
    if (!file)
    {
        Debugger::log("Photon::Object::Skeleton", "read", "File is invalid");
        return;
    }
    if (!file.find("skeleton"))
    {
        Debugger::log("Photon::Object::Skeleton", "read", "Missing section skeleton");
        return;
    }
    auto section = file.getSection<BinaryData::SkeletonSection>("skeleton");
    auto &joints = section->joints;
    for (const auto &joint: joints)
        addJoint(joint.name, joint.parent, {joint.posX, joint.posY, joint.posZ}, {joint.rotX, joint.rotY, joint.rotZ});
}

void Photon::Object::Skeleton::write(Photon::BinaryData::File &file)
{
    if (!valid)
        return;
    file.addSection(std::make_shared<BinaryData::SkeletonSection>());
    auto section = file.getSection<BinaryData::SkeletonSection>("skeleton");
    for (const auto &pair : joints)
    {
        BinaryData::SkeletonSection::Joint jointToAdd;
        const auto &joint = pair.second;
        const auto &pos = joint->jointPos;
        const auto &rotation = joint->jointRotation;
        jointToAdd.name = joint->name;
        jointToAdd.parent = joint->parent;
        jointToAdd.posX = pos.getX();
        jointToAdd.posY = pos.getY();
        jointToAdd.posZ = pos.getZ();
        jointToAdd.rotX = rotation.getX();
        jointToAdd.rotY = rotation.getY();
        jointToAdd.rotZ = rotation.getZ();
        section->joints.emplace_back(jointToAdd);
    }
}
