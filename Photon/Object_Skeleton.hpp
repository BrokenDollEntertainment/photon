#ifndef PHOTON_OBJECT_SKELETON_HPP
#define PHOTON_OBJECT_SKELETON_HPP

#include <set>
#include <variant>
#include "Drawable3.hpp"
#include "Object_SkeletonKeyFrame.hpp"
#include "BinaryData_Object.hpp"

namespace Photon::Object
{
    class Skeleton : public Photon::Drawable3, public Photon::BinaryData::Object
    {
    private:
        class Joint;
        class Bone : public Photon::Drawable3
        {
            friend class Joint;
            friend class Skeleton;
        private:
            std::set<std::string> rendered;
            std::shared_ptr<Joint> joint;
            std::shared_ptr<Photon::Drawable3> drawable;

        private:
            void draw() override;
            std::set<std::string> render(std::set<std::string> &);
            void setDrawable(const std::shared_ptr<Drawable3> &);

        public:
            Bone(const std::shared_ptr<Joint> &);
        };

        class Joint : public Photon::Drawable3
        {
            friend class Bone;
            friend class Skeleton;
        private:
            std::set<std::string> rendered;
            std::string name;
            std::string parent;
            Photon::Vector3f bakedPosition;
            Photon::Vector3f jointPos;
            Photon::Vector3f jointRotation;
            std::weak_ptr<Bone> bone;
            std::shared_ptr<Photon::Drawable3> drawable;
            std::vector<std::shared_ptr<Bone>> childs;

        private:
            void draw() override;
            std::set<std::string> render(std::set<std::string> &);
            bool haveChildren(const std::shared_ptr<Joint> &) const;
            void addChildren(const std::shared_ptr<Joint> &);
            void setBone(const std::shared_ptr<Bone> &);
            const std::shared_ptr<Bone> getBone() const;
            void setDrawable(const std::shared_ptr<Drawable3> &drawable);
            void reset();
            void setRootPosition(const Vector3f &);
            void setJointRotation(const Vector3f &);

        public:
            Joint(const std::string &, const std::string &, const Vector3f &, const Vector3f &);
        };

    private:
        bool valid;
        std::vector<std::string> jointsName;
        std::unordered_map<std::string, std::shared_ptr<Joint>> joints;
        std::unordered_map<std::string, std::shared_ptr<SkeletonKeyFrame>> keyFrames;
        std::vector<std::shared_ptr<Joint>> childs;
        std::vector<std::pair<std::string, std::string>> links;
        std::string keyFrame;

    private:
        void update() override;
        void draw() override;

    public:
        void read(const BinaryData::File &file) override;
        void write(BinaryData::File &file) override;

    public:
        Skeleton();
        void validate();
        void setKeyFrame(const std::string &);
        void addKeyFrame(const std::string &, const std::shared_ptr<SkeletonKeyFrame> &);
        bool addJoint(const std::string &, const std::string &, const Photon::Vector3f &, const Photon::Vector3f & = {0, 0, 0});
        void setJoint(const std::string &, const std::shared_ptr<Photon::Drawable3> &);
        void setBone(const std::string &, const std::shared_ptr<Photon::Drawable3> &);
        const std::vector<std::string> &getJoints() const;
    };
}

#endif
