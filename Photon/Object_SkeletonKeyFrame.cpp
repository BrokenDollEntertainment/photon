#include "Object_SkeletonKeyFrame.hpp"

Photon::Object::SkeletonKeyFrame::SkeletonKeyFrame(Type loopType) :
    loopType(loopType),
    increasing(true),
    frameDuration(0),
    sinceLast(0),
    idx(0) {}

Photon::Object::SkeletonKeyFrame::SkeletonKeyFrame(unsigned int frameDuration, Type loopType) :
    loopType(loopType),
    increasing(true),
    frameDuration(frameDuration - 1),
    sinceLast(0),
    idx(0) {}


void Photon::Object::SkeletonKeyFrame::Frame::addRotation(const std::string &joint, const Photon::Vector3f &rotation)
{
    jointsRotation[joint] = rotation;
}

bool Photon::Object::SkeletonKeyFrame::empty() const
{
    return frames.empty();
}

void Photon::Object::SkeletonKeyFrame::reset()
{
    idx = 0;
}

const std::unordered_map<std::string, Photon::Vector3f> &Photon::Object::SkeletonKeyFrame::next()
{
    auto ret = idx;
    if (sinceLast == 0)
    {
        if (increasing)
        {
            if (idx < (frames.size() - 1))
                ++idx;
            else if (loopType == Type::LOOP)
                idx = 0;
            else if (loopType == Type::LOOP_REVERSED || loopType == Type::ONCE_REVERSED)
                increasing = false;
        }
        else
        {
            if (idx > 0)
                --idx;
            else if (loopType == Type::LOOP_REVERSED)
                increasing = true;
        }
        sinceLast = frameDuration;
    }
    else
        --sinceLast;
    return frames[ret]->jointsRotation;
}

void Photon::Object::SkeletonKeyFrame::addFrame(const std::shared_ptr<Frame> &frame)
{
    frames.emplace_back(frame);
}

void Photon::Object::SkeletonKeyFrame::addMovement(const std::string &joint, const Photon::Vector3f &from,
        const Photon::Vector3f &to, unsigned int on, unsigned int since)
{
    Photon::Vector3f step = (to - from) / static_cast<float>(on);
    unsigned long maxFrame = on + since;
    if (maxFrame > frames.size())
    {
        for (unsigned long n = frames.size(); n != maxFrame; ++n)
            frames.emplace_back(std::make_shared<Frame>());
    }
    Photon::Vector3f rotation = from;
    for (unsigned int n = since; n != maxFrame; ++n)
    {
        frames[n]->addRotation(joint, rotation);
        rotation += step;
    }
}

const std::shared_ptr<Photon::Object::SkeletonKeyFrame::Frame> &Photon::Object::SkeletonKeyFrame::get(unsigned long idx) const
{
    return frames[idx];
}
