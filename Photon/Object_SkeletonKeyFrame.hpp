#ifndef PHOTON_OBJECT_SKELETONKEYFRAME_HPP
#define PHOTON_OBJECT_SKELETONKEYFRAME_HPP

#include <unordered_map>
#include <vector>
#include "Vector3.hpp"

namespace Photon::Object
{
    class Skeleton;
    class SkeletonKeyFrame
    {
        friend class Skeleton;
    public:
        class Frame
        {
            friend class SkeletonKeyFrame;
        private:
            std::unordered_map<std::string, Photon::Vector3f> jointsRotation;

        public:
            void addRotation(const std::string &, const Photon::Vector3f &);
        };

        enum class Type
        {
            LOOP,
            LOOP_REVERSED,
            ONCE,
            ONCE_REVERSED
        };

    private:
        Type loopType;
        bool increasing;
        unsigned int frameDuration;
        unsigned int sinceLast;
        unsigned long idx;
        std::vector<std::shared_ptr<Frame>> frames;

    private:
        bool empty() const;
        void reset();
        const std::unordered_map<std::string, Photon::Vector3f> &next();

    public:
        SkeletonKeyFrame(Type = Type::LOOP);
        SkeletonKeyFrame(unsigned int, Type = Type::LOOP);
        void addFrame(const std::shared_ptr<Frame> &);
        void addMovement(const std::string &, const Photon::Vector3f &, const Photon::Vector3f &, unsigned int, unsigned int = 0);
        const std::shared_ptr<Frame> &get(unsigned long) const;
    };
}

#endif
