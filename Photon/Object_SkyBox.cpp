#include "OpenGL.hpp"
#include "Object_SkyBox.hpp"

float Photon::Object::SkyBox::normal[6][3] = {
        {1.0f, 0.0f, 0.0f}, //Left
        {0.0f, -1.0f, 0.0f}, //Top
        {-1.0f, 0.0f, 0.0f}, //Right
        {0.0f, 1.0f, 0.0f}, //Bottom
        {0.0f, 0.0f, -1.0f}, //Back
        {0.0f, 0.0f, 1.0f} //Front
};

int Photon::Object::SkyBox::faces[6][4] = {
        {3, 2, 1, 0}, //Left
        {7, 6, 2, 3}, //Top
        {4, 5, 6, 7}, //Right
        {0, 1, 5, 4}, //Bottom
        {1, 2, 6, 5}, //Back
        {3, 0, 4, 7} //Front
};

float Photon::Object::SkyBox::texture[6][4][2] = {
        {{0, 0}, {1, 0}, {1, 1}, {0, 1}}, //Left
        {{1, 1}, {0, 1}, {0, 0}, {1, 0}}, //Top
        {{1, 1}, {0, 1}, {0, 0}, {1, 0}}, //Right
        {{1, 1}, {0, 1}, {0, 0}, {1, 0}}, //Bottom
        {{0, 1}, {0, 0}, {1, 0}, {1, 1}}, //Back
        {{1, 0}, {1, 1}, {0, 1}, {0, 0}} //Front
};

Photon::Object::SkyBox::SkyBox(Texture::Box &texture) : size(10)
{
    setTexture(texture);
}

Photon::Object::SkyBox::SkyBox(const Color &color) : size(10)
{
    material->setColor(color);
}

Photon::Object::SkyBox::SkyBox() : size(10) {}

void Photon::Object::SkyBox::draw()
{
    float halfSize = size / 2;
    float vertex[8][3] = {
            {-halfSize, -halfSize, -halfSize}, //Bottom front left corner
            {-halfSize, -halfSize, halfSize}, //Bottom back left corner
            {-halfSize, halfSize, halfSize}, //Top back left corner
            {-halfSize, halfSize, -halfSize}, //Top front left corner
            {halfSize, -halfSize, -halfSize}, //Bottom front right corner
            {halfSize, -halfSize, halfSize}, //Bottom back right corner
            {halfSize, halfSize, halfSize}, //Top back right corner
            {halfSize, halfSize, -halfSize} //Top front right corner
    };
    for (unsigned int i = 6; i != 0; --i)
    {
        material->begin(GL_QUADS, i - 1);
        glNormal3fv(normal[i - 1]);
        material->point(texture[i - 1][0], 0);
        glVertex3fv(vertex[faces[i - 1][0]]);
        material->point(texture[i - 1][1], 1);
        glVertex3fv(vertex[faces[i - 1][1]]);
        material->point(texture[i - 1][2], 2);
        glVertex3fv(vertex[faces[i - 1][2]]);
        material->point(texture[i - 1][3], 3);
        glVertex3fv(vertex[faces[i - 1][3]]);
        material->end();
    }
}

void Photon::Object::SkyBox::setTexture(Texture::Box &texture)
{
    texture[Texture::Box::LEFT].setTextureMode(Texture::Texture::CLAMP, Texture::Texture::CLAMP);
    texture[Texture::Box::TOP].setTextureMode(Texture::Texture::CLAMP, Texture::Texture::CLAMP);
    texture[Texture::Box::RIGHT].setTextureMode(Texture::Texture::CLAMP, Texture::Texture::CLAMP);
    texture[Texture::Box::BOTTOM].setTextureMode(Texture::Texture::CLAMP, Texture::Texture::CLAMP);
    texture[Texture::Box::BACK].setTextureMode(Texture::Texture::CLAMP, Texture::Texture::CLAMP);
    texture[Texture::Box::FRONT].setTextureMode(Texture::Texture::CLAMP, Texture::Texture::CLAMP);
    material->clearTextures();
    material->addTexture(texture[Texture::Box::LEFT].build());
    material->addTexture(texture[Texture::Box::TOP].build());
    material->addTexture(texture[Texture::Box::RIGHT].build());
    material->addTexture(texture[Texture::Box::BOTTOM].build());
    material->addTexture(texture[Texture::Box::BACK].build());
    material->addTexture(texture[Texture::Box::FRONT].build());
}

float Photon::Object::SkyBox::getSize() const
{
    return size;
}

void Photon::Object::SkyBox::setSize(float size)
{
    this->size = size;
}
