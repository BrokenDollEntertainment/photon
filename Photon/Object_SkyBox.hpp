#ifndef PHOTON_OBJECT_SKYBOX_HPP
#define PHOTON_OBJECT_SKYBOX_HPP

#include "Drawable3.hpp"
#include "Texture_Box.hpp"

namespace Photon::Object
{
    class SkyBox : public Photon::Drawable3
    {
    private:
        float size;
        static float normal[6][3];
        static int faces[6][4];
        static float texture[6][4][2];

    private:
        virtual void draw() override;

    public:
        SkyBox();
        SkyBox(const Color &);
        SkyBox(Texture::Box &);
        void setTexture(Texture::Box &);
        float getSize() const;
        void setSize(float size);
    };
}

#endif
