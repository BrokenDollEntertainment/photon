#include "Object_Sphere.hpp"

void Photon::Object::Sphere::draw()
{
    material->bindTexture(0);
    gluSphere(quad.get(), radius, nbLongitude, nbLatitude);
    material->unbindTexture();
}

Photon::Object::Sphere::Sphere(float radius, int nbLatitude, int nbLongitude) :
    radius(radius), nbLatitude(nbLatitude), nbLongitude(nbLongitude),
    quad(std::shared_ptr<GLUquadricObj>(gluNewQuadric(), ::gluDeleteQuadric))
{
    gluQuadricDrawStyle(quad.get(), GLU_FILL);
    gluQuadricTexture(quad.get(), GLU_TRUE);
    gluQuadricNormals(quad.get(), GLU_SMOOTH);
}
