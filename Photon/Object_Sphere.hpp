#ifndef PHOTON_OBJECT_SPHERE_HPP
#define PHOTON_OBJECT_SPHERE_HPP

#include "GLUtil.hpp"
#include "Drawable3.hpp"

namespace Photon::Object
{
    class Sphere : public Photon::Drawable3
    {
    private:
        float radius;
        int nbLatitude;
        int nbLongitude;
        std::shared_ptr<GLUquadricObj> quad;

    private:
        void draw() override;

    public:
        Sphere(float, int, int);
    };
}

#endif
