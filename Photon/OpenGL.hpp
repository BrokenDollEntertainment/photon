#ifndef PHOTON_OPENGL_HPP
#define PHOTON_OPENGL_HPP

#ifdef _WIN32
#pragma comment(lib, "opengl32.lib")
    #include <Windows.h>
    #include <GL/glew.h>
    #include <GL/gl.h>
    #define GL_CLAMP_TO_EDGE 0x812F
#elif __APPLE__
    #include <OpenGL/glew.h>
    #include <OpenGL/gl.h>
#elif __linux__
    #include <GL/glew.h>
    #include <GL/gl.h>
#endif

#endif
