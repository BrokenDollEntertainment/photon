#include "Debugger.hpp"
#include "GLUtil.hpp"
#include "PixelArray.hpp"

Photon::PixelArray::PixelArray(unsigned int width, unsigned int height) : width(width), height(height)
{
    for (unsigned int y = 0; y != height; ++y)
    {
        std::vector<Color> row;
        for (unsigned int x = 0; x != width; ++x)
            row.emplace_back(Color(255, 255, 255, 255));
        pixels.emplace_back(row);
    }
}

void Photon::PixelArray::set(unsigned int x, unsigned int y, const Color &color)
{
    pixels[y][x].setRGB(color.getR(), color.getG(), color.getB(), color.getA());
}

void Photon::PixelArray::set(unsigned int x, unsigned int y, float r, float g, float b, float a)
{
    pixels[y][x].setRGB(r, g, b, a);
}

const Photon::Color &Photon::PixelArray::get(unsigned int x, unsigned int y) const
{
    return pixels[y][x];
}

unsigned char *Photon::PixelArray::toRGB() const
{
    unsigned int rgbSize = width * height * 3;
    auto rgb = new unsigned char[rgbSize];
    unsigned int n = 0;
    for (unsigned int y = 0; y != height; ++y)
    {
        for (unsigned int x = 0; x != width; ++x)
        {
            const auto &color = pixels[y][x];
            rgb[n++] = static_cast<unsigned char>(color.getR());
            rgb[n++] = static_cast<unsigned char>(color.getG());
            rgb[n++] = static_cast<unsigned char>(color.getB());
        }
    }
    return rgb;
}

unsigned char *Photon::PixelArray::toRGBA() const
{
    auto rgb = new unsigned char[width * height * 4];
    unsigned int n = 0;
    for (unsigned int y = 0; y != height; ++y)
    {
        for (unsigned int x = 0; x != width; ++x)
        {
            const auto &color = pixels[y][x];
            rgb[n++] = static_cast<unsigned char>(color.getR());
            rgb[n++] = static_cast<unsigned char>(color.getG());
            rgb[n++] = static_cast<unsigned char>(color.getB());
            rgb[n++] = static_cast<unsigned char>(color.getA());
        }
    }
    return rgb;
}

const Photon::Texture::Texture Photon::PixelArray::toTexture() const
{
    GLuint texId;
    unsigned char *image = toRGBA();
    if (image == nullptr)
    {
        Debugger::log("Photon::PixelArray", "toTexture", "Cannot create bytes array");
        return Texture::Texture();
    }
    glGenTextures(1, &texId);
    glBindTexture(GL_TEXTURE_2D, texId);
    gluBuild2DMipmaps(GL_TEXTURE_2D, 4, width, height, GL_RGBA, GL_UNSIGNED_BYTE, image);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glBindTexture(GL_TEXTURE_2D, 0);
    delete[](image);
    return Texture::Texture(width, height, texId);
}

void Photon::PixelArray::save(const std::string &path, Photon::ImageType type) const
{
    unsigned char *pixels;
    switch (type)
    {
        case ImageType::BMP:
        case ImageType::TGA:
        case ImageType::JPG:
            pixels = toRGB();
            break;
        default:
            pixels = toRGBA();
            break;
    }
    Texture::Stb::Image::save(path, static_cast<int>(type), width, height, pixels);
    delete[](pixels);
}
