#ifndef PHOTON_PIXELARRAY_HPP
#define PHOTON_PIXELARRAY_HPP

#include "Enum.hpp"
#include "Color.hpp"
#include "Texture_Texture.hpp"

namespace Photon
{
    class PixelArray
    {
    private:
        unsigned int width;
        unsigned int height;
        std::vector<std::vector<Color>> pixels;

    private:
        unsigned char *toRGB() const;
        unsigned char *toRGBA() const;

    public:
        PixelArray(unsigned int, unsigned int);
        void set(unsigned int, unsigned int, float, float, float, float = 255);
        void set(unsigned int, unsigned int, const Color &);
        const Color &get(unsigned int, unsigned int) const;
        const Texture::Texture toTexture() const;
        void save(const std::string &, ImageType) const;
    };
}

#endif
