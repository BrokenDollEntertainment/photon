#include "Debugger.hpp"
#include "Glut.hpp"
#include "LightManager.hpp"
#include "Renderer.hpp"
#include "Mouse.hpp"
#include "UI_Font.hpp"

bool Photon::Renderer::inited = false;
bool Photon::Renderer::run;
Photon::Renderer::Configuration Photon::Renderer::configuration;
int Photon::Renderer::window;
int Photon::Renderer::windowWidth;
int Photon::Renderer::windowHeight;
std::string Photon::Renderer::currentScene = DEFAULT_SCENE;
std::string Photon::Renderer::oldCurrentScene = "";
std::unordered_map<std::string, std::shared_ptr<Photon::Scene>> Photon::Renderer::scenes;
const std::unordered_map<unsigned int, Photon::Key> Photon::Renderer::converter
{
    {'0', Key::NUMKEY0},
    {'1', Key::NUMKEY1},
    {'2', Key::NUMKEY2},
    {'3', Key::NUMKEY3},
    {'4', Key::NUMKEY4},
    {'5', Key::NUMKEY5},
    {'6', Key::NUMKEY6},
    {'7', Key::NUMKEY7},
    {'8', Key::NUMKEY8},
    {'9', Key::NUMKEY9},
    {'a', Key::A},
    {'b', Key::B},
    {'c', Key::C},
    {'d', Key::D},
    {'e', Key::E},
    {'f', Key::F},
    {'g', Key::G},
    {'h', Key::H},
    {'i', Key::I},
    {'j', Key::J},
    {'k', Key::K},
    {'l', Key::L},
    {'m', Key::M},
    {'n', Key::N},
    {'o', Key::O},
    {'p', Key::P},
    {'q', Key::Q},
    {'r', Key::R},
    {'s', Key::S},
    {'t', Key::T},
    {'u', Key::U},
    {'v', Key::V},
    {'w', Key::W},
    {'x', Key::X},
    {'y', Key::Y},
    {'z', Key::Z},
    {'=', Key::EQUAL},
    {'\b', Key::BACKSPACE},
    {'\t', Key::TAB},
    {27, Key::ESC},
    {13, Key::BREAK},
    {',', Key::COMMA},
    {';', Key::SEMICOLON},
    {'*', Key::MUL},
    {' ', Key::SPACE},
    {'-', Key::SUB},
    {'.', Key::PERIOD},
    {'/', Key::DIV},
    {'+', Key::ADD},
    {127, Key::DEL},
    {17, Key::EXIT}
};

const std::unordered_map<unsigned int, Photon::Key> Photon::Renderer::specConverter
{
    {GLUT_KEY_F1, Key::F1},
    {GLUT_KEY_F2, Key::F2},
    {GLUT_KEY_F3, Key::F3},
    {GLUT_KEY_F4, Key::F4},
    {GLUT_KEY_F5, Key::F5},
    {GLUT_KEY_F6, Key::F6},
    {GLUT_KEY_F7, Key::F7},
    {GLUT_KEY_F8, Key::F8},
    {GLUT_KEY_F9, Key::F9},
    {GLUT_KEY_F10, Key::F10},
    {GLUT_KEY_F11, Key::F11},
    {GLUT_KEY_F12, Key::F12},
    {GLUT_KEY_PAGE_UP, Key::PGUP},
    {GLUT_KEY_PAGE_DOWN, Key::PGDOWN},
    {GLUT_KEY_UP, Key::UP},
    {GLUT_KEY_LEFT, Key::LEFT},
    {GLUT_KEY_RIGHT, Key::RIGHT},
    {GLUT_KEY_DOWN, Key::DOWN},
    {GLUT_KEY_END, Key::END},
    {GLUT_KEY_INSERT, Key::INSERT},
    {GLUT_KEY_HOME, Key::MENU},
    {112, Key::LSHIFT},
    {114, Key::LCTRL},
    {116, Key::LALT},
    {113, Key::RSHIFT},
    {117, Key::RCTRL},
    {115, Key::RALT}
};

void Photon::Renderer::init(const Configuration &configuration)
{
    Renderer::configuration = configuration;
    int argc = 1;
    char *argv[1];
    argv[0] = ::strdup(configuration.title.c_str());
    srand(static_cast<unsigned int>(time(nullptr)));
    glutInit(&argc, argv);
    free(argv[0]);
    if (configuration.world == Scene::PLAN)
        glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
    else if (configuration.world == Scene::SPACE)
        glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
    addScene(DEFAULT_SCENE, configuration.world);
    scenes[Renderer::currentScene]->load();
    glutInitWindowSize(configuration.width, configuration.height);
    glutInitWindowPosition(configuration.x, configuration.y);
    window = glutCreateWindow(configuration.title.c_str());
    if (configuration.fullscreen)
        glutFullScreen();
    glEnable(GL_SCISSOR_TEST);
    glewExperimental = GL_TRUE;
    GLenum glew_status = glewInit();
    if (GLEW_OK != glew_status)
    {
        const unsigned char *glewError = glewGetErrorString(glew_status);
        Debugger::log("Photon::Renderer", "init", "Cannot initialize glew: " + std::string(reinterpret_cast<const char *>(glewError)));
        return;
    }
    UI::Font::init();
    LightManager::setSmoothLight(false);
    ::glutDisplayFunc(&Photon::Renderer::draw);
    ::glutReshapeFunc(&Photon::Renderer::reshape);
    ::glutMouseFunc(&Photon::Renderer::mouse);
    ::glutKeyboardFunc(&Photon::Renderer::keyboardPressed);
    ::glutSpecialFunc(&Photon::Renderer::specialPressed);
    ::glutIdleFunc(&Photon::Renderer::idle);
    ::glutMotionFunc(&Photon::Renderer::motion);
    ::glutPassiveMotionFunc(&Photon::Renderer::motion);
    inited = true;
}

void Photon::Renderer::start()
{
    if (inited)
    {
        run = true;
        glutMainLoop();
    }
}

void Photon::Renderer::stop()
{
    run = false;
    UI::Font::clean();
}

void Photon::Renderer::resize(int width, int height)
{
    configuration.width = width;
    configuration.height = height;
    glutReshapeWindow(configuration.width, configuration.height);
}

void Photon::Renderer::move(int x, int y)
{
    configuration.x = x;
    configuration.y = y;
    glutPositionWindow(configuration.x, configuration.y);
}

void Photon::Renderer::fullscreen(bool full)
{
    configuration.fullscreen = full;
    if (configuration.fullscreen)
        glutFullScreen();
    else
    {
        glutPositionWindow(configuration.x, configuration.y);
        glutReshapeWindow(configuration.width, configuration.height);
    }
}

void Photon::Renderer::draw()
{
    if (scenes.find(currentScene) != scenes.end())
        scenes[currentScene]->draw();
    if (run)
    {
        if (!oldCurrentScene.empty() && scenes.find(oldCurrentScene) != scenes.end())
        {
            scenes[oldCurrentScene]->unload();
            oldCurrentScene = "";
        }
        glutPostRedisplay();
    }
    else
        glutDestroyWindow(window);
}

void Photon::Renderer::reshape(int w, int h)
{
    windowWidth = w;
    windowHeight = h;
    Event::Queue::setWinDimension(w, h);
}

void Photon::Renderer::idle()
{
    if (scenes.find(currentScene) != scenes.end())
        scenes[currentScene]->idle();
    Event::Queue::update();
}

void Photon::Renderer::keyboard(Photon::Key key, Photon::ButtonState state)
{
    Event::Queue::submit(std::make_shared<Event::KeyboardEvent>(key, state));
}

void Photon::Renderer::keyboardPressed(unsigned char key, int, int)
{
    if (converter.find(key) != converter.end())
        keyboard(converter.at(key), ButtonState::PRESSED);
}

void Photon::Renderer::specialPressed(int key, int, int)
{
    if (specConverter.find(key) != specConverter.end())
        keyboard(specConverter.at(key), ButtonState::PRESSED);
}

void Photon::Renderer::motion(int x, int y)
{
    Event::Queue::submit(std::make_shared<Event::MouseEvent>(static_cast<float>(x), static_cast<float>(y), MouseKey::NOCLICK, ButtonState::NONE, static_cast<short>(Mouse::getDelta(Axis::WHEELAXIS))));
}

void Photon::Renderer::mouse(int button, int state, int, int)
{
    MouseKey click = MouseKey::NOCLICK;
    ButtonState clickState = ButtonState::NONE;
    short delta = 0;
    switch (button)
    {
        case GLUT_LEFT_BUTTON:
            click = MouseKey::LEFTCLICK;
            break;
        case GLUT_MIDDLE_BUTTON:
            click = MouseKey::MIDDLECLICK;
            break;
        case GLUT_RIGHT_BUTTON:
            click = MouseKey::RIGHTCLICK;
            break;
        case 3:
            delta = 1;
            break;
        case 4:
            delta = -1;
            break;
        default:
            break;
    }
    if (button == GLUT_LEFT_BUTTON || button == GLUT_MIDDLE_BUTTON || button == GLUT_RIGHT_BUTTON)
    {
        if (state == GLUT_DOWN)
            clickState = ButtonState::PRESSED;
        else if (state == GLUT_UP)
            clickState = ButtonState::RELEASED;
    }
    auto mousePos = Mouse::getRawPosition();
    auto event = std::make_shared<Event::MouseEvent>(mousePos.getX(), mousePos.getY(), click, clickState, delta);
    Event::Queue::submit(event);
}

bool Photon::Renderer::addScene(const std::string &name, Scene::WorldType type)
{
    if (scenes.find(name) != scenes.end())
    {
        Debugger::log("Photon::Renderer", "addScene", "Scene " + name + " already exist");
        return false;
    }
    auto sceneToAdd = std::make_shared<Scene>(name, type);
    sceneToAdd->setOwn(sceneToAdd);
    scenes[name] = sceneToAdd;
    return true;
}

const std::shared_ptr<Photon::Scene> Photon::Renderer::getScene(const std::string &name)
{
    if (scenes.find(name) == scenes.end())
        return nullptr;
    return scenes[name];
}

void Photon::Renderer::setUpdaterTo(const std::string &name, const std::shared_ptr<Photon::Updater> &updater)
{
    if (scenes.find(name) == scenes.end())
    {
        Debugger::log("Photon::Renderer", "setUpdaterTo", "Scene " + name + " does not exist");
        return;
    }
    auto scene = scenes[name];
    updater->setScene(scene);
    scene->setUpdater(updater);
}

void Photon::Renderer::screenshot(const std::string &path, ImageType type)
{
    int components = 3;
    if (type == ImageType::PNG)
        components = 4;
    size_t pixelArraySize = components * windowWidth * windowHeight;
    unsigned char *pixels = new unsigned char[pixelArraySize];
    if (type == ImageType::PNG)
        glReadPixels(0, 0, windowWidth, windowHeight, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
    else
        glReadPixels(0, 0, windowWidth, windowHeight, GL_RGB, GL_UNSIGNED_BYTE, pixels);
    Texture::Stb::Image::save(path, static_cast<int>(type), windowWidth, windowHeight, pixels);
    delete[](pixels);
}

int Photon::Renderer::getWindowWidth()
{
    return windowWidth;
}

int Photon::Renderer::getWindowHeight()
{
    return windowHeight;
}

void Photon::Renderer::setCurrentScene(const std::string &currentScene)
{
    oldCurrentScene = Renderer::currentScene;
    Renderer::currentScene = currentScene;
    if (scenes.find(Renderer::currentScene) != scenes.end())
        scenes[Renderer::currentScene]->load();
}
