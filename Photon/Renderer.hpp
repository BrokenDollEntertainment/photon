#ifndef PHOTON_RENDERER_HPP
#define PHOTON_RENDERER_HPP

#ifdef _WIN32
    #include <ctime>
#endif
#include "Event_Queue.hpp"
#include "Scene.hpp"

namespace Photon
{
    class Renderer
    {
    public:
        struct Configuration
        {
            std::string title;
            int width;
            int height;
            bool fullscreen;
            Scene::WorldType world;
            int x;
            int y;

            Configuration() = default;
            Configuration(const std::string &title, int width, int height, bool fullscreen, Scene::WorldType world) :
                    title(title), width(width), height(height), fullscreen(fullscreen), world(world), x(100), y(100) {}
            Configuration(const std::string &title, int width, int height, bool fullscreen, Scene::WorldType world, int x, int y) :
                    title(title), width(width), height(height), fullscreen(fullscreen), world(world), x(x), y(y) {}
        };

    private:
        static bool inited;
        static bool run;
        static Configuration configuration;
        static int window;
        static int windowWidth;
        static int windowHeight;
        static std::string currentScene;
        static std::string oldCurrentScene;
        static std::unordered_map<std::string, std::shared_ptr<Scene>> scenes;
        static const std::unordered_map<unsigned int, Key> converter;
        static const std::unordered_map<unsigned int, Key> specConverter;

    private:
        static void draw();
        static void idle();
        static void reshape(int, int);
        static void mouse(int, int, int, int);
        static void keyboard(Key, ButtonState);
        static void keyboardPressed(unsigned char, int, int);
        static void specialPressed(int, int, int);
        static void motion(int, int);

    public:
        static void setCurrentScene(const std::string &currentScene);
        static void setUpdaterTo(const std::string &, const std::shared_ptr<Updater> &);
        static bool addScene(const std::string &, Scene::WorldType);
        static const std::shared_ptr<Scene> getScene(const std::string &);
        static void resize(int, int);
        static void move(int, int);
        static void fullscreen(bool);
        static void init(const Configuration &);
        static void start();
        static void stop();
        static void screenshot(const std::string &, ImageType);
        static int getWindowWidth();
        static int getWindowHeight();
    };
}

#endif