#include "OpenGL.hpp"
#include "Rotation2.hpp"

void Photon::Rotation2::rotate(const Vector2f &position) const
{
    for (const auto &pair : rotations)
    {
        glTranslatef(-(position.getX() - pair.first.getX()), -(position.getY() - pair.first.getY()), 0);
        glRotatef(pair.second.getX(), 1.0, 0.0, 0.0);
        glRotatef(pair.second.getY(), 0.0, 1.0, 0.0);
        glTranslatef((position.getX() - pair.first.getX()), (position.getY() - pair.first.getY()), 0);
    }
}

void Photon::Rotation2::set(const Photon::Vector2f &key, const Photon::Vector2f &value)
{
    for (auto &pair : rotations)
    {
        if (pair.first == key)
        {
            pair.second = value;
            return;
        }
    }
    rotations.emplace_back(std::pair<Vector2f, Vector2f>(key, value));
}

void Photon::Rotation2::remove(const Photon::Vector2f &key)
{
    unsigned int toRemove = 0;
    for (const auto &pair : rotations)
    {
        if (pair.first == key)
        {
            rotations.erase(rotations.begin() + toRemove);
            return;
        }
        ++toRemove;
    }
}