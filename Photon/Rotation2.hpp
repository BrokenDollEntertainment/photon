#ifndef PHOTON_ROTATION2_HPP
#define PHOTON_ROTATION2_HPP

#include <vector>
#include "Vector2.hpp"

namespace Photon
{
    class Drawable2;
    class Rotation2
    {
        friend class Drawable2;
    private:
        std::vector<std::pair<Vector2f, Vector2f>> rotations;

    private:
        void rotate(const Vector2f &) const;

    public:
        void set(const Vector2f &, const Vector2f &);
        void remove(const Vector2f &);
    };
}

#endif
