#include "OpenGL.hpp"
#include "Rotation3.hpp"

void Photon::Rotation3::rotate(const Vector3f &position) const
{
    for (const auto &pair : rotations)
    {
        glTranslatef(-(position.getX() - pair.first.getX()),
                -(position.getY() - pair.first.getY()),
                -(position.getZ() - pair.first.getZ()));
        glRotatef(pair.second.getX(), 1.0, 0.0, 0.0);
        glRotatef(pair.second.getY(), 0.0, 1.0, 0.0);
        glRotatef(pair.second.getZ(), 0.0, 0.0, 1.0);
        glTranslatef((position.getX() - pair.first.getX()),
                (position.getY() - pair.first.getY()),
                (position.getZ() - pair.first.getZ()));
    }
}

void Photon::Rotation3::set(const Photon::Vector3f &key, const Photon::Vector3f &value)
{
    for (auto &pair : rotations)
    {
        if (pair.first == key)
        {
            pair.second = value;
            return;
        }
    }
    rotations.emplace_back(std::pair<Vector3f, Vector3f>(key, value));
}

void Photon::Rotation3::remove(const Photon::Vector3f &key)
{
    unsigned int toRemove = 0;
    for (const auto &pair : rotations)
    {
        if (pair.first == key)
        {
            rotations.erase(rotations.begin() + toRemove);
            return;
        }
        ++toRemove;
    }
}
