#ifndef PHOTON_ROTATION3_HPP
#define PHOTON_ROTATION3_HPP

#include <vector>
#include "Vector3.hpp"

namespace Photon
{
    class Drawable3;
    class Rotation3
    {
        friend class Drawable3;
    private:
        std::vector<std::pair<Vector3f, Vector3f>> rotations;

    private:
        void rotate(const Vector3f &) const;

    public:
        void set(const Vector3f &, const Vector3f &);
        void remove(const Vector3f &);
    };
}

#endif
