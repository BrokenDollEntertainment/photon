#include "Debugger.hpp"
#include "Glut.hpp"
#include "Scene.hpp"
#include "Camera2.hpp"
#include "Camera3.hpp"

Photon::Scene::Scene(const std::string &name, Photon::Scene::WorldType type) :
    name(name),
    background(Color::BLACK),
    updater(nullptr),
    layers
    {
            {DEFAULT_LAYER, std::make_shared<Layer>(DEFAULT_LAYER)},
            {GUI_LAYER, std::make_shared<Layer>(GUI_LAYER)},
    },
    type(type) {}

void Photon::Scene::add(const std::shared_ptr<Photon::Drawable> &drawable)
{
    std::string layer = drawable->getLayer();
    if (layers.find(layer) != layers.end())
        layers[layer]->addDrawable(drawable);
    if (drawable->isUpdatable())
        updatables.emplace_back(drawable);
}

void Photon::Scene::load()
{
    loadHandler();
    if (updater != nullptr)
        updater->load();
    for (const auto &pair : layers)
        pair.second->load();
}

void Photon::Scene::unload()
{
    unloadHandler();
    if (updater != nullptr)
        updater->unload();
    for (const auto &pair : layers)
        pair.second->unload();
    for (auto &updatable : updatables)
        updatable->unload();
    updatables.clear();
}

void Photon::Scene::setBackground(float r, float g, float b)
{
    background.setRGB(r, g, b);
}

void Photon::Scene::setBackground(float r, float g, float b, float a)
{
    background.setRGB(r, g, b, a);
}

void Photon::Scene::setBackground(const Photon::Color &background)
{
    this->background = background;
}

void Photon::Scene::draw()
{
    glClearColor(background.getR(), background.getG(), background.getB(), background.getA());
    glLoadIdentity();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    cameras[DEFAULT_CAMERA]->update();
    for (const auto &pair : cameras)
    {
        auto camera = pair.second;
        if (camera->getName() != DEFAULT_CAMERA && camera->getName() != GUI_CAMERA)
            camera->update();
    }
    cameras[GUI_CAMERA]->update();
    glFlush();
    glutSwapBuffers();
}

void Photon::Scene::idle()
{
    for (auto &updatable : updatables)
    {
        if (!updatable->isPaused())
            updatable->update();
    }
    if (updater != nullptr)
        updater->update();
}

void Photon::Scene::setUpdater(const std::shared_ptr<Photon::Updater> &updater)
{
    if (isLoaded())
    {
        if (this->updater != nullptr)
            this->updater->unload();
        updater->load();
    }
    this->updater = updater;
    if (updater != nullptr)
        glutPostRedisplay();
}

bool Photon::Scene::addLayer(const std::string &name)
{
    if (layers.find(name) != layers.end())
    {
        Debugger::log("Photon::Scene", "addLayer", "Layer " + name + " already exist");
        return false;
    }
    std::shared_ptr<Layer> layerToAdd;
    layerToAdd = std::make_shared<Layer>(name);
    layers[name] = layerToAdd;
    for (const auto &camera : cameras)
        camera.second->addLayer(layerToAdd);
    return true;
}

bool Photon::Scene::addCamera(const std::string &name, WorldType type, const std::string &layer)
{
    if (cameras.find(name) != cameras.end())
    {
        Debugger::log("Photon::Scene", "addCamera", "Camera " + name + " already exist");
        return false;
    }
    std::shared_ptr<Camera> cameraToAdd;
    if (type == SPACE)
        cameraToAdd = std::make_shared<Camera3>(name, own);
    else
        cameraToAdd = std::make_shared<Camera2>(name, own);
    cameraToAdd->addLayer(layer);
    cameras[name] = cameraToAdd;
    return true;
}

const std::shared_ptr<Photon::Camera> Photon::Scene::getCamera(const std::string &name) const
{
    if (cameras.find(name) == cameras.end())
        return nullptr;
    return cameras.at(name);
}

const std::shared_ptr<Photon::Layer> Photon::Scene::getLayer(const std::string &name) const
{
    if (layers.find(name) == layers.end())
        return nullptr;
    return layers.at(name);
}

void Photon::Scene::setOwn(const std::weak_ptr<Photon::Scene> &own)
{
    this->own = own;
    addCamera(DEFAULT_CAMERA, type);
    addCamera(GUI_CAMERA, PLAN, GUI_LAYER);
    auto defaultCamera = cameras[DEFAULT_CAMERA];
    defaultCamera->setEnable(true);
    auto guiCamera = cameras[GUI_CAMERA];
    guiCamera->setEnable(true);
}
