#ifndef PHOTON_SCENE_HPP
#define PHOTON_SCENE_HPP

#include <cstring>
#include <memory>
#include <vector>
#include <unordered_map>
#include "Event_HandlerContainer.hpp"
#include "Camera.hpp"
#include "Color.hpp"
#include "Drawable.hpp"
#include "Enum.hpp"
#include "Updater.hpp"

namespace Photon
{
    class Renderer;
class Scene : public Photon::Event::HandlerContainer
    {
        friend class Renderer;
        friend class Updater;
    public:
        enum WorldType : int
        {
            PLAN,
            SPACE
        };
    private:
        std::string name;
        std::vector<std::shared_ptr<Drawable>> updatables;
        Color background;
        std::shared_ptr<Updater> updater;
        std::unordered_map<std::string, std::shared_ptr<Layer>> layers;
        std::unordered_map<std::string, std::shared_ptr<Camera>> cameras;
        WorldType type;
        std::weak_ptr<Scene> own;

    private:
        void load();
        void unload();
        void draw();
        void idle();
        void setUpdater(const std::shared_ptr<Updater> &);
        void setOwn(const std::weak_ptr<Scene> &);

    public:
        Scene(const std::string &, WorldType);
        void setBackground(float, float, float);
        void setBackground(float, float, float, float);
        void setBackground(const Color &);
        void add(const std::shared_ptr<Drawable> &);
        bool addLayer(const std::string &);
        bool addCamera(const std::string &, WorldType, const std::string & = DEFAULT_LAYER);
        const std::shared_ptr<Layer> getLayer(const std::string &) const;
        const std::shared_ptr<Camera> getCamera(const std::string &) const;
    };
}

#endif