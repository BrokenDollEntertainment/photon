#include "Debugger.hpp"
#include "OpenGL.hpp"
#include "Shader.hpp"

Photon::Shader::Shader(const std::string &shaderCode, const std::string &fragmentCode)
{
    std::vector<unsigned int> attachedShader;
    compileShader(true, shaderCode, attachedShader);
    compileShader(false, fragmentCode, attachedShader);
    linkProgram(attachedShader);
}

void Photon::Shader::linkProgram(const std::vector<unsigned int> &attachedShader)
{
    int success;
    int logLength;
    compiled = true;
    id = glCreateProgram();
    for (unsigned int shader : attachedShader)
        glAttachShader(id, shader);
    glLinkProgram(id);
    glGetProgramiv(id, GL_LINK_STATUS, &success);
    if (success == GL_FALSE)
    {
        compiled = false;
        glGetProgramiv(id, GL_INFO_LOG_LENGTH, &logLength);
        if (logLength > 0)
        {
            char *log = new char[logLength];
            glGetProgramInfoLog(id, logLength, nullptr, log);
            Debugger::log("Photon::Shader", "Shader", "Cannot build program: " + std::string(log));
            delete[](log);
        }
        else
            Debugger::log("Photon::Shader", "Shader", "Cannot build program");
    }
    for (unsigned int shader : attachedShader)
        glDeleteShader(shader);
}

void Photon::Shader::compileShader(bool vertex, const std::string &code, std::vector<unsigned int> &attachedShader)
{
    if (!code.empty())
    {
        int success;
        int logLength;
        const char *shaderCode = code.c_str();
        unsigned int shader = glCreateShader(GL_VERTEX_SHADER);
        if (vertex)
            shader = glCreateShader(GL_VERTEX_SHADER);
        else
            shader = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(shader, 1, &shaderCode, nullptr);
        glCompileShader(shader);
        glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
        if (success == GL_FALSE)
        {
            compiled = false;
            glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);
            std::string err = "Cannot build ";
            if (vertex)
                err += "vertex shader code";
            else
                err += "fragment shader code";
            if (logLength > 0)
            {
                char *log = new char[logLength];
                glGetShaderInfoLog(shader, logLength, nullptr, log);
                Debugger::log("Photon::Shader", "Shader", err + ": " + std::string(log));
                delete[](log);
            }
            else
                Debugger::log("Photon::Shader", "Shader", err);
            return;
        }
        attachedShader.emplace_back(shader);
    }
}

void Photon::Shader::enable()
{
    if (compiled)
        glUseProgram(id);
}

void Photon::Shader::disable()
{
    if (compiled)
        glUseProgram(0);
}

int Photon::Shader::getAttrib(const std::string &name)
{
    if (attribs.find(name) == attribs.end())
        attribs[name] = glGetAttribLocation(id, name.c_str());
    return attribs[name];
}

int Photon::Shader::getUniform(const std::string &name)
{
    if (uniforms.find(name) == uniforms.end())
        uniforms[name] = glGetUniformLocation(id, name.c_str());
    return uniforms[name];
}

void Photon::Shader::setUniform(const std::string &name, bool value)
{
    glUniform1i(getUniform(name), static_cast<int>(value));
}

void Photon::Shader::setUniform(const std::string &name, int value)
{
    glUniform1i(getUniform(name), value);
}

void Photon::Shader::setUniform(const std::string &name, float value)
{
    glUniform1f(getUniform(name), value);
}

void Photon::Shader::setUniform(const std::string &name, const Color &value)
{
    glUniform3f(getUniform(name), value.getR(), value.getG(), value.getB());
}

bool Photon::Shader::isCompiled() const
{
    return compiled;
}

unsigned int Photon::Shader::getId() const
{
    return id;
}
