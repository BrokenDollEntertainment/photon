#ifndef PHOTON_SHADER_HPP
#define PHOTON_SHADER_HPP

#include <string>
#include <unordered_map>
#include "Color.hpp"

namespace Photon
{
    class Shader
    {
    private:
        bool compiled;
        unsigned int id;
        std::unordered_map<std::string, int> attribs;
        std::unordered_map<std::string, int> uniforms;

    private:
        void compileShader(bool, const std::string &, std::vector<unsigned int> &);
        void linkProgram(const std::vector<unsigned int> &);

    public:
        Shader(const std::string &, const std::string &);
        Shader(const Shader &) = default;
        Shader &operator=(const Shader &) = default;
        void enable();
        void disable();
        int getAttrib(const std::string &);
        int getUniform(const std::string &);
        void setUniform(const std::string &, bool);
        void setUniform(const std::string &, int);
        void setUniform(const std::string &, float);
        void setUniform(const std::string &, const Color &);
        bool isCompiled() const;
        unsigned int getId() const;
    };
}

#endif
