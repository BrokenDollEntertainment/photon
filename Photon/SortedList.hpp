#ifndef PHOTON_SORTEDLIST_HPP
#define PHOTON_SORTEDLIST_HPP

#include <vector>

namespace Photon
{
    template <typename T>
    class SortedList : public std::vector<T>
    {
    private:
        unsigned long vectorSize;
        std::vector<int> weights;

    private:
        unsigned long getPos(const T &item)
        {
            for (unsigned long n = 0; n != vectorSize; ++n)
            {
                if (std::vector<T>::at(n) == item)
                    return n;
            }
            return vectorSize;
        }

        void insert(const T &item, int weight, unsigned long pos)
        {
            weights.insert(weights.begin() + pos, weight);
            std::vector<T>::insert(std::vector<T>::begin() + pos, item);
            ++vectorSize;
        }

    public:
        SortedList() : vectorSize(0) {}

        void add(const T &item, int weight)
        {
            if (vectorSize == 0)
            {
                insert(item, weight, vectorSize);
                return;
            }
            unsigned long left = 0;
            unsigned long right = vectorSize - 1;
            while ((weights[left] != weights[right]) && (weight > weights[left]) && (weight < weights[right]))
            {
                auto pos = left + ((right - left) * ((weight - weights[left]) / (weights[right] - weights[left])));
                if (weights[pos] < weight)
                    left = pos + 1;
                else if (weights[pos] > weight)
                    right = pos - 1;
                else
                {
                    insert(item, weight, pos - 1);
                    return;
                }
            }
            if (weight <= weights[left])
                insert(item, weight, left);
            else if (weight >= weights[right])
                insert(item, weight, right + 1);
        }

        bool remove(const T &item)
        {
            unsigned long pos = getPos(item);
            if (pos != vectorSize)
            {
                weights.erase(weights.begin() + pos);
                std::vector<T>::erase(std::vector<T>::begin() + pos);
                --vectorSize;
                return true;
            }
            return false;
        }

        bool find(const T &item)
        {
            return (getPos(item) != vectorSize);
        }

        void clear()
        {
            vectorSize = 0;
            weights.clear();
            std::vector<T>::clear();
            weights.shrink_to_fit();
            std::vector<T>::shrink_to_fit();
        }

        ~SortedList()
        {
            weights.clear();
            std::vector<T>::clear();
            weights.shrink_to_fit();
            std::vector<T>::shrink_to_fit();
        }
    };
}

#endif
