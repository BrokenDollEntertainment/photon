#include "Texture_Box.hpp"

Photon::Texture::Builder &Photon::Texture::Box::operator[](Face face)
{
    switch (face)
    {
        case TOP:
            return top;
        case BOTTOM:
            return bottom;
        case FRONT:
            return front;
        case BACK:
            return back;
        case LEFT:
            return left;
        default:
            return right;
    }
}

const Photon::Texture::Builder &Photon::Texture::Box::operator[](Face face) const
{
    switch (face)
    {
        case TOP:
            return top;
        case BOTTOM:
            return bottom;
        case FRONT:
            return front;
        case BACK:
            return back;
        case LEFT:
            return left;
        default:
            return right;
    }
}