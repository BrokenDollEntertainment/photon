#ifndef PHOTON_TEXTURE_BOX_HPP
#define PHOTON_TEXTURE_BOX_HPP

#include "Texture_Builder.hpp"

namespace Photon::Texture
{
    class Box
    {
    public:
        enum Face
        {
            TOP,
            BOTTOM,
            FRONT,
            BACK,
            LEFT,
            RIGHT
        };

    private:
        Builder top;
        Builder bottom;
        Builder front;
        Builder back;
        Builder left;
        Builder right;

    public:
        Builder &operator[](Face);
        const Builder &operator[](Face) const;
    };
}

#endif
