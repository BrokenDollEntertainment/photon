#include "Texture_Builder.hpp"

const Photon::Texture::Texture &Photon::Texture::Builder::build() const
{
    return texture;
}

void Photon::Texture::Builder::setRect(float x, float y, float width, float height)
{
    texture.setRect(x, y, width, height);
}

void Photon::Texture::Builder::setRectPos(float x, float y)
{
    texture.setRectPos(x, y);
}

void Photon::Texture::Builder::setRectDim(float width, float height)
{
    texture.setRectDim(width, height);
}

void Photon::Texture::Builder::setRectWidth(float width)
{
    texture.setRectWidth(width);
}

void Photon::Texture::Builder::setRectHeight(float height)
{
    texture.setRectHeight(height);
}

void Photon::Texture::Builder::setTextureMode(Texture::TextureMode textureModeWidth, Texture::TextureMode textureModeHeight)
{
    texture.setTextureMode(textureModeWidth, textureModeHeight);
}

int Photon::Texture::Builder::getWidth() const
{
    return texture.getWidth();
}

int Photon::Texture::Builder::getHeight() const
{
    return texture.getHeight();
}

void Photon::Texture::Builder::operator()(const Photon::Texture::Texture &texture)
{
    this->texture = texture;
}

void Photon::Texture::Builder::operator()(const std::string &texture, int comp)
{
    this->texture = Loader::load(texture, comp);
}

void Photon::Texture::Builder::operator()(const Photon::Color &color)
{
    this->texture = Texture(color);
}
