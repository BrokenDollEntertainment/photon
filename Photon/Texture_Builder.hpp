#ifndef PHOTON_TEXTURE_BUILDER_HPP
#define PHOTON_TEXTURE_BUILDER_HPP

#include "Texture_Loader.hpp"

namespace Photon::Texture
{
    class Builder
    {
    private:
        Texture texture;

    public:
        const Texture &build() const;
        void setRect(float, float, float, float);
        void setRectPos(float, float);
        void setRectDim(float, float);
        void setRectWidth(float);
        void setRectHeight(float);
        void setTextureMode(Texture::TextureMode, Texture::TextureMode);
        int getWidth() const;
        int getHeight() const;
        void operator()(const Texture &);
        void operator()(const std::string &, int = -1);
        void operator()(const Color &);
    };
}

#endif
