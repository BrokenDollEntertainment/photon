#include <cstring>
#include "Debugger.hpp"
#include "GLUtil.hpp"
#include "Texture_Loader.hpp"

std::unordered_map<std::string, std::shared_ptr<Photon::Texture::Texture::Inner>> Photon::Texture::Loader::textures;

const Photon::Texture::Texture Photon::Texture::Loader::load(const std::string &path, int channels)
{
    if (textures.find(path) == textures.end())
    {
        GLuint texId;
        int width, height, comp;
        unsigned char *image = Stb::Image::load(path, &width, &height, &comp, channels);
        if (image == nullptr)
        {
            Debugger::log("Photon::Texture::Loader", "load", "Cannot load image " + path + ": " + ::strerror(errno));
            return Texture();
        }
        glGenTextures(1, &texId);
        glBindTexture(GL_TEXTURE_2D, texId);
        if (comp == 3)
            gluBuild2DMipmaps(GL_TEXTURE_2D, 3, width, height, GL_RGB, GL_UNSIGNED_BYTE, image);
        else if (comp == 4)
            gluBuild2DMipmaps(GL_TEXTURE_2D, 4, width, height, GL_RGBA, GL_UNSIGNED_BYTE, image);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glBindTexture(GL_TEXTURE_2D, 0);
        Stb::Image::free(image);
        textures[path] = std::make_shared<Texture::Inner>(width, height, texId);
    }
    return Texture(textures.at(path));
}
