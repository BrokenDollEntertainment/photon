#ifndef PHOTON_TEXTURE_LOADER_HPP
#define PHOTON_TEXTURE_LOADER_HPP

#include <memory>
#include <unordered_map>
#include "Texture_Texture.hpp"

namespace Photon::Texture
{
    class Loader
    {
    private:
        static std::unordered_map<std::string, std::shared_ptr<Texture::Inner>> textures;

    public:
        static const Texture load(const std::string &, int = -1);
    };
}

#endif
