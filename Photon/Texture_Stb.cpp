#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb_image_write.h>
#include "Renderer.hpp"
#include "Texture_Stb.hpp"

const char* Photon::Texture::Stb::Image::errorMessage()
{
    return stbi_failure_reason();
}

unsigned char *Photon::Texture::Stb::Image::load(const std::string &filename, int *width, int *height, int *channelInFile, int targetChannels)
{
    int realChannels = targetChannels;
    if (realChannels == -1)
    {
        auto image = stbi_load(filename.c_str(), width, height, &realChannels, 0);
        stbi_image_free(image);
    }
    switch (realChannels)
    {
        case 3:
            return stbi_load(filename.c_str(), width, height, channelInFile, STBI_rgb);
        case 4:
            return stbi_load(filename.c_str(), width, height, channelInFile, STBI_rgb_alpha);
        default:
            return stbi_load(filename.c_str(), width, height, channelInFile, 0);
    }
}

void Photon::Texture::Stb::Image::save(const std::string &filename, int type, int width, int height, unsigned char *pixels)
{
    stbi_flip_vertically_on_write(1);
    switch (static_cast<ImageType>(type))
    {
        case ImageType::BMP:
            stbi_write_bmp((filename + ".bmp").c_str(), width, height, 3, pixels);
            break;
        case ImageType::TGA:
            stbi_write_tga((filename + ".tga").c_str(), width, height, 3, pixels);
            break;
        case ImageType::JPG:
            stbi_write_jpg((filename + ".jpg").c_str(), width, height, 3, pixels, 100);
            break;
        default:
            stbi_write_png((filename + ".png").c_str(), width, height, 4, pixels, 4 * width);
            break;
    }
}

void Photon::Texture::Stb::Image::free(unsigned char *image)
{
    if (image != nullptr)
        stbi_image_free(image);
}
