#ifndef PHOTON_STB_HPP
#define PHOTON_STB_HPP

#include <string>

namespace Photon::Texture::Stb
{
    class Image
    {
	public:
		static const char* errorMessage();
        static unsigned char *load(const std::string &, int *, int *, int *, int);
        static void save(const std::string &, int, int, int, unsigned char *);
        static void free(unsigned char *);
    };
}

#endif
