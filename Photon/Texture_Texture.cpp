#include "OpenGL.hpp"
#include "Texture_Texture.hpp"

Photon::Texture::Texture::Inner::Inner(int width, int height, unsigned int texId) :
        width(width), height(height), texId(texId) {}

void Photon::Texture::Texture::Inner::bind() const
{
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texId);
    glEnable(GL_TEXTURE_2D);
}

void Photon::Texture::Texture::Inner::unbind() const
{
    glDisable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, 0);
}

int Photon::Texture::Texture::Inner::getWidth() const
{
    return width;
}

int Photon::Texture::Texture::Inner::getHeight() const
{
    return height;
}

Photon::Texture::Texture::Inner::~Inner()
{
    glDeleteTextures(1, &texId);
}

Photon::Texture::Texture::Rectangle::Rectangle(float x, float y, float width, float height) :
        x(x), y(y), width(width), height(height) {}

Photon::Texture::Texture::Texture() : def(true), texture(nullptr), rect(std::make_shared<Texture::Rectangle>(0.0f, 0.0f, 0.0f, 0.0f)) {}

Photon::Texture::Texture::Texture(const std::shared_ptr<Photon::Texture::Texture::Inner> &texture) :
    textureModeWidth(REPEAT), textureModeHeight(REPEAT), def(false), texture(texture),
    rect(std::make_shared<Texture::Rectangle>(0.0f, 0.0f, static_cast<float>(texture->getWidth()), static_cast<float>(texture->getHeight()))) {}

Photon::Texture::Texture::Texture(int width, int height, unsigned int texId) :
    textureModeWidth(REPEAT), textureModeHeight(REPEAT), def(false), texture(std::make_shared<Texture::Inner>(width, height, texId)),
    rect(std::make_shared<Texture::Rectangle>(0.0f, 0.0f, static_cast<float>(width), static_cast<float>(height))) {}

Photon::Texture::Texture::Texture(const Photon::Color &color) :
    textureModeWidth(REPEAT), textureModeHeight(REPEAT), def(false), texture(nullptr),
    rect(std::make_shared<Texture::Rectangle>(0.0f, 0.0f, 0.0f, 0.0f))
{
    colors.emplace_back(color);
}

void Photon::Texture::Texture::bind() const
{
    if (texture != nullptr)
    {
        texture->bind();
        if (textureModeWidth == REPEAT)
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        else if (textureModeWidth == MIRRORED)
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
        else
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        if (textureModeHeight == REPEAT)
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        else if (textureModeHeight == MIRRORED)
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
        else
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    }
}

void Photon::Texture::Texture::unbind() const
{
    if (texture != nullptr)
        texture->unbind();
}

void Photon::Texture::Texture::coord(float x, float y) const
{
    if (texture == nullptr)
        return;
    float realX = ((x * rect->width) + rect->x) / texture->getWidth();
    float realY = ((y * rect->height) + rect->y) / texture->getHeight();
    glTexCoord2f(realX, realY);
}

void Photon::Texture::Texture::coord(const float arr[2]) const
{
    coord(arr[0], arr[1]);
}

void Photon::Texture::Texture::clearColor()
{
    colors.clear();
}

void Photon::Texture::Texture::addColor(const Photon::Color &color)
{
    colors.emplace_back(color);
}

void Photon::Texture::Texture::color(unsigned int idx) const
{
    if (colors.empty())
    {
        glColor4f(1, 1, 1, 1);
        return;
    }
    auto color = colors[idx % colors.size()];
    glColor4f(color.getR() / 255, color.getG() / 255, color.getB() / 255, color.getA() / 255);
}

const Photon::Color Photon::Texture::Texture::getColor(unsigned int idx) const
{
    if (colors.empty())
        return Color::WHITE;
    return colors[idx % colors.size()];
}

void Photon::Texture::Texture::setRect(float x, float y, float width, float height)
{
    rect->width = width;
    rect->height = height;
    rect->x = x;
    rect->y = y;
}

void Photon::Texture::Texture::setRectPos(float x, float y)
{
    rect->x = x;
    rect->y = y;
}

void Photon::Texture::Texture::setRectDim(float width, float height)
{
    rect->width = width;
    rect->height = height;
}

void Photon::Texture::Texture::setRectWidth(float width)
{
    rect->width = width;
}

void Photon::Texture::Texture::setRectHeight(float height)
{
    rect->height = height;
}

int Photon::Texture::Texture::getWidth() const
{
    if (texture != nullptr)
        return texture->getWidth();
    return 0;
}

int Photon::Texture::Texture::getHeight() const
{
    if (texture != nullptr)
        return texture->getHeight();
    return 0;
}

bool Photon::Texture::Texture::operator==(const Photon::Texture::Texture &other) const
{
    return def == other.def &&
           texture == other.texture &&
           rect == other.rect &&
           colors == other.colors;
}

bool Photon::Texture::Texture::operator!=(const Photon::Texture::Texture &other) const
{
    return !(other == *this);
}

bool Photon::Texture::Texture::isDefault() const
{
    return def;
}

void Photon::Texture::Texture::setTextureMode(TextureMode textureModeWrapperX, TextureMode textureModeWrapperY)
{
    this->textureModeWidth = textureModeWrapperX;
    this->textureModeHeight = textureModeWrapperY;
}

