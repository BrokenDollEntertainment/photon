#ifndef PHOTON_TEXTURE_TEXTURE_HPP
#define PHOTON_TEXTURE_TEXTURE_HPP

#include <memory>
#include <string>
#include <vector>
#include "Color.hpp"
#include "Texture_Stb.hpp"

namespace Photon::Texture
{
    class Loader;
    class Texture
    {
        friend class Loader;
    public:
        enum TextureMode
        {
            CLAMP,
            MIRRORED,
            REPEAT
        };
    private:
        class Inner
        {
        private:
            int width;
            int height;
            unsigned int texId;

        public:
            Inner(int, int, unsigned int);
            int getWidth() const;
            int getHeight() const;
            void bind() const;
            void unbind() const;
            ~Inner();
        };
        struct Rectangle
        {
            float x;
            float y;
            float width;
            float height;
            Rectangle(float x, float y, float width, float height);
        };

    private:
        TextureMode textureModeWidth;
        TextureMode textureModeHeight;
        bool def;
        std::shared_ptr<Texture::Inner> texture;
        std::shared_ptr<Texture::Rectangle> rect;
        std::vector<Color> colors;

    private:
        Texture(const std::shared_ptr<Texture::Inner> &);

    public:
        Texture();
        Texture(const Color &);
        Texture(int, int, unsigned int);
        Texture(const Texture &) = default;
        Texture &operator=(const Texture &) = default;
        bool operator==(const Texture &) const;
        bool operator!=(const Texture &) const;
        void clearColor();
        void addColor(const Color &);
        void color(unsigned int) const;
        const Color getColor(unsigned int) const;
        void coord(float, float) const;
        void coord(const float [2]) const;
        void setRect(float, float, float, float);
        void setRectPos(float, float);
        void setRectDim(float, float);
        void setRectWidth(float);
        void setRectHeight(float);
        int getWidth() const;
        int getHeight() const;
        void bind() const;
        void unbind() const;
        bool isDefault() const;
        void setTextureMode(TextureMode, TextureMode);
    };
}

#endif
