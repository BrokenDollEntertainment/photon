#include "Mouse.hpp"
#include "UI_AButtonEventHandler.hpp"

Photon::UI::AButtonEventHandler::AButtonEventHandler(const std::shared_ptr<Button> &button) : button(button) {}

void Photon::UI::AButtonEventHandler::handle(const std::shared_ptr<Photon::Event::MouseEvent> &event)
{
    Vector2f mousePos = Mouse::getPosition();
    Vector2f position = getButtonPos();
    bool mouseOnButton = false;
    bool mouseIsClicked = Mouse::isClicked(MouseKey::LEFTCLICK);
    if (mousePos.getX() >= position.getX() &&
        mousePos.getX() <= (position.getX() + button->getWidth()) &&
        mousePos.getY() >= position.getY() &&
        mousePos.getY() <= (position.getY() + button->getHeight()))
        mouseOnButton = true;
    auto state = button->getState();
    if (state == UI::ButtonState::RELEASED && mouseOnButton && !mouseIsClicked)
        button->setState(UI::ButtonState::HOVER);
    else if (state == UI::ButtonState::HOVER && !mouseOnButton)
        button->setState(UI::ButtonState::RELEASED);
    else if (state == UI::ButtonState::HOVER && mouseIsClicked)
    {
        event->consume();
        button->setState(UI::ButtonState::PRESSED);
    }
    else if (state == UI::ButtonState::PRESSED)
    {
        event->consume();
        handle(mousePos, mouseIsClicked, mouseOnButton);
    }
}
