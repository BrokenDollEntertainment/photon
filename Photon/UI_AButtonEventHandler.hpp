#ifndef PHOTON_ABUTTONEVENTHANDLER_HPP
#define PHOTON_ABUTTONEVENTHANDLER_HPP

#include "Event_MouseEvent.hpp"
#include "UI_GuiHandler.hpp"
#include "UI_Button.hpp"

namespace Photon::UI
{
    class AButtonEventHandler : public GuiHandler<Photon::Event::MouseEvent>
    {
    protected:
        std::shared_ptr<Button> button;

    private:
        virtual void handle(const Vector2f &, bool, bool) = 0;
        virtual Vector2f getButtonPos() = 0;

    public:
        AButtonEventHandler(const std::shared_ptr<Button> &button);
        void handle(const std::shared_ptr<Photon::Event::MouseEvent> &) override;
    };
}

#endif
