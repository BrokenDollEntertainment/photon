#include "UI_Button.hpp"

Photon::UI::Button::Button(const std::string &text, float width, float height) :
    noHover(false), state(UI::ButtonState::RELEASED),
    width(width), height(height), text(text),
    releasedDrawable(width, height, Color::WHITE),
    pressedDrawable(width, height, Color::RED),
    hoverDrawable(width, height, Color::BLUE),
    haveAction(false)
{
    init();
}

Photon::UI::Button::Button(const std::string &text, float width, float height,
        const Photon::Color &releasedColor, const Photon::Color &pressedColor) :
    noHover(false), state(UI::ButtonState::RELEASED),
    width(width), height(height), text(text),
    releasedDrawable(width, height, releasedColor),
    pressedDrawable(width, height, pressedColor),
    hoverDrawable(width, height, releasedColor),
    haveAction(false)
{
    init();
}

void Photon::UI::Button::init()
{
    text.setMargin(0, height / 2);
}

void Photon::UI::Button::draw()
{
    switch (state)
    {
        case UI::ButtonState::PRESSED:
            pressedDrawable.render();
            break;
        case UI::ButtonState::HOVER:
            if (noHover)
                releasedDrawable.render();
            else
                hoverDrawable.render();
            break;
        case UI::ButtonState::RELEASED:
            releasedDrawable.render();
            break;
    }
    text.render();
}

void Photon::UI::Button::setText(const std::string &text)
{
    this->text.setText(text);
}

void Photon::UI::Button::setTextColor(const Color &color)
{
    text.getMaterial()->setColor(color);
}

void Photon::UI::Button::setReleasedColor(const Photon::Color &color)
{
    releasedDrawable.getMaterial()->setColor(color);
}

void Photon::UI::Button::setPressedColor(const Photon::Color &color)
{
    pressedDrawable.getMaterial()->setColor(color);
}

void Photon::UI::Button::setHoverColor(const Photon::Color &color)
{
    hoverDrawable.getMaterial()->setColor(color);
}

void Photon::UI::Button::setReleasedTexture(const Texture::Texture &texture)
{
    auto material = releasedDrawable.getMaterial();
    material->clearTextures();
    material->addTexture(texture);
}

void Photon::UI::Button::setPressedTexture(const Texture::Texture &texture)
{
    auto material = pressedDrawable.getMaterial();
    material->clearTextures();
    material->addTexture(texture);
}

void Photon::UI::Button::setHoverTexture(const Texture::Texture &texture)
{
    auto material = hoverDrawable.getMaterial();
    material->clearTextures();
    material->addTexture(texture);
}

void Photon::UI::Button::onClick(const std::function<void()> &action)
{
    this->action = action;
    haveAction = true;
}

void Photon::UI::Button::setNoHover(bool noHover)
{
    this->noHover = noHover;
}

Photon::UI::ButtonState Photon::UI::Button::getState() const
{
    return state;
}

void Photon::UI::Button::setState(Photon::UI::ButtonState state)
{
    this->state = state;
}

float Photon::UI::Button::getWidth() const
{
    return width;
}

float Photon::UI::Button::getHeight() const
{
    return height;
}

void Photon::UI::Button::clicked()
{
    if (haveAction)
        action();
}

void Photon::UI::Button::setDimension(float width, float height)
{
    this->width = width;
    this->height = height;
    releasedDrawable.setDimension(width, height);
    pressedDrawable.setDimension(width, height);
    hoverDrawable.setDimension(width, height);
}
