#ifndef PHOTON_UI_BUTTON_HPP
#define PHOTON_UI_BUTTON_HPP

#include "UI_Enum.hpp"
#include "UI_Gui.hpp"
#include "UI_Text.hpp"
#include "Object_Rectangle.hpp"

namespace Photon::UI
{
    class Button : public Gui
    {
    private:
        bool noHover;
        UI::ButtonState state;
        float width;
        float height;
        Text text;
        Photon::Object::Rectangle releasedDrawable;
        Photon::Object::Rectangle pressedDrawable;
        Photon::Object::Rectangle hoverDrawable;
        bool haveAction;
        std::function<void()> action;

    private:
        void init();
        void draw() override;

    public:
        Button(const std::string &, float, float);
        Button(const std::string &, float, float, const Color &, const Color &);
        UI::ButtonState getState() const;
        void setState(UI::ButtonState state);
        float getWidth() const;
        float getHeight() const;
        void setDimension(float, float);
        void clicked();
        void setText(const std::string &);
        void setTextColor(const Color &);
        void setReleasedColor(const Color &);
        void setPressedColor(const Color &);
        void setHoverColor(const Color &);
        void setReleasedTexture(const Texture::Texture &);
        void setPressedTexture(const Texture::Texture &);
        void setHoverTexture(const Texture::Texture &);
        void onClick(const std::function<void()> &);
        void setNoHover(bool);
    };
}

#endif