#include "Mouse.hpp"
#include "UI_ButtonEventHandler.hpp"

Photon::UI::ButtonEventHandler::ButtonEventHandler(const std::shared_ptr<Button> &button) : AButtonEventHandler(button) {}

Photon::Vector2f Photon::UI::ButtonEventHandler::getButtonPos()
{
    return button->getPosition();
}

void Photon::UI::ButtonEventHandler::handle(const Vector2f &, bool mouseIsClicked, bool mouseOnButton)
{
    if (!mouseIsClicked)
    {
        if (mouseOnButton)
            button->setState(UI::ButtonState::HOVER);
        else
            button->setState(UI::ButtonState::RELEASED);
        button->clicked();
    }
}
