#ifndef PHOTON_UI_BUTTONEVENTHANDLER_HPP
#define PHOTON_UI_BUTTONEVENTHANDLER_HPP

#include "UI_AButtonEventHandler.hpp"

namespace Photon::UI
{
    class ButtonEventHandler : public AButtonEventHandler
    {
    private:
        void handle(const Vector2f &, bool, bool) override;
        Vector2f getButtonPos() override;

    public:
        ButtonEventHandler(const std::shared_ptr<Button> &button);
    };
}

#endif
