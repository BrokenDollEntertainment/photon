#ifndef PHOTON_UI_ENUM_HPP
#define PHOTON_UI_ENUM_HPP

namespace Photon::UI
{
    enum struct ButtonState : int
    {
        PRESSED,
        RELEASED,
        HOVER
    };

    enum struct SliderDirection : int
    {
        VERTICAL,
        HORIZONTAL
    };
}

#endif
