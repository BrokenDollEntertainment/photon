#include "UI_Factory.hpp"
#include "UI_ButtonEventHandler.hpp"
#include "UI_SliderEventHandler.hpp"

std::shared_ptr<Photon::UI::Text> Photon::UI::Factory::createText(const std::string &text)
{
    return std::make_shared<Photon::UI::Text>(text);
}

std::shared_ptr<Photon::UI::Text> Photon::UI::Factory::createText(const std::string &text, const std::string &font, const Color &color)
{
    return std::make_shared<Photon::UI::Text>(text, std::make_shared<Font>(font, 24, color));
}

std::shared_ptr<Photon::UI::Text> Photon::UI::Factory::createText(const std::string &text, const std::string &font, unsigned int pointSize, const Color &color)
{
    return std::make_shared<Photon::UI::Text>(text, std::make_shared<Font>(font, pointSize, color));
}

std::shared_ptr<Photon::UI::Button> Photon::UI::Factory::createButton(const std::string &text, int width, int height)
{
    auto button = std::make_shared<Button>(text, static_cast<float>(width), static_cast<float>(height));
    button->addEventHandler(std::make_shared<ButtonEventHandler>(button));
    return button;
}

std::shared_ptr<Photon::UI::Button> Photon::UI::Factory::createButton(const std::string &text, int width, int height,
                                                               const Photon::Color &releasedColor, const Photon::Color &pressedColor)
{
    auto button = std::make_shared<Button>(text, static_cast<float>(width), static_cast<float>(height), releasedColor, pressedColor);
    button->addEventHandler(std::make_shared<ButtonEventHandler>(button));
    return button;
}

std::shared_ptr<Photon::UI::Slider> Photon::UI::Factory::createSlider(SliderDirection direction, float size, float thickness, float sliderSize)
{
    auto slider = std::make_shared<Slider>(direction, size, thickness, sliderSize, sliderSize);
    slider->addEventHandler(std::make_shared<SliderEventHandler>(slider));
    return slider;
}

std::shared_ptr<Photon::UI::Slider> Photon::UI::Factory::createSlider(SliderDirection direction, float size, float thickness, float sliderSize, float sliderThickness)
{
    auto slider = std::make_shared<Slider>(direction, size, thickness, sliderSize, sliderThickness);
    slider->addEventHandler(std::make_shared<SliderEventHandler>(slider));
    return slider;
}