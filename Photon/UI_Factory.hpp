#ifndef PHOTON_UI_FACTORY_HPP
#define PHOTON_UI_FACTORY_HPP

#include "UI_Button.hpp"
#include "UI_Slider.hpp"

namespace Photon::UI
{
    class Factory
    {
    public:
        static std::shared_ptr<Text> createText(const std::string &);
        static std::shared_ptr<Text> createText(const std::string &, const std::string &, const Color &);
        static std::shared_ptr<Text> createText(const std::string &, const std::string &, unsigned int = 24, const Color & = Color::BLACK);
        static std::shared_ptr<Button> createButton(const std::string &, int, int);
        static std::shared_ptr<Button> createButton(const std::string &, int, int, const Color &, const Color &);
        static std::shared_ptr<Slider> createSlider(SliderDirection, float, float, float);
        static std::shared_ptr<Slider> createSlider(SliderDirection, float, float, float, float);
    };
}

#endif
