#include "Debugger.hpp"
#include "OpenGL.hpp"
#include "PixelArray.hpp"
#include "View.hpp"
#include "UI_Font.hpp"

bool Photon::UI::Font::inited = false;
FT_Library Photon::UI::Font::ft;

void Photon::UI::Font::init()
{
    if (FT_Init_FreeType(&ft))
    {
        inited = false;
        Debugger::log("Photon::UI::Font", "init", "Cannot init FreeType");
        return;
    }
    inited = true;
}

void Photon::UI::Font::clean()
{
    FT_Done_FreeType(ft);
}

Photon::UI::Font::Font(const std::string &font, unsigned int pointSize, const Color &color):
    font(font), pointSize(pointSize), color(color)
{
    loadFont();
}

void Photon::UI::Font::loadFont()
{
    if (!inited)
    {
        valid = false;
        return;
    }
    FT_Face face;
    if (FT_New_Face(ft, font.c_str(), 0, &face))
    {
        valid = false;
        Debugger::log("Photon::UI::Font", "loadFont", "Cannot load font " + font);
        return;
    }
    characters.clear();
    FT_Set_Pixel_Sizes(face, 0, pointSize);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    for (unsigned char c = 0; c < 128; ++c)
    {
        if (FT_Load_Char(face, c, FT_LOAD_RENDER) == 0)
        {
            unsigned int width = face->glyph->bitmap.width;
            unsigned int height = face->glyph->bitmap.rows;
            PixelArray array(width, height);
            for (unsigned int y = 0; y != height; ++y)
            {
                for (unsigned int x = 0; x != width; ++x)
                {
                    char red = face->glyph->bitmap.buffer[(y * width) + x];
                    array.set(x, y, color.getR(), color.getG(), color.getB(), red);
                }
            }
            auto character = std::make_shared<Object::Rectangle>(static_cast<float>(width), static_cast<float>(height));
            character->getMaterial()->addTexture(array.toTexture());
            characters.insert(std::make_pair(c, character));
        }
        else
            Debugger::log("Photon::UI::Font", "loadFont", "Cannot load char " + std::to_string(static_cast<int>(c)));
    }
    valid = true;
    glBindTexture(GL_TEXTURE_2D, 0);
    FT_Done_Face(face);
}

void Photon::UI::Font::render(const std::string &text, const Vector2f &margin, const Vector2f &scale)
{
    float x = margin.getX();
    float y = margin.getY();
    for (char textChar : text)
    {
        auto c = static_cast<unsigned char>(textChar);
        if (c == ' ')
            x += (static_cast<float>(pointSize) / 4);
        else if (characters.find(c) != characters.end())
        {
            auto ch = characters[c];
            ch->setPosition(x, y);
            ch->render();
            x += ch->getWidth() * scale.getX();
        }
        else
        {
            float xPos = x;
            float yPos = y - pointSize;
            float w = pointSize;
            float h = pointSize;
            glBegin(GL_QUADS);
                glVertex2f(xPos, yPos);
                glVertex2f(xPos, h);
                glVertex2f(w, h);
                glVertex2f(w, yPos);
            glEnd();
            x += pointSize;
        }
    }
    glBindTexture(GL_TEXTURE_2D, 0);
}

void Photon::UI::Font::setPointSize(unsigned int pointSize)
{
    this->pointSize = pointSize;
    loadFont();
}

void Photon::UI::Font::setColor(const Photon::Color &color)
{
    this->color = color;
    loadFont();
}
