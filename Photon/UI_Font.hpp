#ifndef PHOTON_UI_FONT_HPP
#define PHOTON_UI_FONT_HPP

#include <ft2build.h>
#include FT_FREETYPE_H
#include <map>
#include <string>
#include "Color.hpp"
#include "Object_Rectangle.hpp"

namespace Photon
{
    class Renderer;
    namespace UI
    {
        class Text;
        class Font
        {
            friend class Text;
            friend class Photon::Renderer;
        private:
            static bool inited;
            static FT_Library ft;

        private:
            static void init();
            static void clean();

        private:
            bool valid;
            std::string font;
            unsigned int pointSize;
            Color color;
            std::map<unsigned char, std::shared_ptr<Object::Rectangle>> characters;

        private:
            void loadFont();
            void render(const std::string &, const Vector2f &, const Vector2f &);

        public:
            Font(const std::string &, unsigned int = 24, const Color & = Color::BLACK);
            void setPointSize(unsigned int);
            void setColor(const Color &);
        };
    }
}

#endif
