#include "UI_Gui.hpp"
#include "Enum.hpp"

Photon::UI::Gui::Gui()
{
    setLayer(GUI_LAYER);
}

void Photon::UI::Gui::load()
{
    loadHandler();
}

void Photon::UI::Gui::unload()
{
    unloadHandler();
}