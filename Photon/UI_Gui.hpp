#ifndef PHOTON_UI_GUI_HPP
#define PHOTON_UI_GUI_HPP

#include "Drawable2.hpp"
#include "Event_HandlerContainer.hpp"

namespace Photon
{
    class Layer;
    namespace UI
    {
        class Button;
        class Gui : public Photon::Drawable2, public Photon::Event::HandlerContainer
        {
            friend class Photon::Layer;
        private:
            void load() override;
            void unload() override;

        protected:
            Gui();
        };
    };
}

#endif
