#ifndef PHOTON_UI_GUIHANDLER_HPP
#define PHOTON_UI_GUIHANDLER_HPP

#include "Event_Handler.hpp"

namespace Photon::UI
{
    template <typename T>
    class GuiHandler : public Photon::Event::Handler<T>
    {
    public:
        GuiHandler() : Photon::Event::Handler<T>(1) {}
    };
}
#endif
