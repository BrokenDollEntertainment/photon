#include "OpenGL.hpp"
#include "UI_Slider.hpp"

Photon::UI::Slider::Slider(Photon::UI::SliderDirection direction, float size, float thickness, float sliderSize, float sliderThickness) :
        bar((direction == UI::SliderDirection::HORIZONTAL) ? size : thickness,
            (direction == UI::SliderDirection::HORIZONTAL) ? thickness : size),
        direction(direction),
        size(size),
        thickness(thickness),
        sliderSize(sliderSize),
        sliderThickness(sliderThickness),
        percent(0),
        haveAction(false)
{

    if (direction == UI::SliderDirection::HORIZONTAL)
        slider = std::make_shared<Button>("", sliderThickness, sliderSize);
    else
        slider = std::make_shared<Button>("", sliderSize, sliderThickness);
    refreshSliderPosition();
}

void Photon::UI::Slider::refreshSliderPosition()
{
    float halfSliderSize = sliderSize / 2;
    float halfSliderThickness = sliderThickness / 2;
    float halfThickness = thickness / 2;
    float position = size * (percent / 100);
    if (direction == UI::SliderDirection::HORIZONTAL)
        slider->setPosition({position - halfSliderThickness, halfThickness - halfSliderSize});
    else
        slider->setPosition({halfThickness - halfSliderSize, position - halfSliderThickness});
}

const std::shared_ptr<Photon::UI::Button> &Photon::UI::Slider::getSlider() const
{
    return slider;
}

void Photon::UI::Slider::draw()
{
    bar.render();
    slider->render();
}

void Photon::UI::Slider::setDirection(Photon::UI::SliderDirection direction)
{
    this->direction = direction;
    if (direction == UI::SliderDirection::HORIZONTAL)
    {
        bar.setDimension(size, thickness);
        slider->setDimension(sliderThickness, sliderSize);
    }
    else
    {
        bar.setDimension(thickness, size);
        slider->setDimension(sliderSize, sliderThickness);
    }
    refreshSliderPosition();
}

void Photon::UI::Slider::setSize(float size)
{
    this->size = size;
    if (direction == UI::SliderDirection::HORIZONTAL)
        bar.setDimension(size, thickness);
    else
        bar.setDimension(thickness, size);
    refreshSliderPosition();
}

void Photon::UI::Slider::setThickness(float thickness)
{
    this->thickness = thickness;
    if (direction == UI::SliderDirection::HORIZONTAL)
        bar.setDimension(size, thickness);
    else
        bar.setDimension(thickness, size);
    refreshSliderPosition();
}

void Photon::UI::Slider::setSliderSize(float sliderSize)
{
    this->sliderSize = sliderSize;
    if (direction == UI::SliderDirection::HORIZONTAL)
        slider->setDimension(sliderThickness, sliderSize);
    else
        slider->setDimension(sliderSize, sliderThickness);
    refreshSliderPosition();
}

void Photon::UI::Slider::setSliderThickness(float sliderThickness)
{
    this->sliderThickness = sliderThickness;
    if (direction == UI::SliderDirection::HORIZONTAL)
        slider->setDimension(sliderThickness, sliderSize);
    else
        slider->setDimension(sliderSize, sliderThickness);
    refreshSliderPosition();
}

void Photon::UI::Slider::setPercent(float percent)
{
    float oldPercent = this->percent;
    this->percent = percent;
    refreshSliderPosition();
    if (haveAction)
        action(oldPercent, percent);
}

Photon::UI::SliderDirection Photon::UI::Slider::getDirection() const
{
    return direction;
}

float Photon::UI::Slider::getSize() const
{
    return size;
}

float Photon::UI::Slider::getThickness() const
{
    return thickness;
}

float Photon::UI::Slider::getSliderSize() const
{
    return sliderSize;
}

float Photon::UI::Slider::getPercent() const
{
    return percent;
}

void Photon::UI::Slider::setBarColor(const Photon::Color &color)
{
    bar.getMaterial()->setColor(color);
}

void Photon::UI::Slider::setBarTexture(const Photon::Texture::Texture &texture)
{
    auto barMaterial = bar.getMaterial();
    barMaterial->clearTextures();
    barMaterial->addTexture(texture);
}

void Photon::UI::Slider::onChange(const std::function<void(float, float)> &action)
{
    this->action = action;
    haveAction = true;
}

float Photon::UI::Slider::getSliderThickness() const
{
    return sliderThickness;
}
