#ifndef PHOTON_UI_SLIDER_HPP
#define PHOTON_UI_SLIDER_HPP

#include "UI_Button.hpp"

namespace Photon::UI
{
    class Slider : public Gui
    {
    private:
        Photon::Object::Rectangle bar;
        std::shared_ptr<Button> slider;
        UI::SliderDirection direction;
        float size;
        float thickness;
        float sliderSize;
        float sliderThickness;
        float percent;
        bool haveAction;
        std::function<void(float, float)> action;

    private:
        void draw() override;
        void refreshSliderPosition();

    public:
        Slider(SliderDirection, float, float, float, float);
        const std::shared_ptr<Button> &getSlider() const;
        SliderDirection getDirection() const;
        float getSize() const;
        float getThickness() const;
        float getSliderSize() const;
        float getSliderThickness() const;
        float getPercent() const;
        void setDirection(SliderDirection);
        void setSize(float);
        void setThickness(float);
        void setSliderSize(float);
        void setSliderThickness(float);
        void setPercent(float);
        void setBarColor(const Color &);
        void setBarTexture(const Texture::Texture &);
        void onChange(const std::function<void(float, float)> &);
    };
}

#endif
