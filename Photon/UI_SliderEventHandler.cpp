#include "Mouse.hpp"
#include "UI_SliderEventHandler.hpp"

Photon::UI::SliderEventHandler::SliderEventHandler(const std::shared_ptr<Slider> &slider) :
    AButtonEventHandler(slider->getSlider()), slider(slider) {}

Photon::Vector2f Photon::UI::SliderEventHandler::getButtonPos()
{
    return button->getPosition() + slider->getPosition();
}

void Photon::UI::SliderEventHandler::handle(const Vector2f &mousePosition, bool mouseIsClicked, bool mouseOnButton)
{
    if (!mouseIsClicked)
    {
        if (mouseOnButton)
            button->setState(UI::ButtonState::HOVER);
        else
            button->setState(UI::ButtonState::RELEASED);
    }
    else
    {
        float delta;
        auto buttonPosition = getButtonPos();
        if (slider->getDirection() == UI::SliderDirection::HORIZONTAL)
            delta = mousePosition.getX() - (buttonPosition.getX() + (button->getWidth() / 2));
        else
            delta = mousePosition.getY() - (buttonPosition.getY() + (button->getHeight() / 2));
        if (delta != 0)
        {
            auto movementPercent = (100 * delta) / slider->getSize();
            auto newPercent = slider->getPercent() + movementPercent;
            if (newPercent > 100)
                newPercent = 100;
            else if (newPercent < 0)
                newPercent = 0;
            slider->setPercent(newPercent);
        }
    }
}