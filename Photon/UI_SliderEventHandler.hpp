#ifndef PHOTON_UI_SLIDEREVENTHANDLER_HPP
#define PHOTON_UI_SLIDEREVENTHANDLER_HPP

#include "UI_AButtonEventHandler.hpp"
#include "UI_Slider.hpp"

namespace Photon::UI
{
    class SliderEventHandler : public AButtonEventHandler
    {
    private:
        std::shared_ptr<Slider> slider;

    private:
        void handle(const Vector2f &, bool, bool) override;
        Vector2f getButtonPos() override;

    public:
        SliderEventHandler(const std::shared_ptr<Slider> &button);
    };
}

#endif
