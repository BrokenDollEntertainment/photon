#include "Glut.hpp"
#include "UI_Text.hpp"

Photon::UI::Text::Text(const std::string &text, const std::shared_ptr<Photon::UI::Font> &font) :
    text(text), font(font) {}

void Photon::UI::Text::setText(const std::string &text)
{
    this->text = text;
}

const std::string &Photon::UI::Text::getText() const
{
    return text;
}

void Photon::UI::Text::draw()
{
    if (font == nullptr || !font->valid)
    {
        glRasterPos2f(margin.getX(), margin.getY());
        for (char c : text)
            glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, c);
    }
    else
        font->render(text, margin, getScale());
}

void Photon::UI::Text::setMargin(float marginX, float marginY)
{
    margin.setX(marginX);
    margin.setY(marginY);
}

void Photon::UI::Text::setFont(const std::shared_ptr<Photon::UI::Font> &font)
{
    this->font = font;
}

void Photon::UI::Text::setTextColor(const Photon::Color &textColor)
{
    getMaterial()->setColor(textColor);
    if (font != nullptr)
        font->setColor(textColor);
}
