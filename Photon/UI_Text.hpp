#ifndef PHOTON_UI_TEXT_HPP
#define PHOTON_UI_TEXT_HPP

#include "UI_Gui.hpp"
#include "UI_Font.hpp"

namespace Photon::UI
{
    class Text : public Gui
    {
    private:
        Vector2f margin;
        std::string text;
        std::shared_ptr<Font> font;

    private:
        void draw() override;

    public:
        Text(const std::string &, const std::shared_ptr<Font> & = nullptr);
        void setText(const std::string &);
        void setFont(const std::shared_ptr<Font> &);
        const std::string &getText() const;
        void setMargin(float, float);
        void setTextColor(const Color &);
    };
}

#endif