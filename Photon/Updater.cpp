#include "Updater.hpp"
#include "Scene.hpp"

Photon::Updater::Updater() : loaded(false) {}

void Photon::Updater::init() {}
void Photon::Updater::destroy() {}

void Photon::Updater::load()
{
    if (!loaded)
    {
        init();
        loaded = true;
    }
}

void Photon::Updater::unload()
{
    if (loaded)
    {
        destroy();
        loaded = false;
    }
}

void Photon::Updater::setScene(const std::shared_ptr<Photon::Scene> &scene)
{
    this->scene = scene;
}
