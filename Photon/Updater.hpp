#ifndef PHOTON_UPDATER_HPP
#define PHOTON_UPDATER_HPP

#include <memory>

namespace Photon
{
    class Renderer;
    class Scene;
    class Updater
    {
        friend class Renderer;
        friend class Scene;
    private:
        bool loaded;

    protected:
        std::shared_ptr<Scene> scene;

    private:
        void setScene(const std::shared_ptr<Scene> &scene);
        virtual void init();
        virtual void destroy();
        virtual void update() = 0;

    public:
        Updater();
        void load();
        void unload();
    };
}

#endif
