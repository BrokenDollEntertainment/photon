#ifndef PHOTON_VECTOR_VECTOR2_HPP
#define PHOTON_VECTOR_VECTOR2_HPP

#include <cmath>
#include <memory>
#include <ostream>

namespace Photon
{
    template <typename T>
    class Vector2
    {
    private:
        template <typename U>
        struct Vector2InternalStruct
        {
            U x;
            U y;
            Vector2InternalStruct(U x, U y) : x(x), y(y) {}
        };

    protected:
        std::shared_ptr<Vector2InternalStruct<T>> position;

    private:

    public:
        Vector2(T x, T y) : position(std::make_shared<Vector2InternalStruct<T>>(x, y)) {}
        Vector2() : position(std::make_shared<Vector2InternalStruct<T>>(static_cast<T>(0), static_cast<T>(0))) {}
        Vector2(const Vector2<T> &other) : position(std::make_shared<Vector2InternalStruct<T>>(other.position->x, other.position->y)) {}

        void link(const Vector2<T> &other)
        {
            position = other.position;
        }

        T getX() const
        {
            return position->x;
        }

        T getY() const
        {
            return position->y;
        }

        void setX(T x)
        {
            position->x = x;
        }

        void setY(T y)
        {
            position->y = y;
        }

        Vector2<T>& operator=(const Vector2<T>& other)
		{
			position->x = other.position->x;
			position->y = other.position->y;
			return *this;
		}

        bool operator==(const Vector2<T> &other) const
        {
            return (position->x == other.position->x &&
                    position->y == other.position->y);
        }

        bool operator!=(const Vector2<T> &other) const
        {
            return (position->x != other.position->x ||
                    position->y != other.position->y);
        }

        Vector2<T> rotate(double angle)
        {
            return Vector2<T>(position->x * cos(angle) + position->y * -sin(angle),
                       position->x * sin(angle) + position->y * cos(angle));
        }

        Vector2<T> normalize() const
        {
            T x = position->x, y = position->y;
            T newX = 0, newY = 0;
            T length = ::sqrtf((x * x) + (y * y));
            if (length != 0)
            {
                newX = x / length;
                newY = y / length;
            }
            return Vec(newX, newY);
        }

//        Vector2<T> crossProduct(const Vector2<T> &other) const
//        {
//            TODO Create the cross product for 2D Vector
//        }

        T dotProduct(const Vector2<T> &other) const
        {
            return (position->x * other.position->x) + (position->y * other.position->y);
        }

//        Vector2<T> crossProduct(T otherX, T otherY) const
//        {
//            TODO Create the cross product for 2D Vector
//        }

        T dotProduct(T otherX, T otherY) const
        {
            return (position->x * otherX) + (position->y * otherY);
        }

        void operator*=(T value)
        {
            position->x *= value;
            position->y *= value;
        }

        Vector2<T> operator*(T value) const
        {
            return Vector2<T>(position->x * value, position->y * value);
        }

        void operator*=(const Vector2<T> &other)
        {
            position->x *= other.position->x;
            position->y *= other.position->y;
        }

        Vector2<T> operator*(const Vector2<T> &other) const
        {
            return Vector2<T>(position->x * other.position->x, position->y * other.position->y);
        }

        void operator/=(T value)
        {
            position->x /= value;
            position->y /= value;
        }

        Vector2<T> operator/(T value) const
        {
            return Vector2<T>(position->x / value, position->y / value);
        }

        void operator/=(const Vector2<T>&other)
        {
            position->x /= other.position->x;
            position->y /= other.position->y;
        }

        Vector2<T> operator/(const Vector2<T> &other) const
        {
            return Vector2<T>(position->x / other.position->x, position->y / other.position->y);
        }

        void operator+=(const Vector2<T>&other)
        {
            position->x += other.position->x;
            position->y += other.position->y;
        }

        Vector2<T> operator+(const Vector2<T>&other) const
        {
            return Vector2<T>(position->x + other.position->x, position->y + other.position->y);
        }

        void operator-=(const Vector2<T>&other)
        {
            position->x -= other.position->x;
            position->y -= other.position->y;
        }

        Vector2<T> operator-(const Vector2<T>&other) const
        {
            return Vector2<T>(position->x - other.position->x, position->y - other.position->y);
        }

        friend std::ostream &operator<<(std::ostream &os, const Vector2<T> &vector)
        {
            os << "x:" << vector.getX() << " y:" << vector.getY();
            return os;
        }
    };

	using Vector2c = Vector2<char>;
	using Vector2d = Vector2<double>;
	using Vector2f = Vector2<float>;
	using Vector2i = Vector2<int>;
	using Vector2l = Vector2<long>;
	using Vector2ld = Vector2<long double>;
	using Vector2ll = Vector2<long long>;
	using Vector2s = Vector2<short>;
	using Vector2uc = Vector2<unsigned char>;
	using Vector2ui = Vector2<unsigned int>;
	using Vector2ul = Vector2<unsigned long>;
	using Vector2ull = Vector2<unsigned long long>;
	using Vector2us = Vector2<unsigned short>;
}

#endif
