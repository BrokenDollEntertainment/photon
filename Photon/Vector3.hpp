#ifndef PHOTON_VECTOR_VECTOR3_HPP
#define PHOTON_VECTOR_VECTOR3_HPP

#include <cmath>
#include <memory>
#include <ostream>

namespace Photon
{
    template <typename T>
    class Vector3
    {
    private:
		template <typename U>
		struct Vector3InternalStruct
        {
            U x;
            U y;
            U z;
            Vector3InternalStruct(U x, U y, U z) : x(x), y(y), z(z) {}
        };

    protected:
        std::shared_ptr<Vector3InternalStruct<T>> position;

    private:
        Vector3<T> rotateX(double angle) const
        {
            return Vector3<T>(position->x,
                       position->y * cos(angle) - position->z * sin(angle),
                       position->y * sin(angle) + position->z * cos(angle));
        }

        Vector3<T> rotateY(T angle) const
        {
            return Vector3<T>(position->x * cos(angle) + position->z * sin(angle),
                       position->y,
                       position->x * -sin(angle) + position->z * cos(angle));
        }

        Vector3<T> rotateZ(T angle) const
        {
            return Vector3<T>(position->x * cos(angle) - position->y * sin(angle),
                       position->x * sin(angle) + position->y * cos(angle),
                       position->z);
        }

    public:
        Vector3(T x, T y, T z) : position(std::make_shared<Vector3InternalStruct<T>>(x, y, z)) {}
        Vector3() : position(std::make_shared<Vector3InternalStruct<T>>(static_cast<T>(0), static_cast<T>(0), static_cast<T>(0))) {}
        Vector3(const Vector3<T>&other) : position(std::make_shared<Vector3InternalStruct<T>>(other.position->x, other.position->y, other.position->z)) {}

        void link(const Vector3<T> &other)
        {
            position = other.position;
        }

        T getX() const
        {
            return position->x;
        }

        T getY() const
        {
            return position->y;
        }

        T getZ() const
        {
            return position->z;
        }

        void setX(T x)
        {
            position->x = x;
        }

        void setY(T y)
        {
            position->y = y;
        }

        void setZ(T z)
        {
            position->z = z;
        }

        bool operator==(const Vector3<T> &other) const
        {
            return (position->x == other.position->x &&
                    position->y == other.position->y &&
                    position->z == other.position->z);
        }

        bool operator!=(const Vector3<T> &other) const
        {
            return (position->x != other.position->x ||
                    position->y != other.position->y ||
                    position->z != other.position->z);
        }

        Vector3<T> rotate(double angleX, double angleY, double angleZ) const
        {
            Vector3<T> ret = rotateX(angleX);
            ret = ret.rotateY(angleY);
            return ret.rotateZ(angleZ);
        }

        Vector3<T> normalize() const
        {
            T x = position->x, y = position->y, z = position->z;
            T newX = 0, newY = 0, newZ = 0;
            T length = ::sqrtf((x * x) + (y * y) + (z * z));
            if (length != 0)
            {
                newX = x / length;
                newY = y / length;
                newZ = z / length;
            }
            return Vector3<T>(newX, newY, newZ);
        }

        Vector3<T> crossProduct(const Vector3<T> &other) const
        {
            T x = position->x, y = position->y, z = position->z;
            T otherX = other.position->x, otherY = other.position->y, otherZ = other.position->z;
            return Vector3<T>(((y * otherZ) - (z * otherY)), ((x * otherZ) - (z * otherX)), ((x * otherY) - (y * otherX)));
        }

        T dotProduct(const Vector3<T> &other) const
        {
            return (position->x * other.position->x) + (position->y * other.position->y) + (position->z * other.position->z);
        }

        Vector3<T> crossProduct(T otherX, T otherY, T otherZ) const
        {
            T x = position->x, y = position->y, z = position->z;
            return Vector3<T>(((y * otherZ) - (z * otherY)), ((x * otherZ) - (z * otherX)), ((x * otherY) - (y * otherX)));
        }

        T dotProduct(T otherX, T otherY, T otherZ) const
        {
            return (position->x * otherX) + (position->y * otherY) + (position->z * otherZ);
        }

		Vector3<T> &operator=(const Vector3<T> &other)
		{
			position->x = other.position->x;
			position->y = other.position->y;
			position->z = other.position->z;
			return *this;
		}

        void operator*=(T value)
        {
            position->x *= value;
            position->y *= value;
            position->z *= value;
        }

        Vector3<T> operator*(T value) const
        {
            return Vector3<T>(position->x * value, position->y * value, position->z * value);
        }

        void operator*=(const Vector3<T> &other)
        {
            position->x *= other.position->x;
            position->y *= other.position->y;
            position->z *= other.position->z;
        }

        Vector3<T> operator*(const Vector3<T> &other) const
        {
            return Vector3<T>(position->x * other.position->x, position->y * other.position->y, position->z * other.position->z);
        }

        Vector3<T> operator/(T value) const
        {
            return Vector3<T>(position->x / value, position->y / value, position->z / value);
        }

        void operator/=(T value)
        {
            position->x /= value;
            position->y /= value;
            position->z /= value;
        }

        Vector3<T> operator/(const Vector3<T> &other) const
        {
            return Vector3<T>(position->x / other.position->x, position->y / other.position->y, position->z / other.position->z);
        }

        void operator/=(const Vector3<T> &other)
        {
            position->x /= other.position->x;
            position->y /= other.position->y;
            position->z /= other.position->z;
        }

        void operator+=(const Vector3<T> &other)
        {
            position->x += other.position->x;
            position->y += other.position->y;
            position->z += other.position->z;
        }

        Vector3<T> operator+(const Vector3<T> &other) const
        {
            return Vector3<T>(position->x + other.position->x, position->y + other.position->y, position->z + other.position->z);
        }

        void operator-=(const Vector3<T> &other)
        {
            position->x -= other.position->x;
            position->y -= other.position->y;
            position->z -= other.position->z;
        }

        Vector3<T> operator-(const Vector3<T> &other) const
        {
            return Vector3<T>(position->x - other.position->x, position->y - other.position->y, position->z - other.position->z);
        }

        friend std::ostream &operator<<(std::ostream &os, const Vector3<T> &vector)
        {
            os << "x:" << vector.getX() << " y:" << vector.getY() << " z:" << vector.getZ();
            return os;
        }
	};

	using Vector3c = Vector3<char>;
	using Vector3d = Vector3<double>;
	using Vector3f = Vector3<float>;
	using Vector3i = Vector3<int>;
	using Vector3l = Vector3<long>;
	using Vector3ld = Vector3<long double>;
	using Vector3ll = Vector3<long long>;
	using Vector3s = Vector3<short>;
	using Vector3uc = Vector3<unsigned char>;
	using Vector3ui = Vector3<unsigned int>;
	using Vector3ul = Vector3<unsigned long>;
	using Vector3ull = Vector3<unsigned long long>;
	using Vector3us = Vector3<unsigned short>;
}

#endif
