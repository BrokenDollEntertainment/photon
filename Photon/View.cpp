#include "View.hpp"

Photon::Vector2i Photon::View::viewPos = {0, 0};
unsigned int Photon::View::viewWidth = 0;
unsigned int Photon::View::viewHeight = 0;
float Photon::View::projection[4][4] = {
        {1, 1, 1, 1},
        {1, 1, 1, 1},
        {1, 1, 1, 1},
        {1, 1, 1, 1}
};

void Photon::View::resetProjectionMatrix()
{
    for (unsigned int x = 0; x != 4; ++x)
    {
        for (unsigned int y = 0; y != 4; ++y)
            projection[x][y] = 1;
    }
}

void Photon::View::setView(const Photon::Vector2i &viewPos, unsigned int viewWidth, unsigned int viewHeight)
{
    View::viewPos = viewPos;
    View::viewWidth = viewWidth;
    View::viewHeight = viewHeight;
}

void Photon::View::ortho(float left, float right, float bottom, float top)
{
    resetProjectionMatrix();
    projection[0][0] = 2 / (right - left);
    projection[1][1] = 2 / (top - bottom);
    projection[2][2] = -1;
    projection[3][0] = -(right + left) / (right - left);
    projection[3][1] = -(top + bottom) / (top - bottom);
}

float *Photon::View::getProjection()
{
    return &projection[0][0];
}
