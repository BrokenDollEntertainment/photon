#ifndef PHOTON_VIEW_HPP
#define PHOTON_VIEW_HPP

#include "Vector2.hpp"

namespace Photon
{
    class Camera;
    class Camera2;
    class Camera3;
    class View
    {
        friend class Camera;
        friend class Camera2;
        friend class Camera3;
    private:
        static Vector2i viewPos;
        static unsigned int viewWidth;
        static unsigned int viewHeight;
        static float projection[4][4];

    private:
        static void resetProjectionMatrix();
        static void setView(const Vector2i &, unsigned int, unsigned int);
        static void ortho(float, float, float, float);

    public:
        static float *getProjection();
    };
}

#endif
