#include <Photon/Handler_StopHandler.hpp>
#include <Photon/Keyboard.hpp>
#include <Photon/LightManager.hpp>
#include <Photon/Mouse.hpp>
#include <Photon/Object_Box.hpp>
#include <Photon/Object_Mesh.hpp>
#include <Photon/Object_Plane.hpp>
#include "Updater.hpp"

#ifdef _WIN32
    #define SKY_BOX_LEFT "D:/Programmation/Photon/bin/assets/skybox/Animator/Left.tga"
    #define SKY_BOX_TOP "D:/Programmation/Photon/bin/assets/skybox/Animator/Top.tga"
    #define SKY_BOX_RIGHT "D:/Programmation/Photon/bin/assets/skybox/Animator/Right.tga"
    #define SKY_BOX_BOTTOM "D:/Programmation/Photon/bin/assets/skybox/Animator/Bottom.tga"
    #define SKY_BOX_BACK "D:/Programmation/Photon/bin/assets/skybox/Animator/Back.tga"
    #define SKY_BOX_FRONT "D:/Programmation/Photon/bin/assets/skybox/Animator/Front.tga"
    #define BOX_LEFT "D:/Programmation/Photon/bin/assets/crate.jpg"
    #define BOX_TOP "D:/Programmation/Photon/bin/assets/crate.jpg"
    #define BOX_RIGHT "D:/Programmation/Photon/bin/assets/crate.jpg"
    #define BOX_BOTTOM "D:/Programmation/Photon/bin/assets/crate.jpg"
    #define BOX_BACK "D:/Programmation/Photon/bin/assets/crate.jpg"
    #define BOX_FRONT "D:/Programmation/Photon/bin/assets/crate.jpg"
    #define FLOOR "D:/Programmation/Photon/bin/assets/grass.png"
#elif __linux__
    #define SKY_BOX_LEFT "assets/skybox/Animator/Left.tga"
    #define SKY_BOX_TOP "assets/skybox/Animator/Top.tga"
    #define SKY_BOX_RIGHT "assets/skybox/Animator/Right.tga"
    #define SKY_BOX_BOTTOM "assets/skybox/Animator/Bottom.tga"
    #define SKY_BOX_BACK "assets/skybox/Animator/Back.tga"
    #define SKY_BOX_FRONT "assets/skybox/Animator/Front.tga"
    #define BOX_LEFT "assets/crate.jpg"
    #define BOX_TOP "assets/crate.jpg"
    #define BOX_RIGHT "assets/crate.jpg"
    #define BOX_BOTTOM "assets/crate.jpg"
    #define BOX_BACK "assets/crate.jpg"
    #define BOX_FRONT "assets/crate.jpg"
    #define FLOOR "assets/grass.png"
#endif

Updater::Updater()
{
    skyBoxTexture[Photon::Texture::Box::LEFT](SKY_BOX_LEFT);
    skyBoxTexture[Photon::Texture::Box::TOP](SKY_BOX_TOP);
    skyBoxTexture[Photon::Texture::Box::RIGHT](SKY_BOX_RIGHT);
	skyBoxTexture[Photon::Texture::Box::BOTTOM](SKY_BOX_BOTTOM);
	skyBoxTexture[Photon::Texture::Box::BACK](SKY_BOX_BACK);
    skyBoxTexture[Photon::Texture::Box::FRONT](SKY_BOX_FRONT);

    boxTexture[Photon::Texture::Box::LEFT](BOX_LEFT);
    boxTexture[Photon::Texture::Box::TOP](BOX_TOP);
    boxTexture[Photon::Texture::Box::RIGHT](BOX_RIGHT);
    boxTexture[Photon::Texture::Box::BOTTOM](BOX_BOTTOM);
    boxTexture[Photon::Texture::Box::BACK](BOX_BACK);
    boxTexture[Photon::Texture::Box::FRONT](BOX_FRONT);
}

void Updater::update()
{
    if (Photon::Mouse::isClicked(Photon::MouseKey::LEFTCLICK))
        camera->translateAt(Photon::Mouse::getDelta(Photon::Axis::XAXIS) * -0.1f, Photon::Mouse::getDelta(Photon::Axis::YAXIS) * -0.1f, 0);
    if (Photon::Keyboard::isKeyPressed(Photon::Key::W))
        camera->translate(0, 0, -0.1f);
    else if (Photon::Keyboard::isKeyPressed(Photon::Key::S))
        camera->translate(0, 0, 0.1f);
    if (Photon::Keyboard::isKeyPressed(Photon::Key::A))
        camera->translate(0.1f, 0, 0);
    else if (Photon::Keyboard::isKeyPressed(Photon::Key::D))
        camera->translate(-0.1f, 0, 0);
}

void Updater::init()
{
	Photon::LightManager::enable();
    Photon::LightManager::setSmoothLight(true);
    Photon::Mouse::setCursorVisibility(true);
    camera = scene->getCamera(DEFAULT_CAMERA);
    camera->enableDepthClamp(false);
    camera->setPosition(0, 4, 10);
    camera->setLookAt(0, 4, 0);
    camera->setSkyBox(skyBoxTexture);
	scene->addEventHandler(std::make_shared<Photon::Handler::StopHandler>(KEY_SET{ Photon::Key::ESC, Photon::Key::EXIT }));
    if (auto floor = std::make_shared<Photon::Object::Plane>(200.0f, 200.0f))
    {
        floor->setPosition(-100, 0, -100);
        floor->getMaterial()->addTexture(Photon::Texture::Loader::load(FLOOR));
        scene->add(floor);
    }
    if (auto mesh = Photon::Object::Mesh::loadOBJFile("D:/Programmation/Photon/bin/assets/mesh.obj", Photon::Color(125, 125, 125)))
    {
		mesh->setScale(0.1f, 0.1f, 0.1f);
		scene->add(mesh);
    }
    if (auto light1 = Photon::LightManager::get(0))
    {
        light1->setOn(true);
        light1->setAmbientColor(Photon::Color::YELLOW);
        light1->setPosition(Photon::Vector3f{ 10, 4, 0 });
        light1->setSpotDirection(Photon::Vector3f{ 0, 4, 0 });
	}
	if (auto light2 = Photon::LightManager::get(1))
	{
        light2->setOn(true);
        light2->setAmbientColor(Photon::Color::RED);
        light2->setPosition(Photon::Vector3f{ -10, 4, 0 });
        light2->setSpotDirection(Photon::Vector3f{ 0, 4, 0 });
	}
}
