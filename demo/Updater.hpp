#ifndef COURSEPROJECT_COMPUTERGRAPHICS_ANIMATORUPDATER_HPP
#define COURSEPROJECT_COMPUTERGRAPHICS_ANIMATORUPDATER_HPP

#include <Photon/Renderer.hpp>

class Updater : public Photon::Updater
{
private:
	Photon::Texture::Box boxTexture;
    Photon::Texture::Box skyBoxTexture;
    std::shared_ptr<Photon::Camera> camera;

private:
    void init() override;
    void update() override;

public:
    Updater();
};

#endif
