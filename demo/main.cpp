#include <Photon/Debugger.hpp>
#include <Photon/Renderer.hpp>
#include <Photon/LightManager.hpp>
#include "Updater.hpp"

int main(int, char **)
{
    Photon::Debugger::setEnabled(true);
    Photon::Renderer::Configuration configuration = {"Photon Demo", 1920, 1080, true, Photon::Scene::SPACE};
    Photon::Renderer::init(configuration);
    Photon::Renderer::addScene("Updater", Photon::Scene::SPACE);
    Photon::Renderer::setUpdaterTo("Updater", std::make_shared<Updater>());
    Photon::Renderer::setCurrentScene("Updater");
    Photon::Renderer::start();
}
