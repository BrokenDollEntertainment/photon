#!/usr/bin/env bash

mkdir -p deploy/photon
mkdir -p deploy/photon/lib
cp -R include/ deploy/photon
cp -R src/ deploy/photon
cp -R stb/ deploy/photon
cp -R CMakeLists.txt deploy/photon
cp -R PhotonModule.cmake deploy/photon
cd deploy
ls
zip -r ../deploy.zip photon/
cd ..
rm -rf deploy/