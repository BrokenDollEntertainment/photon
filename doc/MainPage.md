The Photon is a Graphical Engine created by BrokenDollEntertainment<br>
The Photon is free to use<br>
To build with the Photon, unzip the deploy.zip in your project and use CMake<br>
A classic project structure is:
.<br>
├── assets/<br>
├── include/<br>
├── photon/<br>
├── src/<br>
└── CMakeLists.txt<br>
## Build
### Linux
To build on Linux use CMake:
```bash
mkdir build
cd build
cmake ..
make
```
### Windows
To build on Windows use Visual Studio
